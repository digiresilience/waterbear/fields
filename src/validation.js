const R = require("ramda");
const Ajv = require("ajv");

const Draft4 = require("./data/draft4");
const {
  getRequiredFieldIds,
  getOptionIdsForField,
  loadFields
} = require("./fields");

const schemaSkeleton = {
  $schema: "http://json-schema.org/draft-04/schema#",
  type: "object",
  properties: {}
};

const xRequired = R.lensProp("required");
const xProperties = R.lensProp("properties");
const setProperties = props => R.set(xProperties, props, schemaSkeleton);

const isRequiredEmpty = R.pipe(R.view(xRequired), R.isEmpty);
const setRequired = required =>
  R.pipe(
    R.set(xRequired, required),
    R.when(isRequiredEmpty, R.dissoc("required"))
  );

/**
 * Converts a single field spec into a json schema snippet
 */
const fieldToSchema = (fields, field) => {
  const f = {
    type: "string"
  };
  const isRequired = field.required;

  if (field.type === "select") {
    f.enum = getOptionIdsForField(fields, field.id);
    if (!isRequired) {
      f.type = ["string", "null"];
      f.enum.push(null);
    }
  }
  if (field.type === "multiselect") {
    f.type = isRequired ? "array" : ["array", "null"];
    f.items = {
      type: "string",
      enum: getOptionIdsForField(fields, field.id)
    };
    f.uniqueItems = true;
  }
  if (field.type === "stringlist") {
    f.type = { $ref: "#/definitions/stringArray" };
  } else if (field.type === "objectlist") {
    f.type = ["array", "null"];
  }

  const r = {};
  r[field.id] = f;
  return r;
};

/**
 * Convert fields spec into a valid JSON Schema document that can be used to validate
 * submitted for data
 *
 * @param fields - the list of field specs
 * @return a JSON Schema document
 */
const fieldsToSchema = fields => {
  const loaded = loadFields(fields);
  return R.pipe(
    R.map(R.partial(fieldToSchema, [loaded])),
    R.mergeAll,
    setProperties,
    setRequired(getRequiredFieldIds(loaded))
  )(loaded);
};

module.exports.fieldsToSchema = fieldsToSchema;

const createAjv = options => {
  const ajv = new Ajv({
    schemaId: "auto",
    allErrors: true,
    jsonPointers: true,
    errorDataPath: "property",
    verbose: true,
    ...options
  });
  ajv.addFormat("time", "^([0-1][0-9]|2[0-3]):[0-5][0-9]$");
  ajv.addMetaSchema(Draft4);
  return ajv;
};

const validate = (validator, data) => {
  const valid = validator(data);
  if (valid) {
    return [];
  }

  return validator.errors;
};

const sanitizeErrors = (validator, data) =>
  validate(validator, data).map(error => {
    // eslint-disable-next-line no-param-reassign
    error.dataPath = error.dataPath.replace(/\//g, ".").substr(1);

    return error;
  });

/**
 * Validates that a json schema compiles properly
 *
 * @return true when there are no errors, otherwise throws
 */
const validateSchema = schema => {
  const ajv = createAjv();
  ajv.compile(schema);
  return true;
};

/**
 * Validates user submitted data against a schema
 * @see fieldsToSchema to get the schema from the field spec
 * @param schema - the schema of the fields
 * @param data = the form data to validate
 * @return true when there are no errors, otherwise a list of errors
 */
const validateData = (schema, data) => {
  const ajv = createAjv();
  const validator = ajv.compile(schema);
  const errors = sanitizeErrors(validator, data);
  return errors;
};

module.exports.createAjv = createAjv;
module.exports.validateSchema = validateSchema;
module.exports.validateData = validateData;
module.exports.Draft4 = Draft4;
