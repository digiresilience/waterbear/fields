/* eslint global-require: "off" */
const { loadFields } = require("./fields");

module.exports.FieldsV1 = loadFields(require("./specs/v1").FieldsV1);
module.exports.TagsV1 = loadFields(require("./specs/tags-v1").TagsV1);
