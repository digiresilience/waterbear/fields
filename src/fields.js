const Joi = require("@hapi/joi");
const R = require("ramda");
const humanizeString = require("humanize-string");

/**
 * Humanizes its argument unless the arg is nil
 * @param string - the string
 * @return the humanized string or nil
 */
const safeHumanize = R.unless(R.isNil, R.curry(humanizeString));

/**
 * The types of fields we support in the form
 */
const fieldTypes = [
  "text",
  "longtext",
  "select",
  "multiselect",
  "treeselect",
  "date",
  "timestamp",
  "stringlist",
  "objectlist",
];
module.exports.fieldTypes = fieldTypes;

/**
 * Every field must be filled by one of these roles
 */
const roleTypes = ["submitter", "analyst"];
module.exports.roleTypes = roleTypes;

/**
 * @see {@link fieldSchema} for the full definition of a FieldSpec
 * @typedef {Object} FieldSpec
 */

/**
 * Joi schema that defines a valid field object
 */
const fieldSchema = Joi.object({
  id: Joi.string().required(),
  required: Joi.boolean().default(false),
  name: Joi.string().default((f) => safeHumanize(f.id)),
  displayName: Joi.string(),
  comment: Joi.string(),
  caption: Joi.string(),
  hidden: Joi.boolean().default(false),
  hint: Joi.string(),
  default: Joi.any().default(null),
  for: Joi.string()
    .valid(...roleTypes)
    .required(),
  type: Joi.string()
    .valid(...fieldTypes)
    .required(),
  options: Joi.alternatives().try(
    Joi.array()
      .when("type", { is: "select", then: Joi.required() })
      .when("type", { is: "multiselect", then: Joi.required() })
      .items(
        /**
         * An option to choose from, maybe more than one
         * @typedef {Object} OptionSpec
         * @property {string} comment
         * @property {string} id
         * @property {string} name
         */
        Joi.object({
          comment: Joi.string(),
          id: Joi.string(),
          name: Joi.string(),
        })
      ),
    Joi.array()
      .when("type", { is: "treeselect", then: Joi.required() })
      .items(
        Joi.object({
          name: Joi.string(),
          value: Joi.string(),
          children: Joi.array(),
        })
      )
  ),
  keys: Joi.array()
    .when("type", { is: "objectlist", then: Joi.required() })
    .items(
      Joi.object({
        id: Joi.string().required(),
        comment: Joi.string(),
        name: Joi.string().default((f) => safeHumanize(f.id)),
        type: Joi.string()
          .valid(...fieldTypes)
          .required(),
      })
    ),
});
module.exports.fieldSchema = fieldSchema;

/**
 * A list of FieldSpec
 * @typedef {FieldSpec[]} FieldSpecs
 */

const fieldsSchema = Joi.array().items(fieldSchema.required()).required();
module.exports.fieldsSchema = fieldsSchema;

/**
 * @param {FieldSpecs} fields - the form fields spec
 * @return {true|Object[]} true if the field spec is valid, otherwise a list of Joi errors
 */
const validateFields = (fields) => {
  const { error } = fieldsSchema.validate(fields);
  if (error) return error;
  return true;
};
module.exports.validateFields = validateFields;

const loadFields = (fields) => {
  const { value } = fieldsSchema.validate(fields);
  return value;
};
module.exports.loadFields = loadFields;

/**
 * @param {FieldSpecs} fields - the form fields spec
 * @return {string[]} an array containing the ids of all the fields
 */
const getFieldIds = (fields) => R.map(R.prop("id"), fields);
module.exports.getFieldIds = getFieldIds;

/**
 *
 * @param {FieldSpecs} fields - the form fields spec
 * @param {string} fieldId - id of the field
 * @return {FieldSpec} the spec object for the field named by id, or nil
 */
const getFieldById = (fields, fieldId) =>
  R.find(R.propEq("id", fieldId))(fields);
module.exports.getFieldById = getFieldById;
/**
 * @param {FieldSpecs} fields - the form fields spec
 * @param {string} fieldId - id of the field
 * @return {OptionSpec[]} an array of options for the field, or false if there are no options
 */
const getOptionsForField = (fields, fieldId) =>
  R.propOr(false, "options", getFieldById(fields, fieldId));
module.exports.getOptionsForField = getOptionsForField;

/**
 * @param {FieldSpecs} fields - the form fields spec
 * @param {string} fieldId - id of the field
 * @return {string[]} an array of ids for all the options of the field, or false if there are no options
 */
const getOptionIdsForField = (fields, fieldId) =>
  R.when(R.identity, getFieldIds, getOptionsForField(fields, fieldId));
module.exports.getOptionIdsForField = getOptionIdsForField;

/**
 * @param {string} prop - a property in the field spec, e.g.,  "id"
 * @param {*} value - the value to search for
 * @return {function} - returns a function that accepts one parameter, a list of field specs {FieldSpecs} and returns a list of fields whose prop == value.
 */
const getFieldsByProp = (prop, value) => R.filter(R.propEq(prop, value));
module.exports.getFieldsByProp = getFieldsByProp;

/**
 * @param {FieldSpecs} fields - the form fields spec
 * @return {string[]} an array containing the ids of all the required fields
 */
const getRequiredFieldIds = (fields) =>
  getFieldIds(getFieldsByProp("required", true)(fields));
module.exports.getRequiredFieldIds = getRequiredFieldIds;

/**
 * @param {FieldSpecs} fields - the form fields specs
 * @return {FieldSpec[]} an array containing the FieldSpec of all the fields for submitters
 */
const getSubmitterFields = (fields) =>
  getFieldsByProp("for", "submitter")(fields);
module.exports.getSubmitterFields = getSubmitterFields;

/**
 * @param {FieldSpecs} fields - the form fields specs
 * @return {FieldSpec[]} an array containing the FieldSpec of all the fields for analysts
 */
const getAnalystFields = (fields) => getFieldsByProp("for", "analyst")(fields);
module.exports.getAnalystFields = getAnalystFields;

/**
 * @param {FieldSpecs} fields - the form fields specs
 * @param {string} id - the id to search for in fields
 * @return {number} the index in fields that has id
 */
const indexOfField = (fields, id) => R.findIndex(R.propEq("id", id), fields);
module.exports.indexOfField = indexOfField;

/** @param {FieldSpecs} fields - the form fields specs
 * @return {Object} an object with keys for each field id, and the values the default values for the field. NOTE: required fields have no default value.
 */
const getDefaults = (fields) =>
  R.reduce(
    (acc, field) => R.set(R.lensProp(field.id), field.default, acc),
    {},
    fields
  );
module.exports.getDefaults = getDefaults;
