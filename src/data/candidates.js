const loadCandidates = () => [
  {
    name: "President",
    value: "President",
    children: [
      {
        name: "Current",
        value: "Current",
        children: [
          {
            name: "Joe Biden (D)",
            value: "Joe Biden (D)",
          },
          {
            name: "Donald J. Trump (R)",
            value: "Donald J. Trump (R)",
          },
        ],
      },
      {
        name: "Legacy",
        value: "Legacy",
        children: [
          {
            name: "Amy Klobuchar (D)",
            value: "Amy Klobuchar (D)",
          },
          {
            name: "Bernie Sanders (D)",
            value: "Bernie Sanders (D)",
          },
          {
            name: "Bill Weld (R)",
            value: "Bill Weld (R)",
          },
          {
            name: "Elizabeth Warren (D)",
            value: "Elizabeth Warren (D)",
          },
          {
            name: "Michael Bloomberg (D)",
            value: "Michael Bloomberg (D)",
          },
          {
            name: "Pete Buttigieg (D)",
            value: "Pete Buttigieg (D)",
          },
          {
            name: "Roque De La Fuente (R)",
            value: "Roque De La Fuente (R)",
          },
          {
            name: "Tom Steyer (D)",
            value: "Tom Steyer (D)",
          },
          {
            name: "Tulsi Gabbard (D)",
            value: "Tulsi Gabbard (D)",
          },
        ],
      },
    ],
  },
  {
    name: "Vice President",
    value: "Vice President",
    children: [
      {
        name: "Kamala Harris (D)",
        value: "Kamala Harris (D)",
      },
      {
        name: "Mike Pence (R)",
        value: "Mike Pence (R)",
      },
    ],
  },
  {
    name: "Senate",
    value: "Senate",
    children: [
      {
        name: "AL",
        value: "Senate::AL",
        children: [
          {
            name: "Jeff Sessions (R)",
            value: "Senate::AL::Jeff Sessions (R)",
            children: [],
          },
          {
            name: "Tommy Tuberville (R)",
            value: "Senate::AL::Tommy Tuberville (R)",
            children: [],
          },
          {
            name: "Doug Jones (D)",
            value: "Senate::AL::Doug Jones (D)",
            children: [],
          },
        ],
      },
      {
        name: "AK",
        value: "Senate::AK",
        children: [
          {
            name: "Daniel S. Sullivan (R)",
            value: "Senate::AK::Daniel S. Sullivan (R)",
            children: [],
          },
          {
            name: "Adam Master Newman (R)",
            value: "Senate::AK::Adam Master Newman (R)",
            children: [],
          },
        ],
      },
      {
        name: "AZ",
        value: "Senate::AZ",
        children: [
          {
            name: "Mark Kelly (D)",
            value: "Senate::AZ::Mark Kelly (D)",
            children: [],
          },
          {
            name: "Martha McSally (R)",
            value: "Senate::AZ::Martha McSally (R)",
            children: [],
          },
          {
            name: "Daniel McCarthy (R)",
            value: "Senate::AZ::Daniel McCarthy (R)",
            children: [],
          },
        ],
      },
      {
        name: "AR",
        value: "Senate::AR",
        children: [
          {
            name: "Tom Cotton (R)",
            value: "Senate::AR::Tom Cotton (R)",
            children: [],
          },
        ],
      },
      {
        name: "CO",
        value: "Senate::CO",
        children: [
          {
            name: "John Hickenlooper (D)",
            value: "Senate::CO::John Hickenlooper (D)",
            children: [],
          },
          {
            name: "Andrew Romanoff (D)",
            value: "Senate::CO::Andrew Romanoff (D)",
            children: [],
          },
          {
            name: "Cory Gardner (R)",
            value: "Senate::CO::Cory Gardner (R)",
            children: [],
          },
        ],
      },
      {
        name: "DE",
        value: "Senate::DE",
        children: [
          {
            name: "Chris Coons (D)",
            value: "Senate::DE::Chris Coons (D)",
            children: [],
          },
          {
            name: "Jessica Scarane (D)",
            value: "Senate::DE::Jessica Scarane (D)",
            children: [],
          },
          {
            name: "James DeMartino (R)",
            value: "Senate::DE::James DeMartino (R)",
            children: [],
          },
          {
            name: "Lauren Witzke (R)",
            value: "Senate::DE::Lauren Witzke (R)",
            children: [],
          },
        ],
      },
      {
        name: "GA",
        value: "Senate::GA",
        children: [
          {
            name: "Sarah Riggs Amico (D)",
            value: "Senate::GA::Sarah Riggs Amico (D)",
            children: [],
          },
          {
            name: "Marckeith DeJesus (D)",
            value: "Senate::GA::Marckeith DeJesus (D)",
            children: [],
          },
          {
            name: "Maya Dillard Smith (D)",
            value: "Senate::GA::Maya Dillard Smith (D)",
            children: [],
          },
          {
            name: "James Knox (D)",
            value: "Senate::GA::James Knox (D)",
            children: [],
          },
          {
            name: "Tricia Carpenter McCracken (D)",
            value: "Senate::GA::Tricia Carpenter McCracken (D)",
            children: [],
          },
          {
            name: "Jon Ossoff (D)",
            value: "Senate::GA::Jon Ossoff (D)",
            children: [],
          },
          {
            name: "Teresa Tomlinson (D)",
            value: "Senate::GA::Teresa Tomlinson (D)",
            children: [],
          },
          {
            name: "David Perdue (R)",
            value: "Senate::GA::David Perdue (R)",
            children: [],
          },
        ],
      },
      {
        name: "ID",
        value: "Senate::ID",
        children: [
          {
            name: "Paulette E. Jordan (D)",
            value: "Senate::ID::Paulette E. Jordan (D)",
            children: [],
          },
          {
            name: "James Vandermaas (D)",
            value: "Senate::ID::James Vandermaas (D)",
            children: [],
          },
          {
            name: "Jim Risch (R)",
            value: "Senate::ID::Jim Risch (R)",
            children: [],
          },
        ],
      },
      {
        name: "IL",
        value: "Senate::IL",
        children: [
          {
            name: "Dick Durbin (D)",
            value: "Senate::IL::Dick Durbin (D)",
            children: [],
          },
          {
            name: "Mark Curran (R)",
            value: "Senate::IL::Mark Curran (R)",
            children: [],
          },
        ],
      },
      {
        name: "IA",
        value: "Senate::IA",
        children: [
          {
            name: "Michael Franken (D)",
            value: "Senate::IA::Michael Franken (D)",
            children: [],
          },
          {
            name: "Kimberly Graham (D)",
            value: "Senate::IA::Kimberly Graham (D)",
            children: [],
          },
          {
            name: "Theresa Greenfield (D)",
            value: "Senate::IA::Theresa Greenfield (D)",
            children: [],
          },
          {
            name: "Eddie Mauro (D)",
            value: "Senate::IA::Eddie Mauro (D)",
            children: [],
          },
          {
            name: "Cal Woods (D)",
            value: "Senate::IA::Cal Woods (D)",
            children: [],
          },
          {
            name: "Joni Ernst (R)",
            value: "Senate::IA::Joni Ernst (R)",
            children: [],
          },
        ],
      },
      {
        name: "KS",
        value: "Senate::KS",
        children: [
          {
            name: "Barbara Bollier (D)",
            value: "Senate::KS::Barbara Bollier (D)",
            children: [],
          },
          {
            name: "Usha Reddi (D)",
            value: "Senate::KS::Usha Reddi (D)",
            children: [],
          },
          {
            name: "Adam Smith (D)",
            value: "Senate::KS::Adam Smith (D)",
            children: [],
          },
          {
            name: "Robert Tillman (D)",
            value: "Senate::KS::Robert Tillman (D)",
            children: [],
          },
          {
            name: "Bob Hamilton (R)",
            value: "Senate::KS::Bob Hamilton (R)",
            children: [],
          },
          {
            name: "Kris Kobach (R)",
            value: "Senate::KS::Kris Kobach (R)",
            children: [],
          },
          {
            name: "Dave Lindstrom (R)",
            value: "Senate::KS::Dave Lindstrom (R)",
            children: [],
          },
          {
            name: "Roger Marshall (R)",
            value: "Senate::KS::Roger Marshall (R)",
            children: [],
          },
          {
            name: "Brian Matlock (R)",
            value: "Senate::KS::Brian Matlock (R)",
            children: [],
          },
          {
            name: "Susan Wagle (R)",
            value: "Senate::KS::Susan Wagle (R)",
            children: [],
          },
        ],
      },
      {
        name: "KY",
        value: "Senate::KY",
        children: [
          {
            name: "Jimmy Ausbrooks (D)",
            value: "Senate::KY::Jimmy Ausbrooks (D)",
            children: [],
          },
          {
            name: "Charles Booker (D)",
            value: "Senate::KY::Charles Booker (D)",
            children: [],
          },
          {
            name: "Mike Broihier (D)",
            value: "Senate::KY::Mike Broihier (D)",
            children: [],
          },
          {
            name: "Maggie Jo Hilliard (D)",
            value: "Senate::KY::Maggie Jo Hilliard (D)",
            children: [],
          },
          {
            name: "Andrew Maynard (D)",
            value: "Senate::KY::Andrew Maynard (D)",
            children: [],
          },
          {
            name: "Amy McGrath (D)",
            value: "Senate::KY::Amy McGrath (D)",
            children: [],
          },
          {
            name: "Eric Rothmuller (D)",
            value: "Senate::KY::Eric Rothmuller (D)",
            children: [],
          },
          {
            name: "John Sharpensteen (D)",
            value: "Senate::KY::John Sharpensteen (D)",
            children: [],
          },
          {
            name: "Bennie Smith (D)",
            value: "Senate::KY::Bennie Smith (D)",
            children: [],
          },
          {
            name: "Mary Ann Tobin (D)",
            value: "Senate::KY::Mary Ann Tobin (D)",
            children: [],
          },
          {
            name: "Mitch McConnell (D)",
            value: "Senate::KY::Mitch McConnell (D)",
            children: [],
          },
          {
            name: "Nicholas Alsager (D)",
            value: "Senate::KY::Nicholas Alsager (D)",
            children: [],
          },
          {
            name: "Wendell Crow (D)",
            value: "Senate::KY::Wendell Crow (D)",
            children: [],
          },
          {
            name: "Paul John Frangedakis (D)",
            value: "Senate::KY::Paul John Frangedakis (D)",
            children: [],
          },
          {
            name: "Louis Grider (D)",
            value: "Senate::KY::Louis Grider (D)",
            children: [],
          },
          {
            name: "Naren James (D)",
            value: "Senate::KY::Naren James (D)",
            children: [],
          },
          {
            name: "Kenneth Lowndes (D)",
            value: "Senate::KY::Kenneth Lowndes (D)",
            children: [],
          },
          {
            name: "C. Wesley Morgan (D)",
            value: "Senate::KY::C. Wesley Morgan (D)",
            children: [],
          },
        ],
      },
      {
        name: "LA",
        value: "Senate::LA",
        children: [
          {
            name: "Bill Cassidy (R)",
            value: "Senate::LA::Bill Cassidy (R)",
            children: [],
          },
          {
            name: "Antoine Pierce (D)",
            value: "Senate::LA::Antoine Pierce (D)",
            children: [],
          },
          {
            name: "Dartanyon Williams (D)",
            value: "Senate::LA::Dartanyon Williams (D)",
            children: [],
          },
        ],
      },
      {
        name: "ME",
        value: "Senate::ME",
        children: [
          {
            name: "Sara Gideon (D)",
            value: "Senate::ME::Sara Gideon (D)",
            children: [],
          },
          {
            name: "Bre Kidman (D)",
            value: "Senate::ME::Bre Kidman (D)",
            children: [],
          },
          {
            name: "Betsy Sweet (D)",
            value: "Senate::ME::Betsy Sweet (D)",
            children: [],
          },
          {
            name: "Susan Collin (R)",
            value: "Senate::ME::Susan Collin (R)",
            children: [],
          },
        ],
      },
      {
        name: "MA",
        value: "Senate::MA",
        children: [
          {
            name: "Edward J. Markey (D)",
            value: "Senate::MA::Edward J. Markey (D)",
            children: [],
          },
          {
            name: "Washington Blask (D)",
            value: "Senate::MA::Washington Blask (D)",
            children: [],
          },
          {
            name: "Joseph Kennedy III (D)",
            value: "Senate::MA::Joseph Kennedy III (D)",
            children: [],
          },
          {
            name: "Shiva Ayyadurai (R)",
            value: "Senate::MA::Shiva Ayyadurai (R)",
            children: [],
          },
          {
            name: "Kevin O'Connor (R)",
            value: "Senate::MA::Kevin O'Connor (R)",
            children: [],
          },
        ],
      },
      {
        name: "MI",
        value: "Senate::MI",
        children: [
          {
            name: "Gary Peters (D)",
            value: "Senate::MI::Gary Peters (D)",
            children: [],
          },
          {
            name: "Bob Carr (R)",
            value: "Senate::MI::Bob Carr (R)",
            children: [],
          },
          {
            name: "John James (R)",
            value: "Senate::MI::John James (R)",
            children: [],
          },
        ],
      },
      {
        name: "MN",
        value: "Senate::MN",
        children: [
          {
            name: "Tina Smith (D)",
            value: "Senate::MN::Tina Smith (D)",
            children: [],
          },
          {
            name: "Steve Carlson (D)",
            value: "Senate::MN::Steve Carlson (D)",
            children: [],
          },
          {
            name: "Paula Overby (D)",
            value: "Senate::MN::Paula Overby (D)",
            children: [],
          },
          {
            name: "Rob Barrett Jr. (R)",
            value: "Senate::MN::Rob Barrett Jr. (R)",
            children: [],
          },
          {
            name: "Christopher Chamberlin (R)",
            value: "Senate::MN::Christopher Chamberlin (R)",
            children: [],
          },
          {
            name: "Forest Hyatt (R)",
            value: "Senate::MN::Forest Hyatt (R)",
            children: [],
          },
          {
            name: "Jason Lewis (R)",
            value: "Senate::MN::Jason Lewis (R)",
            children: [],
          },
          {
            name: "Theron Preston Washington (R)",
            value: "Senate::MN::Theron Preston Washington (R)",
            children: [],
          },
        ],
      },
      {
        name: "MS",
        value: "Senate::MS",
        children: [
          {
            name: "Cindy Hyde-Smith (R)",
            value: "Senate::MS::Cindy Hyde-Smith (R)",
            children: [],
          },
          {
            name: "Mike Espy (D)",
            value: "Senate::MS::Mike Espy (D)",
            children: [],
          },
        ],
      },
      {
        name: "MT",
        value: "Senate::MT",
        children: [
          {
            name: "Steve Bullock (D)",
            value: "Senate::MT::Steve Bullock (D)",
            children: [],
          },
          {
            name: "John Mues (D)",
            value: "Senate::MT::John Mues (D)",
            children: [],
          },
          {
            name: "Steve Daines (R)",
            value: "Senate::MT::Steve Daines (R)",
            children: [],
          },
          {
            name: "John Driscoll (R)",
            value: "Senate::MT::John Driscoll (R)",
            children: [],
          },
          {
            name: "Daniel Larson (R)",
            value: "Senate::MT::Daniel Larson (R)",
            children: [],
          },
        ],
      },
      {
        name: "NE",
        value: "Senate::NE",
        children: [
          {
            name: "Ben Sasse (R)",
            value: "Senate::NE::Ben Sasse (R)",
            children: [],
          },
          {
            name: "Chris Janicek (D)",
            value: "Senate::NE::Chris Janicek (D)",
            children: [],
          },
        ],
      },
      {
        name: "NH",
        value: "Senate::NH",
        children: [
          {
            name: "Jeanne Shaheen (D)",
            value: "Senate::NH::Jeanne Shaheen (D)",
            children: [],
          },
          {
            name: "Don Bolduc (R)",
            value: "Senate::NH::Don Bolduc (R)",
            children: [],
          },
          {
            name: "Andy Martin (R)",
            value: "Senate::NH::Andy Martin (R)",
            children: [],
          },
          {
            name: "Bryant Messner (R)",
            value: "Senate::NH::Bryant Messner (R)",
            children: [],
          },
        ],
      },
      {
        name: "NJ",
        value: "Senate::NJ",
        children: [
          {
            name: "Cory Booker (D)",
            value: "Senate::NJ::Cory Booker (D)",
            children: [],
          },
          {
            name: "Lawernce Hamm (D)",
            value: "Senate::NJ::Lawernce Hamm (D)",
            children: [],
          },
          {
            name: "Eugene Anagnos (R)",
            value: "Senate::NJ::Eugene Anagnos (R)",
            children: [],
          },
          {
            name: "Tricia Flanagan (R)",
            value: "Senate::NJ::Tricia Flanagan (R)",
            children: [],
          },
          {
            name: "Rik Mehta (R)",
            value: "Senate::NJ::Rik Mehta (R)",
            children: [],
          },
          {
            name: "Natalie Rivera (R)",
            value: "Senate::NJ::Natalie Rivera (R)",
            children: [],
          },
          {
            name: "Hirsh Singh (R)",
            value: "Senate::NJ::Hirsh Singh (R)",
            children: [],
          },
        ],
      },
      {
        name: "NM",
        value: "Senate::NM",
        children: [
          {
            name: "Ben Ray Luján (D)",
            value: "Senate::NM::Ben Ray Luján (D)",
            children: [],
          },
          {
            name: "Gavin Clarkson (R)",
            value: "Senate::NM::Gavin Clarkson (R)",
            children: [],
          },
          {
            name: "Elisa Martinez (R)",
            value: "Senate::NM::Elisa Martinez (R)",
            children: [],
          },
          {
            name: "Mark Ronchetti (R)",
            value: "Senate::NM::Mark Ronchetti (R)",
            children: [],
          },
        ],
      },
      {
        name: "NC",
        value: "Senate::NC",
        children: [
          {
            name: "Thom Tillis (R)",
            value: "Senate::NC::Thom Tillis (R)",
            children: [],
          },
          {
            name: "Cal Cunningham (D)",
            value: "Senate::NC::Cal Cunningham (D)",
            children: [],
          },
        ],
      },
      {
        name: "OK",
        value: "Senate::OK",
        children: [
          {
            name: "Sheila Bilyeu (D)",
            value: "Senate::OK::Sheila Bilyeu (D)",
            children: [],
          },
          {
            name: "Elysabeth Britt (D)",
            value: "Senate::OK::Elysabeth Britt (D)",
            children: [],
          },
          {
            name: "Abby Broyles (D)",
            value: "Senate::OK::Abby Broyles (D)",
            children: [],
          },
          {
            name: "R.O. Joe Cassity (D)",
            value: "Senate::OK::R.O. Joe Cassity (D)",
            children: [],
          },
          {
            name: "Jim Inhofe (R)",
            value: "Senate::OK::Jim Inhofe (R)",
            children: [],
          },
          {
            name: "Neil Mavis (R)",
            value: "Senate::OK::Neil Mavis (R)",
            children: [],
          },
          {
            name: "JJ Stitt (R)",
            value: "Senate::OK::JJ Stitt (R)",
            children: [],
          },
          {
            name: "John Tompkins (R)",
            value: "Senate::OK::John Tompkins (R)",
            children: [],
          },
        ],
      },
      {
        name: "OR",
        value: "Senate::OR",
        children: [
          {
            name: "Jeff Merkley (D)",
            value: "Senate::OR::Jeff Merkley (D)",
            children: [],
          },
          {
            name: "Jo Rae Perkins (R)",
            value: "Senate::OR::Jo Rae Perkins (R)",
            children: [],
          },
          {
            name: "Paul Romero (R)",
            value: "Senate::OR::Paul Romero (R)",
            children: [],
          },
          {
            name: "Robert Schwartz (R)",
            value: "Senate::OR::Robert Schwartz (R)",
            children: [],
          },
          {
            name: "John Verbeek (R)",
            value: "Senate::OR::John Verbeek (R)",
            children: [],
          },
        ],
      },
      {
        name: "RI",
        value: "Senate::RI",
        children: [
          {
            name: "Jack Reed (D)",
            value: "Senate::RI::Jack Reed (D)",
            children: [],
          },
          {
            name: "Allen Waters (R)",
            value: "Senate::RI::Allen Waters (R)",
            children: [],
          },
        ],
      },
      {
        name: "SC",
        value: "Senate::SC",
        children: [
          {
            name: "Jamie Harrison (D)",
            value: "Senate::SC::Jamie Harrison (D)",
            children: [],
          },
          {
            name: "Lindsey Graham (R)",
            value: "Senate::SC::Lindsey Graham (R)",
            children: [],
          },
          {
            name: "Duke Buckner (R)",
            value: "Senate::SC::Duke Buckner (R)",
            children: [],
          },
          {
            name: "Michael LaPierre (R)",
            value: "Senate::SC::Michael LaPierre (R)",
            children: [],
          },
          {
            name: "Joe Reynolds (R)",
            value: "Senate::SC::Joe Reynolds (R)",
            children: [],
          },
        ],
      },
      {
        name: "SD",
        value: "Senate::SD",
        children: [
          {
            name: "Dan Ahlers (D)",
            value: "Senate::SD::Dan Ahlers (D)",
            children: [],
          },
          {
            name: "Mike Rounds (R)",
            value: "Senate::SD::Mike Rounds (R)",
            children: [],
          },
          {
            name: "Scyller Borglum (R)",
            value: "Senate::SD::Scyller Borglum (R)",
            children: [],
          },
        ],
      },
      {
        name: "TN",
        value: "Senate::TN",
        children: [
          {
            name: "Marquita Bradshaw (D)",
            value: "Senate::TN::Marquita Bradshaw (D)",
            children: [],
          },
          {
            name: "Gary Davis (D)",
            value: "Senate::TN::Gary Davis (D)",
            children: [],
          },
          {
            name: "Robin Kimbrough (D)",
            value: "Senate::TN::Robin Kimbrough (D)",
            children: [],
          },
          {
            name: "James Mackler (D)",
            value: "Senate::TN::James Mackler (D)",
            children: [],
          },
          {
            name: "Mark Pickrell (D)",
            value: "Senate::TN::Mark Pickrell (D)",
            children: [],
          },
          {
            name: "Clifford Adkins (R)",
            value: "Senate::TN::Clifford Adkins (R)",
            children: [],
          },
          {
            name: "Natisha Brooks (R)",
            value: "Senate::TN::Natisha Brooks (R)",
            children: [],
          },
          {
            name: "Byron Bush (R)",
            value: "Senate::TN::Byron Bush (R)",
            children: [],
          },
          {
            name: "Roy Cope (R)",
            value: "Senate::TN::Roy Cope (R)",
            children: [],
          },
          {
            name: "Terry Dicus (R)",
            value: "Senate::TN::Terry Dicus (R)",
            children: [],
          },
          {
            name: "Tom Emerson Jr. (R)",
            value: "Senate::TN::Tom Emerson Jr. (R)",
            children: [],
          },
          {
            name: "George Flinn Jr. (R)",
            value: "Senate::TN::George Flinn Jr. (R)",
            children: [],
          },
          {
            name: "Bill Hagerty (R)",
            value: "Senate::TN::Bill Hagerty (R)",
            children: [],
          },
          {
            name: "Jon Henry (R)",
            value: "Senate::TN::Jon Henry (R)",
            children: [],
          },
          {
            name: "Kent Morrell (R)",
            value: "Senate::TN::Kent Morrell (R)",
            children: [],
          },
          {
            name: "Glen Neal (R)",
            value: "Senate::TN::Glen Neal (R)",
            children: [],
          },
          {
            name: "John Osborne (R)",
            value: "Senate::TN::John Osborne (R)",
            children: [],
          },
          {
            name: "Aaron Pettigrew (R)",
            value: "Senate::TN::Aaron Pettigrew (R)",
            children: [],
          },
          {
            name: "David Schuster (R)",
            value: "Senate::TN::David Schuster (R)",
            children: [],
          },
          {
            name: "Manny Sethi (R)",
            value: "Senate::TN::Manny Sethi (R)",
            children: [],
          },
        ],
      },
      {
        name: "TX",
        value: "Senate::TX",
        children: [
          {
            name: "John Cornyn (R)",
            value: "Senate::TX::John Cornyn (R)",
            children: [],
          },
          {
            name: "Mary Jennings Hegar (D)",
            value: "Senate::TX::Mary Jennings Hegar (D)",
            children: [],
          },
          {
            name: "Royce West (D)",
            value: "Senate::TX::Royce West (D)",
            children: [],
          },
        ],
      },
      {
        name: "VA",
        value: "Senate::VA",
        children: [
          {
            name: "Mark Warner (D)",
            value: "Senate::VA::Mark Warner (D)",
            children: [],
          },
          {
            name: "Alissa Baldwin (R)",
            value: "Senate::VA::Alissa Baldwin (R)",
            children: [],
          },
          {
            name: "Daniel Gade (R)",
            value: "Senate::VA::Daniel Gade (R)",
            children: [],
          },
          {
            name: "Thomas Speciale (R)",
            value: "Senate::VA::Thomas Speciale (R)",
            children: [],
          },
        ],
      },
      {
        name: "WV",
        value: "Senate::WV",
        children: [
          {
            name: "Richard Ojeda (D)",
            value: "Senate::WV::Richard Ojeda (D)",
            children: [],
          },
          {
            name: "Richie Robb (D)",
            value: "Senate::WV::Richie Robb (D)",
            children: [],
          },
          {
            name: "Paula Jean Swearengin (D)",
            value: "Senate::WV::Paula Jean Swearengin (D)",
            children: [],
          },
          {
            name: "Shelley Moore Capito (R)",
            value: "Senate::WV::Shelley Moore Capito (R)",
            children: [],
          },
          {
            name: "Larry Butcher (R)",
            value: "Senate::WV::Larry Butcher (R)",
            children: [],
          },
          {
            name: "Allen Whitt (R)",
            value: "Senate::WV::Allen Whitt (R)",
            children: [],
          },
        ],
      },
      {
        name: "WY",
        value: "Senate::WY",
        children: [
          {
            name: "Merav Ben-David (D)",
            value: "Senate::WY::Merav Ben-David (D)",
            children: [],
          },
          {
            name: "Chuck Jagoda (D)",
            value: "Senate::WY::Chuck Jagoda (D)",
            children: [],
          },
          {
            name: "Yana Ludwig (D)",
            value: "Senate::WY::Yana Ludwig (D)",
            children: [],
          },
          {
            name: "Patrick Dotson (R)",
            value: "Senate::WY::Patrick Dotson (R)",
            children: [],
          },
          {
            name: "Cynthia Lummis (R)",
            value: "Senate::WY::Cynthia Lummis (R)",
            children: [],
          },
          {
            name: "Donna Rice (R)",
            value: "Senate::WY::Donna Rice (R)",
            children: [],
          },
          {
            name: "Robert Short (R)",
            value: "Senate::WY::Robert Short (R)",
            children: [],
          },
          {
            name: "Joshua Wheeler (R)",
            value: "Senate::WY::Joshua Wheeler (R)",
            children: [],
          },
        ],
      },
    ],
  },
  {
    name: "House",
    value: "House",
    children: [
      {
        name: "AL",
        value: "House::AL",
        children: [
          {
            name: "1",
            value: "House::AL::1",
            children: [
              {
                name: "James Averhart (D)",
                value: "House::AL::1::James Averhart (D)",
                children: [],
              },
              {
                name: "Kiani Gardner (D)",
                value: "House::AL::1::Kiani Gardner (D)",
                children: [],
              },
              {
                name: "Jerry Carl (R)",
                value: "House::AL::1::Jerry Carl (R)",
                children: [],
              },
              {
                name: "Bill Hightower (R)",
                value: "House::AL::1::Bill Hightower (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::AL::2",
            children: [
              {
                name: "Phyllis Harvey-Hall (D)",
                value: "House::AL::2::Phyllis Harvey-Hall (D)",
                children: [],
              },
              {
                name: "Jeff Coleman (R)",
                value: "House::AL::2::Jeff Coleman (R)",
                children: [],
              },
              {
                name: "Barry More (R)",
                value: "House::AL::2::Barry More (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::AL::3",
            children: [
              {
                name: "Mike Rogers (R)",
                value: "House::AL::3::Mike Rogers (R)",
                children: [],
              },
              {
                name: "Adia Winfrey (D)",
                value: "House::AL::3::Adia Winfrey (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::AL::4",
            children: [
              {
                name: "Robert Aderholt (R)",
                value: "House::AL::4::Robert Aderholt (R)",
                children: [],
              },
              {
                name: "Rick Neighbors (D)",
                value: "House::AL::4::Rick Neighbors (D)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::AL::5",
            children: [
              {
                name: "Mo Brooks (R)",
                value: "House::AL::5::Mo Brooks (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::AL::6",
            children: [
              {
                name: "Gary Palmer (R)",
                value: "House::AL::6::Gary Palmer (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::AL::7",
            children: [
              {
                name: "Terri Sewell (D)",
                value: "House::AL::7::Terri Sewell (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "AK",
        value: "House::AK",
        children: [
          {
            name: 1,
            value: "House::AK::1",
            children: [
              {
                name: "Don Young (R)",
                value: "House::AK::1::Don Young (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "AZ",
        value: "House::AZ",
        children: [
          {
            name: "1",
            value: "House::AZ::1",
            children: [
              {
                name: "Tom O'Halleran (D)",
                value: "House::AZ::1::Tom O'Halleran (D)",
                children: [],
              },
              {
                name: "Eva Putzova (D)",
                value: "House::AZ::1::Eva Putzova (D)",
                children: [],
              },
              {
                name: "Nolan Reidhead (R)",
                value: "House::AZ::1::Nolan Reidhead (R)",
                children: [],
              },
              {
                name: "Tiffany Shedd (R)",
                value: "House::AZ::1::Tiffany Shedd (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::AZ::2",
            children: [
              {
                name: "Ann Kirkpatrick (D)",
                value: "House::AZ::2::Ann Kirkpatrick (D)",
                children: [],
              },
              {
                name: "Peter Quilter (D)",
                value: "House::AZ::2::Peter Quilter (D)",
                children: [],
              },
              {
                name: "Brandon Martin (R)",
                value: "House::AZ::2::Brandon Martin (R)",
                children: [],
              },
              {
                name: "Joseph Morgan (R)",
                value: "House::AZ::2::Joseph Morgan (R)",
                children: [],
              },
              {
                name: "Noran Ruden (R)",
                value: "House::AZ::2::Noran Ruden (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::AZ::3",
            children: [
              {
                name: "Raul Grijalva (D)",
                value: "House::AZ::3::Raul Grijalva (D)",
                children: [],
              },
              {
                name: "Daniel Wood (R)",
                value: "House::AZ::3::Daniel Wood (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::AZ::4",
            children: [
              {
                name: "Delina DiSanto (D)",
                value: "House::AZ::4::Delina DiSanto (D)",
                children: [],
              },
              {
                name: "Stuart Starky (D)",
                value: "House::AZ::4::Stuart Starky (D)",
                children: [],
              },
              {
                name: "Paul Gosar (R)",
                value: "House::AZ::4::Paul Gosar (R)",
                children: [],
              },
              {
                name: "Annie Marie Ward (R)",
                value: "House::AZ::4::Annie Marie Ward (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::AZ::5",
            children: [
              {
                name: "Joan Greene (D)",
                value: "House::AZ::5::Joan Greene (D)",
                children: [],
              },
              {
                name: "Jonathan Ireland (D)",
                value: "House::AZ::5::Jonathan Ireland (D)",
                children: [],
              },
              {
                name: "Javier Ramos (D)",
                value: "House::AZ::5::Javier Ramos (D)",
                children: [],
              },
              {
                name: "Andy Biggs (R)",
                value: "House::AZ::5::Andy Biggs (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::AZ::6",
            children: [
              {
                name: "Karl Gentles (D)",
                value: "House::AZ::6::Karl Gentles (D)",
                children: [],
              },
              {
                name: "Anita Malik (D)",
                value: "House::AZ::6::Anita Malik (D)",
                children: [],
              },
              {
                name: "Hiral Tipirneni (D)",
                value: "House::AZ::6::Hiral Tipirneni (D)",
                children: [],
              },
              {
                name: "David Schweikert (R)",
                value: "House::AZ::6::David Schweikert (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::AZ::7",
            children: [
              {
                name: "Ruben Gallego (D)",
                value: "House::AZ::7::Ruben Gallego (D)",
                children: [],
              },
              {
                name: "Josh Barnett (R)",
                value: "House::AZ::7::Josh Barnett (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::AZ::8",
            children: [
              {
                name: "Michael Muscato (D)",
                value: "House::AZ::8::Michael Muscato (D)",
                children: [],
              },
              {
                name: "Bob Musselwhite (D)",
                value: "House::AZ::8::Bob Musselwhite (D)",
                children: [],
              },
              {
                name: "Bob Olsen (D)",
                value: "House::AZ::8::Bob Olsen (D)",
                children: [],
              },
              {
                name: "Debbie Lesko (R)",
                value: "House::AZ::8::Debbie Lesko (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::AZ::9",
            children: [
              {
                name: "Greg Stanton (D)",
                value: "House::AZ::9::Greg Stanton (D)",
                children: [],
              },
              {
                name: "Talia Fuentes-Wolfe (D)",
                value: "House::AZ::9::Talia Fuentes-Wolfe (D)",
                children: [],
              },
              {
                name: "David Giles (R)",
                value: "House::AZ::9::David Giles (R)",
                children: [],
              },
              {
                name: "Sam Huang (R)",
                value: "House::AZ::9::Sam Huang (R)",
                children: [],
              },
              {
                name: "Nicholas Tutora (R)",
                value: "House::AZ::9::Nicholas Tutora (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "AR",
        value: "House::AR",
        children: [
          {
            name: "1",
            value: "House::AR::1",
            children: [
              {
                name: "Rick Crawford (R)",
                value: "House::AR::1::Rick Crawford (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::AR::2",
            children: [
              {
                name: "French Hill (R)",
                value: "House::AR::2::French Hill (R)",
                children: [],
              },
              {
                name: "Joyce Elliott (D)",
                value: "House::AR::2::Joyce Elliott (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::AR::3",
            children: [
              {
                name: "Steve Womack (R)",
                value: "House::AR::3::Steve Womack (R)",
                children: [],
              },
              {
                name: "Celeste Willaims (D)",
                value: "House::AR::3::Celeste Willaims (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::AR::4",
            children: [
              {
                name: "Bruce Westerman (R)",
                value: "House::AR::4::Bruce Westerman (R)",
                children: [],
              },
              {
                name: "Willaim Hanson (D)",
                value: "House::AR::4::Willaim Hanson (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "CA",
        value: "House::CA",
        children: [
          {
            name: "1",
            value: "House::CA::1",
            children: [
              {
                name: "Doug LaMalfa (R)",
                value: "House::CA::1::Doug LaMalfa (R)",
                children: [],
              },
              {
                name: "Audrey Denney (D)",
                value: "House::CA::1::Audrey Denney (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::CA::2",
            children: [
              {
                name: "Jared Huffman (D)",
                value: "House::CA::2::Jared Huffman (D)",
                children: [],
              },
              {
                name: "Dale Mensing (R)",
                value: "House::CA::2::Dale Mensing (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::CA::3",
            children: [
              {
                name: "John Garamendi (D)",
                value: "House::CA::3::John Garamendi (D)",
                children: [],
              },
              {
                name: "Tamika Hamilton (R)",
                value: "House::CA::3::Tamika Hamilton (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::CA::4",
            children: [
              {
                name: "Tom McClintock (R)",
                value: "House::CA::4::Tom McClintock (R)",
                children: [],
              },
              {
                name: "Brynne Kennedy (D)",
                value: "House::CA::4::Brynne Kennedy (D)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::CA::5",
            children: [
              {
                name: "Mike Thompson (D)",
                value: "House::CA::5::Mike Thompson (D)",
                children: [],
              },
              {
                name: "Scott Giblin (R)",
                value: "House::CA::5::Scott Giblin (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::CA::6",
            children: [
              {
                name: "Dorsi Matsu (D)",
                value: "House::CA::6::Dorsi Matsu (D)",
                children: [],
              },
              {
                name: "Chirs Bish (R)",
                value: "House::CA::6::Chirs Bish (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::CA::7",
            children: [
              {
                name: "Ami Bera (D)",
                value: "House::CA::7::Ami Bera (D)",
                children: [],
              },
              {
                name: "Buzz Patterson (R)",
                value: "House::CA::7::Buzz Patterson (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::CA::8",
            children: [
              {
                name: "Chris Bubser (D)",
                value: "House::CA::8::Chris Bubser (D)",
                children: [],
              },
              {
                name: "Jay Overnolte (R)",
                value: "House::CA::8::Jay Overnolte (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::CA::9",
            children: [
              {
                name: "Jerry McNerney (D)",
                value: "House::CA::9::Jerry McNerney (D)",
                children: [],
              },
              {
                name: "Antonio Amador (R)",
                value: "House::CA::9::Antonio Amador (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::CA::10",
            children: [
              {
                name: "Josh Harder (D)",
                value: "House::CA::10::Josh Harder (D)",
                children: [],
              },
              {
                name: "Ted Howze (R)",
                value: "House::CA::10::Ted Howze (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::CA::11",
            children: [
              {
                name: "Mark DeSaulnier (D)",
                value: "House::CA::11::Mark DeSaulnier (D)",
                children: [],
              },
              {
                name: "Nisha Sharma (R)",
                value: "House::CA::11::Nisha Sharma (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::CA::12",
            children: [
              {
                name: "Nancy Pelosi (D)",
                value: "House::CA::12::Nancy Pelosi (D)",
                children: [],
              },
              {
                name: "Shahid Buttar (D)",
                value: "House::CA::12::Shahid Buttar (D)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::CA::13",
            children: [
              {
                name: "Barbara Lee (D)",
                value: "House::CA::13::Barbara Lee (D)",
                children: [],
              },
              {
                name: "Nikka Piterman (R)",
                value: "House::CA::13::Nikka Piterman (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::CA::14",
            children: [
              {
                name: "Jackie Speier (D)",
                value: "House::CA::14::Jackie Speier (D)",
                children: [],
              },
              {
                name: "Ran Petel (R)",
                value: "House::CA::14::Ran Petel (R)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::CA::15",
            children: [
              {
                name: "Eric Swalwell (D)",
                value: "House::CA::15::Eric Swalwell (D)",
                children: [],
              },
              {
                name: "Alison Hayden (R)",
                value: "House::CA::15::Alison Hayden (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::CA::16",
            children: [
              {
                name: "Jim Costa (D)",
                value: "House::CA::16::Jim Costa (D)",
                children: [],
              },
              {
                name: "Kevin Cookingham (R)",
                value: "House::CA::16::Kevin Cookingham (R)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::CA::17",
            children: [
              {
                name: "Ro Khanna (D)",
                value: "House::CA::17::Ro Khanna (D)",
                children: [],
              },
              {
                name: "Ritesh Tandon (R)",
                value: "House::CA::17::Ritesh Tandon (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::CA::18",
            children: [
              {
                name: "Anna Eshoo (D)",
                value: "House::CA::18::Anna Eshoo (D)",
                children: [],
              },
              {
                name: "Rishi Kumar (D)",
                value: "House::CA::18::Rishi Kumar (D)",
                children: [],
              },
            ],
          },
          {
            name: "19",
            value: "House::CA::19",
            children: [
              {
                name: "Zoe Lofgren (D)",
                value: "House::CA::19::Zoe Lofgren (D)",
                children: [],
              },
              {
                name: "Justin Aguilera (R)",
                value: "House::CA::19::Justin Aguilera (R)",
                children: [],
              },
            ],
          },
          {
            name: "20",
            value: "House::CA::20",
            children: [
              {
                name: "Jimmy Panetta (D)",
                value: "House::CA::20::Jimmy Panetta (D)",
                children: [],
              },
              {
                name: "Jeff Gorman (R)",
                value: "House::CA::20::Jeff Gorman (R)",
                children: [],
              },
            ],
          },
          {
            name: "21",
            value: "House::CA::21",
            children: [
              {
                name: "TJ Cox (D)",
                value: "House::CA::21::TJ Cox (D)",
                children: [],
              },
              {
                name: "David G. Valadao (R)",
                value: "House::CA::21::David G. Valadao (R)",
                children: [],
              },
            ],
          },
          {
            name: "22",
            value: "House::CA::22",
            children: [
              {
                name: "Devin Nunes (R)",
                value: "House::CA::22::Devin Nunes (R)",
                children: [],
              },
              {
                name: "Phil Arballo (D)",
                value: "House::CA::22::Phil Arballo (D)",
                children: [],
              },
            ],
          },
          {
            name: "23",
            value: "House::CA::23",
            children: [
              {
                name: "Kevin McCarthy (R)",
                value: "House::CA::23::Kevin McCarthy (R)",
                children: [],
              },
              {
                name: "Kim Mangone (D)",
                value: "House::CA::23::Kim Mangone (D)",
                children: [],
              },
            ],
          },
          {
            name: "24",
            value: "House::CA::24",
            children: [
              {
                name: "Slud Carbajal (D)",
                value: "House::CA::24::Slud Carbajal (D)",
                children: [],
              },
              {
                name: "Andy Caldwell (R)",
                value: "House::CA::24::Andy Caldwell (R)",
                children: [],
              },
            ],
          },
          {
            name: "25",
            value: "House::CA::25",
            children: [
              {
                name: "Christy Smith (D)",
                value: "House::CA::25::Christy Smith (D)",
                children: [],
              },
              {
                name: "Mike Garcia (R)",
                value: "House::CA::25::Mike Garcia (R)",
                children: [],
              },
            ],
          },
          {
            name: "26",
            value: "House::CA::26",
            children: [
              {
                name: "Julie Brownley (D)",
                value: "House::CA::26::Julie Brownley (D)",
                children: [],
              },
              {
                name: "Ronda Baldwin-Kennedy (R)",
                value: "House::CA::26::Ronda Baldwin-Kennedy (R)",
                children: [],
              },
            ],
          },
          {
            name: "27",
            value: "House::CA::27",
            children: [
              {
                name: "Judy Chu (D)",
                value: "House::CA::27::Judy Chu (D)",
                children: [],
              },
              {
                name: "Johnny Nalbandian (R)",
                value: "House::CA::27::Johnny Nalbandian (R)",
                children: [],
              },
            ],
          },
          {
            name: "28",
            value: "House::CA::28",
            children: [
              {
                name: "Adam Schiff (D)",
                value: "House::CA::28::Adam Schiff (D)",
                children: [],
              },
              {
                name: "Eric Early (R)",
                value: "House::CA::28::Eric Early (R)",
                children: [],
              },
            ],
          },
          {
            name: "29",
            value: "House::CA::29",
            children: [
              {
                name: "Tony Cárdenas (D)",
                value: "House::CA::29::Tony Cárdenas (D)",
                children: [],
              },
              {
                name: "Angelica Dueñas (D)",
                value: "House::CA::29::Angelica Dueñas (D)",
                children: [],
              },
            ],
          },
          {
            name: "30",
            value: "House::CA::30",
            children: [
              {
                name: "Brad Sherman (D)",
                value: "House::CA::30::Brad Sherman (D)",
                children: [],
              },
              {
                name: "Mark Reed (R)",
                value: "House::CA::30::Mark Reed (R)",
                children: [],
              },
            ],
          },
          {
            name: "31",
            value: "House::CA::31",
            children: [
              {
                name: "Peter Aguilar (D)",
                value: "House::CA::31::Peter Aguilar (D)",
                children: [],
              },
              {
                name: "Agnes Gibboney (R)",
                value: "House::CA::31::Agnes Gibboney (R)",
                children: [],
              },
            ],
          },
          {
            name: "32",
            value: "House::CA::32",
            children: [
              {
                name: "Grace Napolitano (D)",
                value: "House::CA::32::Grace Napolitano (D)",
                children: [],
              },
              {
                name: "Joshua Scott (R)",
                value: "House::CA::32::Joshua Scott (R)",
                children: [],
              },
            ],
          },
          {
            name: "33",
            value: "House::CA::33",
            children: [
              {
                name: "James P. Breadley (R)",
                value: "House::CA::33::James P. Breadley (R)",
                children: [],
              },
            ],
          },
          {
            name: "34",
            value: "House::CA::34",
            children: [
              {
                name: "Jimmy Gomez (D)",
                value: "House::CA::34::Jimmy Gomez (D)",
                children: [],
              },
              {
                name: "David Kim (D)",
                value: "House::CA::34::David Kim (D)",
                children: [],
              },
            ],
          },
          {
            name: "35",
            value: "House::CA::35",
            children: [
              {
                name: "Norma Torres (D)",
                value: "House::CA::35::Norma Torres (D)",
                children: [],
              },
              {
                name: "Mike Cargile (R)",
                value: "House::CA::35::Mike Cargile (R)",
                children: [],
              },
            ],
          },
          {
            name: "36",
            value: "House::CA::36",
            children: [
              {
                name: "Rual Ruiz (D)",
                value: "House::CA::36::Rual Ruiz (D)",
                children: [],
              },
              {
                name: "Erin Cruz (R)",
                value: "House::CA::36::Erin Cruz (R)",
                children: [],
              },
            ],
          },
          {
            name: "37 ",
            value: "House::CA::37 ",
            children: [
              {
                name: "Karen Bass (D)",
                value: "House::CA::37 ::Karen Bass (D)",
                children: [],
              },
              {
                name: "Errol Webber (R)",
                value: "House::CA::37 ::Errol Webber (R)",
                children: [],
              },
            ],
          },
          {
            name: "38",
            value: "House::CA::38",
            children: [
              {
                name: "Linda Sánchez (D)",
                value: "House::CA::38::Linda Sánchez (D)",
                children: [],
              },
              {
                name: "Michael Tolar (D)",
                value: "House::CA::38::Michael Tolar (D)",
                children: [],
              },
            ],
          },
          {
            name: "39",
            value: "House::CA::39",
            children: [
              {
                name: "Gil Cisneros (D)",
                value: "House::CA::39::Gil Cisneros (D)",
                children: [],
              },
              {
                name: "Young Kim (R)",
                value: "House::CA::39::Young Kim (R)",
                children: [],
              },
            ],
          },
          {
            name: "40",
            value: "House::CA::40",
            children: [
              {
                name: "Lucille Roybal-Allard (D)",
                value: "House::CA::40::Lucille Roybal-Allard (D)",
                children: [],
              },
              {
                name: "C. Antonio Delgado (R)",
                value: "House::CA::40::C. Antonio Delgado (R)",
                children: [],
              },
            ],
          },
          {
            name: "41",
            value: "House::CA::41",
            children: [
              {
                name: "Mark Takano (D)",
                value: "House::CA::41::Mark Takano (D)",
                children: [],
              },
              {
                name: "Aja Smtih (R)",
                value: "House::CA::41::Aja Smtih (R)",
                children: [],
              },
            ],
          },
          {
            name: "42",
            value: "House::CA::42",
            children: [
              {
                name: "Ken Calvert (R)",
                value: "House::CA::42::Ken Calvert (R)",
                children: [],
              },
              {
                name: "William O'Mara (D)",
                value: "House::CA::42::William O'Mara (D)",
                children: [],
              },
            ],
          },
          {
            name: "43",
            value: "House::CA::43",
            children: [
              {
                name: "Maxine Waters (D)",
                value: "House::CA::43::Maxine Waters (D)",
                children: [],
              },
              {
                name: "Joe Collins (R)",
                value: "House::CA::43::Joe Collins (R)",
                children: [],
              },
            ],
          },
          {
            name: "44",
            value: "House::CA::44",
            children: [
              {
                name: "Nanette Barragàn (D)",
                value: "House::CA::44::Nanette Barragàn (D)",
                children: [],
              },
              {
                name: "Analilia Joya (D)",
                value: "House::CA::44::Analilia Joya (D)",
                children: [],
              },
            ],
          },
          {
            name: "45",
            value: "House::CA::45",
            children: [
              {
                name: "Katie Porter (D)",
                value: "House::CA::45::Katie Porter (D)",
                children: [],
              },
              {
                name: "Greg Raths (R)",
                value: "House::CA::45::Greg Raths (R)",
                children: [],
              },
            ],
          },
          {
            name: "46",
            value: "House::CA::46",
            children: [
              {
                name: "Lou Correa (D)",
                value: "House::CA::46::Lou Correa (D)",
                children: [],
              },
              {
                name: "James Waters (R)",
                value: "House::CA::46::James Waters (R)",
                children: [],
              },
            ],
          },
          {
            name: "47",
            value: "House::CA::47",
            children: [
              {
                name: "Alan Lowenthal (D)",
                value: "House::CA::47::Alan Lowenthal (D)",
                children: [],
              },
              {
                name: "John Briscoe (R)",
                value: "House::CA::47::John Briscoe (R)",
                children: [],
              },
            ],
          },
          {
            name: "48",
            value: "House::CA::48",
            children: [
              {
                name: "Harley Rouda (D)",
                value: "House::CA::48::Harley Rouda (D)",
                children: [],
              },
              {
                name: "Michelle Steel (R)",
                value: "House::CA::48::Michelle Steel (R)",
                children: [],
              },
            ],
          },
          {
            name: "49",
            value: "House::CA::49",
            children: [
              {
                name: "Mike Levin (D)",
                value: "House::CA::49::Mike Levin (D)",
                children: [],
              },
              {
                name: "Brian Maryott (R)",
                value: "House::CA::49::Brian Maryott (R)",
                children: [],
              },
            ],
          },
          {
            name: "50",
            value: "House::CA::50",
            children: [
              {
                name: "Ammar Campa-Najjar (D)",
                value: "House::CA::50::Ammar Campa-Najjar (D)",
                children: [],
              },
              {
                name: "Darrell Issa (R)",
                value: "House::CA::50::Darrell Issa (R)",
                children: [],
              },
            ],
          },
          {
            name: "51",
            value: "House::CA::51",
            children: [
              {
                name: "Juan Vargas (D)",
                value: "House::CA::51::Juan Vargas (D)",
                children: [],
              },
              {
                name: "Juan Hidalgo (R)",
                value: "House::CA::51::Juan Hidalgo (R)",
                children: [],
              },
            ],
          },
          {
            name: "52",
            value: "House::CA::52",
            children: [
              {
                name: "Scott Peters (D)",
                value: "House::CA::52::Scott Peters (D)",
                children: [],
              },
              {
                name: "Jim Debello (R)",
                value: "House::CA::52::Jim Debello (R)",
                children: [],
              },
            ],
          },
          {
            name: "53",
            value: "House::CA::53",
            children: [
              {
                name: "Georgette Gomez (D)",
                value: "House::CA::53::Georgette Gomez (D)",
                children: [],
              },
              {
                name: "Sara Jacobs (D)",
                value: "House::CA::53::Sara Jacobs (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "CA",
        value: "House::CA",
        children: [
          {
            name: "33",
            value: "House::CA::33",
            children: [
              {
                name: "Ted Lieu (D)",
                value: "House::CA::33::Ted Lieu (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "CO",
        value: "House::CO",
        children: [
          {
            name: "1",
            value: "House::CO::1",
            children: [
              {
                name: "Diana DeGette (D)",
                value: "House::CO::1::Diana DeGette (D)",
                children: [],
              },
              {
                name: "Shane Boiling (R)",
                value: "House::CO::1::Shane Boiling (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::CO::2",
            children: [
              {
                name: "Joe Neguse (D)",
                value: "House::CO::2::Joe Neguse (D)",
                children: [],
              },
              {
                name: "Charles Winn (R)",
                value: "House::CO::2::Charles Winn (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::CO::3",
            children: [
              {
                name: "Diane Mitsch Bush (D)",
                value: "House::CO::3::Diane Mitsch Bush (D)",
                children: [],
              },
              {
                name: "James Iacino (D)",
                value: "House::CO::3::James Iacino (D)",
                children: [],
              },
              {
                name: "Scott Tipton (R)",
                value: "House::CO::3::Scott Tipton (R)",
                children: [],
              },
              {
                name: "Lauren Boebert (R)",
                value: "House::CO::3::Lauren Boebert (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::CO::4",
            children: [
              {
                name: "Isaac McCorkle (D)",
                value: "House::CO::4::Isaac McCorkle (D)",
                children: [],
              },
              {
                name: "Ken Buck (R)",
                value: "House::CO::4::Ken Buck (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::CO::5",
            children: [
              {
                name: "Jillian Freeland (D)",
                value: "House::CO::5::Jillian Freeland (D)",
                children: [],
              },
              {
                name: "Doug Lamborn (R)",
                value: "House::CO::5::Doug Lamborn (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::CO::6",
            children: [
              {
                name: "Jason Crow (D)",
                value: "House::CO::6::Jason Crow (D)",
                children: [],
              },
              {
                name: "Steve House (R)",
                value: "House::CO::6::Steve House (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::CO::7",
            children: [
              {
                name: "Ed Perlmutter (D)",
                value: "House::CO::7::Ed Perlmutter (D)",
                children: [],
              },
              {
                name: "Casper Stockham (R)",
                value: "House::CO::7::Casper Stockham (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "CT",
        value: "House::CT",
        children: [
          {
            name: "1",
            value: "House::CT::1",
            children: [
              {
                name: "Betty Dang (R)",
                value: "House::CT::1::Betty Dang (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::CT::2",
            children: [
              {
                name: "Joe Courtney (D)",
                value: "House::CT::2::Joe Courtney (D)",
                children: [],
              },
              {
                name: "Justin Anderson (R)",
                value: "House::CT::2::Justin Anderson (R)",
                children: [],
              },
              {
                name: "Thomas Gilmer (R)",
                value: "House::CT::2::Thomas Gilmer (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::CT::3",
            children: [
              {
                name: "Margaret Streicker (R)",
                value: "House::CT::3::Margaret Streicker (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::CT::4",
            children: [
              {
                name: "Jim Himes (D)",
                value: "House::CT::4::Jim Himes (D)",
                children: [],
              },
              {
                name: "Brian Merlen (D)",
                value: "House::CT::4::Brian Merlen (D)",
                children: [],
              },
              {
                name: "TJ Elgin (R)",
                value: "House::CT::4::TJ Elgin (R)",
                children: [],
              },
              {
                name: "Michael Goldstein (R)",
                value: "House::CT::4::Michael Goldstein (R)",
                children: [],
              },
              {
                name: "Jonathan Riddle (R)",
                value: "House::CT::4::Jonathan Riddle (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::CT::5",
            children: [
              {
                name: "Jahana Hayes (D)",
                value: "House::CT::5::Jahana Hayes (D)",
                children: [],
              },
              {
                name: "Peter Robert Barresi (R)",
                value: "House::CT::5::Peter Robert Barresi (R)",
                children: [],
              },
              {
                name: "Ryan Meehan (R)",
                value: "House::CT::5::Ryan Meehan (R)",
                children: [],
              },
              {
                name: "Ruben Rodriguez (R)",
                value: "House::CT::5::Ruben Rodriguez (R)",
                children: [],
              },
              {
                name: "David Xavier Sullivan (R)",
                value: "House::CT::5::David Xavier Sullivan (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "DE",
        value: "House::DE",
        children: [
          {
            name: 1,
            value: "House::DE::1",
            children: [
              {
                name: "Lisa Blunt Rochester (D)",
                value: "House::DE::1::Lisa Blunt Rochester (D)",
                children: [],
              },
              {
                name: "Anne Kerner (D)",
                value: "House::DE::1::Anne Kerner (D)",
                children: [],
              },
              {
                name: "Matthew Morris (R)",
                value: "House::DE::1::Matthew Morris (R)",
                children: [],
              },
              {
                name: "Lee Murphy (R)",
                value: "House::DE::1::Lee Murphy (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "FL",
        value: "House::FL",
        children: [
          {
            name: "1",
            value: "House::FL::1",
            children: [
              {
                name: "Phil Ehr (D)",
                value: "House::FL::1::Phil Ehr (D)",
                children: [],
              },
              {
                name: "Matt Gaetz (R)",
                value: "House::FL::1::Matt Gaetz (R)",
                children: [],
              },
              {
                name: "Greg Merk (R)",
                value: "House::FL::1::Greg Merk (R)",
                children: [],
              },
              {
                name: "John Mills (R)",
                value: "House::FL::1::John Mills (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::FL::2",
            children: [
              {
                name: "Kristy Thripp (D)",
                value: "House::FL::2::Kristy Thripp (D)",
                children: [],
              },
              {
                name: "Neal Dunn (R)",
                value: "House::FL::2::Neal Dunn (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::FL::3",
            children: [
              {
                name: "Adam Christensen (D)",
                value: "House::FL::3::Adam Christensen (D)",
                children: [],
              },
              {
                name: "Philip Dodds (D)",
                value: "House::FL::3::Philip Dodds (D)",
                children: [],
              },
              {
                name: "Tom Wells (D)",
                value: "House::FL::3::Tom Wells (D)",
                children: [],
              },
              {
                name: "Kat Cammack (R)",
                value: "House::FL::3::Kat Cammack (R)",
                children: [],
              },
              {
                name: "Ryan Chamberlin (R)",
                value: "House::FL::3::Ryan Chamberlin (R)",
                children: [],
              },
              {
                name: "Todd Chase (R)",
                value: "House::FL::3::Todd Chase (R)",
                children: [],
              },
              {
                name: "Bill Engelbrecht (R)",
                value: "House::FL::3::Bill Engelbrecht (R)",
                children: [],
              },
              {
                name: "Joe Dallas Millado (R)",
                value: "House::FL::3::Joe Dallas Millado (R)",
                children: [],
              },
              {
                name: "Amy Pope Wells (R)",
                value: "House::FL::3::Amy Pope Wells (R)",
                children: [],
              },
              {
                name: "Gavin Rollins (R)",
                value: "House::FL::3::Gavin Rollins (R)",
                children: [],
              },
              {
                name: "Judson Sapp (R)",
                value: "House::FL::3::Judson Sapp (R)",
                children: [],
              },
              {
                name: "Edgardo Silva (R)",
                value: "House::FL::3::Edgardo Silva (R)",
                children: [],
              },
              {
                name: "James St. George (R)",
                value: "House::FL::3::James St. George (R)",
                children: [],
              },
              {
                name: "David Theus (R)",
                value: "House::FL::3::David Theus (R)",
                children: [],
              },
            ],
          },
          {
            name: "4 ",
            value: "House::FL::4 ",
            children: [
              {
                name: "Donna Deegan (D)",
                value: "House::FL::4 ::Donna Deegan (D)",
                children: [],
              },
              {
                name: "John Rutherford (R)",
                value: "House::FL::4 ::John Rutherford (R)",
                children: [],
              },
              {
                name: "Erick Aguilar (R)",
                value: "House::FL::4 ::Erick Aguilar (R)",
                children: [],
              },
              {
                name: "Gary Koniz (R)",
                value: "House::FL::4 ::Gary Koniz (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::FL::5",
            children: [
              {
                name: "Alfred Lawson (D)",
                value: "House::FL::5::Alfred Lawson (D)",
                children: [],
              },
              {
                name: "Albert Chester (D)",
                value: "House::FL::5::Albert Chester (D)",
                children: [],
              },
              {
                name: "Gary Adler (R)",
                value: "House::FL::5::Gary Adler (R)",
                children: [],
              },
              {
                name: "Roger Wagoner (R)",
                value: "House::FL::5::Roger Wagoner (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::FL::6",
            children: [
              {
                name: "Clinton Curtis (D)",
                value: "House::FL::6::Clinton Curtis (D)",
                children: [],
              },
              {
                name: "Alan Grayson (D)",
                value: "House::FL::6::Alan Grayson (D)",
                children: [],
              },
              {
                name: "Richard Thripp (D)",
                value: "House::FL::6::Richard Thripp (D)",
                children: [],
              },
              {
                name: "Michael Waltz (R)",
                value: "House::FL::6::Michael Waltz (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::FL::7",
            children: [
              {
                name: "Stephanie Murphy (D)",
                value: "House::FL::7::Stephanie Murphy (D)",
                children: [],
              },
              {
                name: "Richard Goble (R)",
                value: "House::FL::7::Richard Goble (R)",
                children: [],
              },
              {
                name: "Leo Valentin (R)",
                value: "House::FL::7::Leo Valentin (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::FL::8",
            children: [
              {
                name: "Jim Kennedy (D)",
                value: "House::FL::8::Jim Kennedy (D)",
                children: [],
              },
              {
                name: "Bill Posey (R)",
                value: "House::FL::8::Bill Posey (R)",
                children: [],
              },
              {
                name: "Scott Caine (R)",
                value: "House::FL::8::Scott Caine (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::FL::9",
            children: [
              {
                name: "Darren Soto (D)",
                value: "House::FL::9::Darren Soto (D)",
                children: [],
              },
              {
                name: "Jose Castillo (R)",
                value: "House::FL::9::Jose Castillo (R)",
                children: [],
              },
              {
                name: "Bill Olsen (R)",
                value: "House::FL::9::Bill Olsen (R)",
                children: [],
              },
              {
                name: "Sergio Ortiz (R)",
                value: "House::FL::9::Sergio Ortiz (R)",
                children: [],
              },
              {
                name: "Christopher Wright (R)",
                value: "House::FL::9::Christopher Wright (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::FL::10",
            children: [
              {
                name: "Val Demings (D)",
                value: "House::FL::10::Val Demings (D)",
                children: [],
              },
              {
                name: "Sufiyah Yasmine (D)",
                value: "House::FL::10::Sufiyah Yasmine (D)",
                children: [],
              },
              {
                name: "Vennia Francois (R)",
                value: "House::FL::10::Vennia Francois (R)",
                children: [],
              },
              {
                name: "Willie Montague (R)",
                value: "House::FL::10::Willie Montague (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::FL::11",
            children: [
              {
                name: "Dana Cottrell (D)",
                value: "House::FL::11::Dana Cottrell (D)",
                children: [],
              },
              {
                name: "Daniel Webster (R)",
                value: "House::FL::11::Daniel Webster (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::FL::12",
            children: [
              {
                name: "Kimberly Walker (D)",
                value: "House::FL::12::Kimberly Walker (D)",
                children: [],
              },
              {
                name: "Gus M. Bilirakis (R)",
                value: "House::FL::12::Gus M. Bilirakis (R)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::FL::13",
            children: [
              {
                name: "Charlie Crist (D)",
                value: "House::FL::13::Charlie Crist (D)",
                children: [],
              },
              {
                name: "George Buck (R)",
                value: "House::FL::13::George Buck (R)",
                children: [],
              },
              {
                name: "Jacob Currnow (R)",
                value: "House::FL::13::Jacob Currnow (R)",
                children: [],
              },
              {
                name: "Sheilia Griffin (R)",
                value: "House::FL::13::Sheilia Griffin (R)",
                children: [],
              },
              {
                name: "Anna Paulina Luna (R)",
                value: "House::FL::13::Anna Paulina Luna (R)",
                children: [],
              },
              {
                name: "Amanda Makki (R)",
                value: "House::FL::13::Amanda Makki (R)",
                children: [],
              },
              {
                name: "Sharon Barry Newby (R)",
                value: "House::FL::13::Sharon Barry Newby (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::FL::14",
            children: [
              {
                name: "Kathy Castor (D)",
                value: "House::FL::14::Kathy Castor (D)",
                children: [],
              },
              {
                name: "Paul Sidney Elliott (R)",
                value: "House::FL::14::Paul Sidney Elliott (R)",
                children: [],
              },
              {
                name: "Chirstine Quinn (R)",
                value: "House::FL::14::Chirstine Quinn (R)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::FL::15",
            children: [
              {
                name: "Alan Cohn (D)",
                value: "House::FL::15::Alan Cohn (D)",
                children: [],
              },
              {
                name: "Adam Hattersley (D)",
                value: "House::FL::15::Adam Hattersley (D)",
                children: [],
              },
              {
                name: "Jesse Philippe (D)",
                value: "House::FL::15::Jesse Philippe (D)",
                children: [],
              },
              {
                name: "Ross Spasno (R)",
                value: "House::FL::15::Ross Spasno (R)",
                children: [],
              },
              {
                name: "Scott Franklin (R)",
                value: "House::FL::15::Scott Franklin (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::FL::16",
            children: [
              {
                name: "Margaret Good (D)",
                value: "House::FL::16::Margaret Good (D)",
                children: [],
              },
              {
                name: "Venn Buchanan (R)",
                value: "House::FL::16::Venn Buchanan (R)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::FL::17",
            children: [
              {
                name: "Allen Ellison (D)",
                value: "House::FL::17::Allen Ellison (D)",
                children: [],
              },
              {
                name: "Theodore Murray (D)",
                value: "House::FL::17::Theodore Murray (D)",
                children: [],
              },
              {
                name: "Greg Steube (R)",
                value: "House::FL::17::Greg Steube (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::FL::18",
            children: [
              {
                name: "Pam Keith (D)",
                value: "House::FL::18::Pam Keith (D)",
                children: [],
              },
              {
                name: "Oz Vazquez (D)",
                value: "House::FL::18::Oz Vazquez (D)",
                children: [],
              },
              {
                name: "Brian Mast (R)",
                value: "House::FL::18::Brian Mast (R)",
                children: [],
              },
              {
                name: "Nicholas Vessio (R)",
                value: "House::FL::18::Nicholas Vessio (R)",
                children: [],
              },
            ],
          },
          {
            name: "19",
            value: "House::FL::19",
            children: [
              {
                name: "Cindy Banyai (D)",
                value: "House::FL::19::Cindy Banyai (D)",
                children: [],
              },
              {
                name: "David Holden (D)",
                value: "House::FL::19::David Holden (D)",
                children: [],
              },
              {
                name: "Patrick Post (D)",
                value: "House::FL::19::Patrick Post (D)",
                children: [],
              },
              {
                name: "Darren Dione Aquino (R)",
                value: "House::FL::19::Darren Dione Aquino (R)",
                children: [],
              },
              {
                name: "Casey Askar (R)",
                value: "House::FL::19::Casey Askar (R)",
                children: [],
              },
              {
                name: "Byron Donalds (R)",
                value: "House::FL::19::Byron Donalds (R)",
                children: [],
              },
              {
                name: "Dane Eagle (R)",
                value: "House::FL::19::Dane Eagle (R)",
                children: [],
              },
              {
                name: "William Figlesthaler (R)",
                value: "House::FL::19::William Figlesthaler (R)",
                children: [],
              },
              {
                name: "Heather Fitzenhagen (R)",
                value: "House::FL::19::Heather Fitzenhagen (R)",
                children: [],
              },
              {
                name: "Randy Henderson (R)",
                value: "House::FL::19::Randy Henderson (R)",
                children: [],
              },
              {
                name: "Daniel Kowal (R)",
                value: "House::FL::19::Daniel Kowal (R)",
                children: [],
              },
              {
                name: "Christy McLaughlin (R)",
                value: "House::FL::19::Christy McLaughlin (R)",
                children: [],
              },
              {
                name: "Dan Severson (R)",
                value: "House::FL::19::Dan Severson (R)",
                children: [],
              },
            ],
          },
          {
            name: "20",
            value: "House::FL::20",
            children: [
              {
                name: "Alcee Hastings (D)",
                value: "House::FL::20::Alcee Hastings (D)",
                children: [],
              },
              {
                name: "Sheilia Cherfilus-McCormick (D)",
                value: "House::FL::20::Sheilia Cherfilus-McCormick (D)",
                children: [],
              },
              {
                name: "Vic Degrammont (R)",
                value: "House::FL::20::Vic Degrammont (R)",
                children: [],
              },
              {
                name: "Lateresa Jones (R)",
                value: "House::FL::20::Lateresa Jones (R)",
                children: [],
              },
              {
                name: "Greg Musselwhite (R)",
                value: "House::FL::20::Greg Musselwhite (R)",
                children: [],
              },
            ],
          },
          {
            name: "21",
            value: "House::FL::21",
            children: [
              {
                name: "Lois Frankel (D)",
                value: "House::FL::21::Lois Frankel (D)",
                children: [],
              },
              {
                name: "Guido Weiss (D)",
                value: "House::FL::21::Guido Weiss (D)",
                children: [],
              },
              {
                name: "Chirstian Acosta (R)",
                value: "House::FL::21::Chirstian Acosta (R)",
                children: [],
              },
              {
                name: "Piotr Blass (R)",
                value: "House::FL::21::Piotr Blass (R)",
                children: [],
              },
              {
                name: "Liz Felton (R)",
                value: "House::FL::21::Liz Felton (R)",
                children: [],
              },
              {
                name: "Laura Loomer (R)",
                value: "House::FL::21::Laura Loomer (R)",
                children: [],
              },
              {
                name: "Aaron Scanlan (R)",
                value: "House::FL::21::Aaron Scanlan (R)",
                children: [],
              },
              {
                name: "Reba Sherrill (R)",
                value: "House::FL::21::Reba Sherrill (R)",
                children: [],
              },
              {
                name: "Michael Vilardi (R)",
                value: "House::FL::21::Michael Vilardi (R)",
                children: [],
              },
            ],
          },
          {
            name: "22",
            value: "House::FL::22",
            children: [
              {
                name: "Theodore E. Deutch (D)",
                value: "House::FL::22::Theodore E. Deutch (D)",
                children: [],
              },
              {
                name: "Frances Flynn (R)",
                value: "House::FL::22::Frances Flynn (R)",
                children: [],
              },
              {
                name: "Jessi Melton (R)",
                value: "House::FL::22::Jessi Melton (R)",
                children: [],
              },
              {
                name: "James Pruden (R)",
                value: "House::FL::22::James Pruden (R)",
                children: [],
              },
              {
                name: "Darleen Swaffar (R)",
                value: "House::FL::22::Darleen Swaffar (R)",
                children: [],
              },
            ],
          },
          {
            name: "23",
            value: "House::FL::23",
            children: [
              {
                name: "Debbie Wasserman Schultz (D)",
                value: "House::FL::23::Debbie Wasserman Schultz (D)",
                children: [],
              },
              {
                name: "Jen Pereleman (D)",
                value: "House::FL::23::Jen Pereleman (D)",
                children: [],
              },
              {
                name: "D.B. Fugate (R)",
                value: "House::FL::23::D.B. Fugate (R)",
                children: [],
              },
              {
                name: "Michael Kroske (R)",
                value: "House::FL::23::Michael Kroske (R)",
                children: [],
              },
              {
                name: "Jeff Olson (R)",
                value: "House::FL::23::Jeff Olson (R)",
                children: [],
              },
              {
                name: "Carla Spalding (R)",
                value: "House::FL::23::Carla Spalding (R)",
                children: [],
              },
            ],
          },
          {
            name: "24",
            value: "House::FL::24",
            children: [
              {
                name: "Frederica S. Wilcson (D)",
                value: "House::FL::24::Frederica S. Wilcson (D)",
                children: [],
              },
              {
                name: "Ricardo De La Fuente (D)",
                value: "House::FL::24::Ricardo De La Fuente (D)",
                children: [],
              },
              {
                name: "Sakinah Lehtola (D)",
                value: "House::FL::24::Sakinah Lehtola (D)",
                children: [],
              },
              {
                name: "Christine Alexandria Olivo (D)",
                value: "House::FL::24::Christine Alexandria Olivo (D)",
                children: [],
              },
              {
                name: "Howard Knepper (R)",
                value: "House::FL::24::Howard Knepper (R)",
                children: [],
              },
              {
                name: "Lavern Spicer (R)",
                value: "House::FL::24::Lavern Spicer (R)",
                children: [],
              },
            ],
          },
          {
            name: "25",
            value: "House::FL::25",
            children: [
              {
                name: "Mario Diaz-Balart (R)",
                value: "House::FL::25::Mario Diaz-Balart (R)",
                children: [],
              },
            ],
          },
          {
            name: "26",
            value: "House::FL::26",
            children: [
              {
                name: "Debbie Mucarsel-Powell (D)",
                value: "House::FL::26::Debbie Mucarsel-Powell (D)",
                children: [],
              },
              {
                name: "Omar Blanco (R)",
                value: "House::FL::26::Omar Blanco (R)",
                children: [],
              },
              {
                name: "Carlos Gimenez (R)",
                value: "House::FL::26::Carlos Gimenez (R)",
                children: [],
              },
            ],
          },
          {
            name: "27",
            value: "House::FL::27",
            children: [
              {
                name: "Donna Shalala (D)",
                value: "House::FL::27::Donna Shalala (D)",
                children: [],
              },
              {
                name: "Juan Fiol (R)",
                value: "House::FL::27::Juan Fiol (R)",
                children: [],
              },
              {
                name: "Raymond Molina (R)",
                value: "House::FL::27::Raymond Molina (R)",
                children: [],
              },
              {
                name: "Frank Polo Sr. (R)",
                value: "House::FL::27::Frank Polo Sr. (R)",
                children: [],
              },
              {
                name: "Maria Elvira Salazar (R)",
                value: "House::FL::27::Maria Elvira Salazar (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "GA",
        value: "House::GA",
        children: [
          {
            name: "1",
            value: "House::GA::1",
            children: [
              {
                name: "Joyce Marie Griggs (D)",
                value: "House::GA::1::Joyce Marie Griggs (D)",
                children: [],
              },
              {
                name: "Lisa Ring (D)",
                value: "House::GA::1::Lisa Ring (D)",
                children: [],
              },
              {
                name: "Barbara Seidman (D)",
                value: "House::GA::1::Barbara Seidman (D)",
                children: [],
              },
              {
                name: "Earl Carter (R)",
                value: "House::GA::1::Earl Carter (R)",
                children: [],
              },
              {
                name: "Daniel Merritt (R)",
                value: "House::GA::1::Daniel Merritt (R)",
                children: [],
              },
              {
                name: "Ken Yasger (R)",
                value: "House::GA::1::Ken Yasger (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::GA::2",
            children: [
              {
                name: "Sanford Bishop Jr. (D)",
                value: "House::GA::2::Sanford Bishop Jr. (D)",
                children: [],
              },
              {
                name: "Vivian Childs (R)",
                value: "House::GA::2::Vivian Childs (R)",
                children: [],
              },
              {
                name: "Donald Cole (R)",
                value: "House::GA::2::Donald Cole (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::GA::3",
            children: [
              {
                name: "Val Almonord (D)",
                value: "House::GA::3::Val Almonord (D)",
                children: [],
              },
              {
                name: "Drew Ferugson (R)",
                value: "House::GA::3::Drew Ferugson (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::GA::4",
            children: [
              {
                name: "Hank Johnson (D)",
                value: "House::GA::4::Hank Johnson (D)",
                children: [],
              },
              {
                name: "William Haston (D)",
                value: "House::GA::4::William Haston (D)",
                children: [],
              },
              {
                name: "Elaine Nietmann (D)",
                value: "House::GA::4::Elaine Nietmann (D)",
                children: [],
              },
              {
                name: "Johsie Cruz (R)",
                value: "House::GA::4::Johsie Cruz (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::GA::5",
            children: [
              {
                name: "John Lewis (D)",
                value: "House::GA::5::John Lewis (D)",
                children: [],
              },
              {
                name: "Barrington Martin II (D)",
                value: "House::GA::5::Barrington Martin II (D)",
                children: [],
              },
              {
                name: "Angela Stanton-King (R)",
                value: "House::GA::5::Angela Stanton-King (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::GA::6",
            children: [
              {
                name: "Lucy McBath (D)",
                value: "House::GA::6::Lucy McBath (D)",
                children: [],
              },
              {
                name: "Mykel Lynn Barthelemy (R)",
                value: "House::GA::6::Mykel Lynn Barthelemy (R)",
                children: [],
              },
              {
                name: "Karen Handel (R)",
                value: "House::GA::6::Karen Handel (R)",
                children: [],
              },
              {
                name: "Blake Harbin (R)",
                value: "House::GA::6::Blake Harbin (R)",
                children: [],
              },
              {
                name: "Joe Profit (R)",
                value: "House::GA::6::Joe Profit (R)",
                children: [],
              },
              {
                name: "Paulette Smith (R)",
                value: "House::GA::6::Paulette Smith (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::GA::7",
            children: [
              {
                name: "Carolyn Bourdeaux (D)",
                value: "House::GA::7::Carolyn Bourdeaux (D)",
                children: [],
              },
              {
                name: "John Eaves (D)",
                value: "House::GA::7::John Eaves (D)",
                children: [],
              },
              {
                name: "Nabilah Islam (D)",
                value: "House::GA::7::Nabilah Islam (D)",
                children: [],
              },
              {
                name: "Zahra Karinshak (D)",
                value: "House::GA::7::Zahra Karinshak (D)",
                children: [],
              },
              {
                name: "Brenda Lopez Romero (D)",
                value: "House::GA::7::Brenda Lopez Romero (D)",
                children: [],
              },
              {
                name: "Rashid Malik (D)",
                value: "House::GA::7::Rashid Malik (D)",
                children: [],
              },
              {
                name: "Lisa Babbage (R)",
                value: "House::GA::7::Lisa Babbage (R)",
                children: [],
              },
              {
                name: "Mark Gonsalves (R)",
                value: "House::GA::7::Mark Gonsalves (R)",
                children: [],
              },
              {
                name: "Lynne Homrich (R)",
                value: "House::GA::7::Lynne Homrich (R)",
                children: [],
              },
              {
                name: "Zachary Kennemore (R)",
                value: "House::GA::7::Zachary Kennemore (R)",
                children: [],
              },
              {
                name: "Rich McCormick (R)",
                value: "House::GA::7::Rich McCormick (R)",
                children: [],
              },
              {
                name: "Renee Unterman (R)",
                value: "House::GA::7::Renee Unterman (R)",
                children: [],
              },
              {
                name: "Eugene Yu (R)",
                value: "House::GA::7::Eugene Yu (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::GA::8",
            children: [
              {
                name: "Lindsay Holliday (D)",
                value: "House::GA::8::Lindsay Holliday (D)",
                children: [],
              },
              {
                name: "Austin Scott (R)",
                value: "House::GA::8::Austin Scott (R)",
                children: [],
              },
              {
                name: "Robert Vance Dean (R)",
                value: "House::GA::8::Robert Vance Dean (R)",
                children: [],
              },
              {
                name: "Daniel Ellyson (R)",
                value: "House::GA::8::Daniel Ellyson (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::GA::9",
            children: [
              {
                name: "Devin Pandy (D)",
                value: "House::GA::9::Devin Pandy (D)",
                children: [],
              },
              {
                name: "Brooke Siskin (D)",
                value: "House::GA::9::Brooke Siskin (D)",
                children: [],
              },
              {
                name: "Dan Wilson (D)",
                value: "House::GA::9::Dan Wilson (D)",
                children: [],
              },
              {
                name: "Michael Boggus (R)",
                value: "House::GA::9::Michael Boggus (R)",
                children: [],
              },
              {
                name: "Paul C. Broun (R)",
                value: "House::GA::9::Paul C. Broun (R)",
                children: [],
              },
              {
                name: "Andrew Clyde (R)",
                value: "House::GA::9::Andrew Clyde (R)",
                children: [],
              },
              {
                name: "Matt Gurtler (R)",
                value: "House::GA::9::Matt Gurtler (R)",
                children: [],
              },
              {
                name: "Maria Strickland (R)",
                value: "House::GA::9::Maria Strickland (R)",
                children: [],
              },
              {
                name: "Kevin Tanner (R)",
                value: "House::GA::9::Kevin Tanner (R)",
                children: [],
              },
              {
                name: "Ethan Underwood (R)",
                value: "House::GA::9::Ethan Underwood (R)",
                children: [],
              },
              {
                name: "Kellie Weeks (R)",
                value: "House::GA::9::Kellie Weeks (R)",
                children: [],
              },
              {
                name: "John Wilkinson (R)",
                value: "House::GA::9::John Wilkinson (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::GA::10",
            children: [
              {
                name: "Andrew Ferguson (D)",
                value: "House::GA::10::Andrew Ferguson (D)",
                children: [],
              },
              {
                name: "Tabitha Johnson-Green (D)",
                value: "House::GA::10::Tabitha Johnson-Green (D)",
                children: [],
              },
              {
                name: "Jody Hice (R)",
                value: "House::GA::10::Jody Hice (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::GA::11",
            children: [
              {
                name: "Dana Barrett (D)",
                value: "House::GA::11::Dana Barrett (D)",
                children: [],
              },
              {
                name: "Barry Loudermilk (R)",
                value: "House::GA::11::Barry Loudermilk (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::GA::12",
            children: [
              {
                name: "Liz Johnson (D)",
                value: "House::GA::12::Liz Johnson (D)",
                children: [],
              },
              {
                name: "Dan Steiner (D)",
                value: "House::GA::12::Dan Steiner (D)",
                children: [],
              },
              {
                name: "Rick Allen (R)",
                value: "House::GA::12::Rick Allen (R)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::GA::13",
            children: [
              {
                name: "David Scott (D)",
                value: "House::GA::13::David Scott (D)",
                children: [],
              },
              {
                name: "Michael Owens (D)",
                value: "House::GA::13::Michael Owens (D)",
                children: [],
              },
              {
                name: "Jannquell Peters (D)",
                value: "House::GA::13::Jannquell Peters (D)",
                children: [],
              },
              {
                name: "Keisha Sean Waites (D)",
                value: "House::GA::13::Keisha Sean Waites (D)",
                children: [],
              },
              {
                name: "Caesar Gonzales (R)",
                value: "House::GA::13::Caesar Gonzales (R)",
                children: [],
              },
              {
                name: "Becky E. Hites (R)",
                value: "House::GA::13::Becky E. Hites (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::GA::14",
            children: [
              {
                name: "Kevin Van Ausdal (D)",
                value: "House::GA::14::Kevin Van Ausdal (D)",
                children: [],
              },
              {
                name: "John Barge (R)",
                value: "House::GA::14::John Barge (R)",
                children: [],
              },
              {
                name: "Ben Bullock (R)",
                value: "House::GA::14::Ben Bullock (R)",
                children: [],
              },
              {
                name: "Kevin Cooke (R)",
                value: "House::GA::14::Kevin Cooke (R)",
                children: [],
              },
              {
                name: "John Cowan (R)",
                value: "House::GA::14::John Cowan (R)",
                children: [],
              },
              {
                name: "Clayton Fuller (R)",
                value: "House::GA::14::Clayton Fuller (R)",
                children: [],
              },
              {
                name: "Marjorie Taylor Greene (R)",
                value: "House::GA::14::Marjorie Taylor Greene (R)",
                children: [],
              },
              {
                name: "Andy Gunther (R)",
                value: "House::GA::14::Andy Gunther (R)",
                children: [],
              },
              {
                name: "Bill Hembree (R)",
                value: "House::GA::14::Bill Hembree (R)",
                children: [],
              },
              {
                name: "Matt Laughridge (R)",
                value: "House::GA::14::Matt Laughridge (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "HI",
        value: "House::HI",
        children: [
          {
            name: "1",
            value: "House::HI::1",
            children: [
              {
                name: "Ed Case (D)",
                value: "House::HI::1::Ed Case (D)",
                children: [],
              },
              {
                name: "Taylor Smith (R)",
                value: "House::HI::1::Taylor Smith (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::HI::2",
            children: [
              {
                name: "David Cornejo (D)",
                value: "House::HI::2::David Cornejo (D)",
                children: [],
              },
              {
                name: "Brian Evans (D)",
                value: "House::HI::2::Brian Evans (D)",
                children: [],
              },
              {
                name: "Noelle Famera (D)",
                value: "House::HI::2::Noelle Famera (D)",
                children: [],
              },
              {
                name: "Kaiali'i Kahele (D)",
                value: "House::HI::2::Kaiali'i Kahele (D)",
                children: [],
              },
              {
                name: "Ryan Meza (D)",
                value: "House::HI::2::Ryan Meza (D)",
                children: [],
              },
              {
                name: "Joseph Akana (R)",
                value: "House::HI::2::Joseph Akana (R)",
                children: [],
              },
              {
                name: "Jonathan Hoomanawanui (R)",
                value: "House::HI::2::Jonathan Hoomanawanui (R)",
                children: [],
              },
              {
                name: "Elise Hatsuko Kaneshiro (R)",
                value: "House::HI::2::Elise Hatsuko Kaneshiro (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "ID",
        value: "House::ID",
        children: [
          {
            name: "1",
            value: "House::ID::1",
            children: [
              {
                name: "Staniela Nikolova (D)",
                value: "House::ID::1::Staniela Nikolova (D)",
                children: [],
              },
              {
                name: "Rudy Soto (D)",
                value: "House::ID::1::Rudy Soto (D)",
                children: [],
              },
              {
                name: "Russ Fulcher (R)",
                value: "House::ID::1::Russ Fulcher (R)",
                children: [],
              },
              {
                name: "Nicholas Jones (R)",
                value: "House::ID::1::Nicholas Jones (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::ID::2",
            children: [
              {
                name: "Aaron Swisher (D)",
                value: "House::ID::2::Aaron Swisher (D)",
                children: [],
              },
              {
                name: "Michael K. Simpson (R)",
                value: "House::ID::2::Michael K. Simpson (R)",
                children: [],
              },
              {
                name: "Kevin Rhoades (R)",
                value: "House::ID::2::Kevin Rhoades (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "IL",
        value: "House::IL",
        children: [
          {
            name: "1",
            value: "House::IL::1",
            children: [
              {
                name: "Bobby Rush (D)",
                value: "House::IL::1::Bobby Rush (D)",
                children: [],
              },
              {
                name: "Philanise White (R)",
                value: "House::IL::1::Philanise White (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::IL::2",
            children: [
              {
                name: "Robin Kelly (D)",
                value: "House::IL::2::Robin Kelly (D)",
                children: [],
              },
              {
                name: "Theresa Rabron (R)",
                value: "House::IL::2::Theresa Rabron (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::IL::3",
            children: [
              {
                name: "Marie Newman (D)",
                value: "House::IL::3::Marie Newman (D)",
                children: [],
              },
              {
                name: "Mike Fricilone (R)",
                value: "House::IL::3::Mike Fricilone (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::IL::4",
            children: [
              {
                name: "Jesus Garcia (D)",
                value: "House::IL::4::Jesus Garcia (D)",
                children: [],
              },
              {
                name: "Christopher Lasky (R)",
                value: "House::IL::4::Christopher Lasky (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::IL::5",
            children: [
              {
                name: "Mike Quigley (D)",
                value: "House::IL::5::Mike Quigley (D)",
                children: [],
              },
              {
                name: "Tom Hanson (R)",
                value: "House::IL::5::Tom Hanson (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::IL::6",
            children: [
              {
                name: "Sean Casten (D)",
                value: "House::IL::6::Sean Casten (D)",
                children: [],
              },
              {
                name: "Jeanne M. Ives (R)",
                value: "House::IL::6::Jeanne M. Ives (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::IL::7",
            children: [
              {
                name: "Danny K. Davis (D)",
                value: "House::IL::7::Danny K. Davis (D)",
                children: [],
              },
              {
                name: "Craig Cameron (R)",
                value: "House::IL::7::Craig Cameron (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::IL::8",
            children: [
              {
                name: "Raja Krishnamoorthi (D)",
                value: "House::IL::8::Raja Krishnamoorthi (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::IL::9",
            children: [
              {
                name: "Jan Schakowsky (D)",
                value: "House::IL::9::Jan Schakowsky (D)",
                children: [],
              },
              {
                name: "Sargis Sangari (R)",
                value: "House::IL::9::Sargis Sangari (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::IL::10",
            children: [
              {
                name: "Brad Schneider (D)",
                value: "House::IL::10::Brad Schneider (D)",
                children: [],
              },
              {
                name: "Valeirie Ramirez Mukherjee (R)",
                value: "House::IL::10::Valeirie Ramirez Mukherjee (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::IL::11",
            children: [
              {
                name: "Bill Foster (D)",
                value: "House::IL::11::Bill Foster (D)",
                children: [],
              },
              {
                name: "Rick Laib (R)",
                value: "House::IL::11::Rick Laib (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::IL::12",
            children: [
              {
                name: "Mike Bost (R)",
                value: "House::IL::12::Mike Bost (R)",
                children: [],
              },
              {
                name: "Ray Lenzi (D)",
                value: "House::IL::12::Ray Lenzi (D)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::IL::13",
            children: [
              {
                name: "Rodney Davis (R)",
                value: "House::IL::13::Rodney Davis (R)",
                children: [],
              },
              {
                name: "Betsy Londrigan (D)",
                value: "House::IL::13::Betsy Londrigan (D)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::IL::14",
            children: [
              {
                name: "Lauren Underwood (D)",
                value: "House::IL::14::Lauren Underwood (D)",
                children: [],
              },
              {
                name: "Jim Oberweis (R)",
                value: "House::IL::14::Jim Oberweis (R)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::IL::15",
            children: [
              {
                name: "Erika Weaver (D)",
                value: "House::IL::15::Erika Weaver (D)",
                children: [],
              },
              {
                name: "Mary Miller (R)",
                value: "House::IL::15::Mary Miller (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::IL::16",
            children: [
              {
                name: "Adam Kinzinger (R)",
                value: "House::IL::16::Adam Kinzinger (R)",
                children: [],
              },
              {
                name: "Dani Brzozowsky (D)",
                value: "House::IL::16::Dani Brzozowsky (D)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::IL::17",
            children: [
              {
                name: "Cheri Bustos (D)",
                value: "House::IL::17::Cheri Bustos (D)",
                children: [],
              },
              {
                name: "Esther Joy King (R)",
                value: "House::IL::17::Esther Joy King (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::IL::18",
            children: [
              {
                name: "Darin Lahood (R)",
                value: "House::IL::18::Darin Lahood (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "IN",
        value: "House::IN",
        children: [
          {
            name: "1",
            value: "House::IN::1",
            children: [
              {
                name: "Melissa Borom (D)",
                value: "House::IN::1::Melissa Borom (D)",
                children: [],
              },
              {
                name: "Carrie Castro (D)",
                value: "House::IN::1::Carrie Castro (D)",
                children: [],
              },
              {
                name: "Scott Costello (D)",
                value: "House::IN::1::Scott Costello (D)",
                children: [],
              },
              {
                name: "Antonio Daggett Sr. (D)",
                value: "House::IN::1::Antonio Daggett Sr. (D)",
                children: [],
              },
              {
                name: "Ryan Farrar (D)",
                value: "House::IN::1::Ryan Farrar (D)",
                children: [],
              },
              {
                name: "Sabrina Haake (D)",
                value: "House::IN::1::Sabrina Haake (D)",
                children: [],
              },
              {
                name: "John Hall (D)",
                value: "House::IN::1::John Hall (D)",
                children: [],
              },
              {
                name: "Jim Harper (D)",
                value: "House::IN::1::Jim Harper (D)",
                children: [],
              },
              {
                name: "Ryan Lamb (D)",
                value: "House::IN::1::Ryan Lamb (D)",
                children: [],
              },
              {
                name: "Thomas McDermott Jr. (D)",
                value: "House::IN::1::Thomas McDermott Jr. (D)",
                children: [],
              },
              {
                name: "Wendell Mosby (D)",
                value: "House::IN::1::Wendell Mosby (D)",
                children: [],
              },
              {
                name: "Frank Mrvan (D)",
                value: "House::IN::1::Frank Mrvan (D)",
                children: [],
              },
              {
                name: "Mara Candelaria Reardon (D)",
                value: "House::IN::1::Mara Candelaria Reardon (D)",
                children: [],
              },
              {
                name: "Jayson Reeves (D)",
                value: "House::IN::1::Jayson Reeves (D)",
                children: [],
              },
              {
                name: "Andrew Sylwestrowicz (D)",
                value: "House::IN::1::Andrew Sylwestrowicz (D)",
                children: [],
              },
              {
                name: "Dion Bergeron (R)",
                value: "House::IN::1::Dion Bergeron (R)",
                children: [],
              },
              {
                name: "Mont Handley (R)",
                value: "House::IN::1::Mont Handley (R)",
                children: [],
              },
              {
                name: "Spencer Lemmons (R)",
                value: "House::IN::1::Spencer Lemmons (R)",
                children: [],
              },
              {
                name: "Mark Leyva (R)",
                value: "House::IN::1::Mark Leyva (R)",
                children: [],
              },
              {
                name: "William Powers (R)",
                value: "House::IN::1::William Powers (R)",
                children: [],
              },
              {
                name: "Delano Scaife (R)",
                value: "House::IN::1::Delano Scaife (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::IN::2",
            children: [
              {
                name: "Pat Hackett (D)",
                value: "House::IN::2::Pat Hackett (D)",
                children: [],
              },
              {
                name: "Ellen Marks (D)",
                value: "House::IN::2::Ellen Marks (D)",
                children: [],
              },
              {
                name: "Jackie Walorski (R)",
                value: "House::IN::2::Jackie Walorski (R)",
                children: [],
              },
              {
                name: "Christopher Davis (R)",
                value: "House::IN::2::Christopher Davis (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::IN::3",
            children: [
              {
                name: "Chip Coldiron (D)",
                value: "House::IN::3::Chip Coldiron (D)",
                children: [],
              },
              {
                name: "Jean-Paul Kalonji (D)",
                value: "House::IN::3::Jean-Paul Kalonji (D)",
                children: [],
              },
              {
                name: "Carlos Marcano (D)",
                value: "House::IN::3::Carlos Marcano (D)",
                children: [],
              },
              {
                name: "Tommy Schrader (D)",
                value: "House::IN::3::Tommy Schrader (D)",
                children: [],
              },
              {
                name: "Jim Banks (R)",
                value: "House::IN::3::Jim Banks (R)",
                children: [],
              },
              {
                name: "Chirs Magiera (R)",
                value: "House::IN::3::Chirs Magiera (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::IN::4",
            children: [
              {
                name: "Benjamin Frederick (D)",
                value: "House::IN::4::Benjamin Frederick (D)",
                children: [],
              },
              {
                name: "Joe Mackey (D)",
                value: "House::IN::4::Joe Mackey (D)",
                children: [],
              },
              {
                name: "Howard Pollchik (D)",
                value: "House::IN::4::Howard Pollchik (D)",
                children: [],
              },
              {
                name: "Veronikka Ziol (D)",
                value: "House::IN::4::Veronikka Ziol (D)",
                children: [],
              },
              {
                name: "Jim Baird (R)",
                value: "House::IN::4::Jim Baird (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::IN::5",
            children: [
              {
                name: "Jennifer Christie (D)",
                value: "House::IN::5::Jennifer Christie (D)",
                children: [],
              },
              {
                name: "Christina Hale (D)",
                value: "House::IN::5::Christina Hale (D)",
                children: [],
              },
              {
                name: "Andy Jacobs (D)",
                value: "House::IN::5::Andy Jacobs (D)",
                children: [],
              },
              {
                name: "Ralph Spelbring (D)",
                value: "House::IN::5::Ralph Spelbring (D)",
                children: [],
              },
              {
                name: "Dee Thornton (D)",
                value: "House::IN::5::Dee Thornton (D)",
                children: [],
              },
              {
                name: "Kent Abernathy (R)",
                value: "House::IN::5::Kent Abernathy (R)",
                children: [],
              },
              {
                name: "Andrew Bales (R)",
                value: "House::IN::5::Andrew Bales (R)",
                children: [],
              },
              {
                name: "Micah Beckwith (R)",
                value: "House::IN::5::Micah Beckwith (R)",
                children: [],
              },
              {
                name: "Carl Brizzi (R)",
                value: "House::IN::5::Carl Brizzi (R)",
                children: [],
              },
              {
                name: "Allen Davidson (R)",
                value: "House::IN::5::Allen Davidson (R)",
                children: [],
              },
              {
                name: "Chuck Dietzen (R)",
                value: "House::IN::5::Chuck Dietzen (R)",
                children: [],
              },
              {
                name: "Beth Henderson (R)",
                value: "House::IN::5::Beth Henderson (R)",
                children: [],
              },
              {
                name: "Matthew Hook (R)",
                value: "House::IN::5::Matthew Hook (R)",
                children: [],
              },
              {
                name: "Matthew Hullinger (R)",
                value: "House::IN::5::Matthew Hullinger (R)",
                children: [],
              },
              {
                name: "Kelly Mitchell (R)",
                value: "House::IN::5::Kelly Mitchell (R)",
                children: [],
              },
              {
                name: "Danny Niederberger (R)",
                value: "House::IN::5::Danny Niederberger (R)",
                children: [],
              },
              {
                name: "Mark Small (R)",
                value: "House::IN::5::Mark Small (R)",
                children: [],
              },
              {
                name: "Victoria Spartz (R)",
                value: "House::IN::5::Victoria Spartz (R)",
                children: [],
              },
              {
                name: "Russell Stwalley (R)",
                value: "House::IN::5::Russell Stwalley (R)",
                children: [],
              },
              {
                name: "Victor Wakley (R)",
                value: "House::IN::5::Victor Wakley (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::IN::6",
            children: [
              {
                name: "George Thomas Holland (D)",
                value: "House::IN::6::George Thomas Holland (D)",
                children: [],
              },
              {
                name: "Jeannine Lee Lake (D)",
                value: "House::IN::6::Jeannine Lee Lake (D)",
                children: [],
              },
              {
                name: "Barry Welsh (D)",
                value: "House::IN::6::Barry Welsh (D)",
                children: [],
              },
              {
                name: "Greg Pence (R)",
                value: "House::IN::6::Greg Pence (R)",
                children: [],
              },
              {
                name: "Mike Campbell (R)",
                value: "House::IN::6::Mike Campbell (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::IN::7",
            children: [
              {
                name: "André Carson (D)",
                value: "House::IN::7::André Carson (D)",
                children: [],
              },
              {
                name: "Pierre Quincy Pullins (D)",
                value: "House::IN::7::Pierre Quincy Pullins (D)",
                children: [],
              },
              {
                name: "Jon Davis (R)",
                value: "House::IN::7::Jon Davis (R)",
                children: [],
              },
              {
                name: "Douglas Merrill (R)",
                value: "House::IN::7::Douglas Merrill (R)",
                children: [],
              },
              {
                name: "J.D. Miniear (R)",
                value: "House::IN::7::J.D. Miniear (R)",
                children: [],
              },
              {
                name: "Martin Ramey (R)",
                value: "House::IN::7::Martin Ramey (R)",
                children: [],
              },
              {
                name: "Susan Marie Smith (R)",
                value: "House::IN::7::Susan Marie Smith (R)",
                children: [],
              },
              {
                name: "Gerald Walters (R)",
                value: "House::IN::7::Gerald Walters (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::IN::8",
            children: [
              {
                name: "Ron Drake (D)",
                value: "House::IN::8::Ron Drake (D)",
                children: [],
              },
              {
                name: "Thomasina Marsili (D)",
                value: "House::IN::8::Thomasina Marsili (D)",
                children: [],
              },
              {
                name: "Mike Webster (D)",
                value: "House::IN::8::Mike Webster (D)",
                children: [],
              },
              {
                name: "Larry Bucshon (R)",
                value: "House::IN::8::Larry Bucshon (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::IN::9",
            children: [
              {
                name: "D. Liam Dorris (D)",
                value: "House::IN::9::D. Liam Dorris (D)",
                children: [],
              },
              {
                name: "Brandon Hood (D)",
                value: "House::IN::9::Brandon Hood (D)",
                children: [],
              },
              {
                name: "James O'Gabhann III (D)",
                value: "House::IN::9::James O'Gabhann III (D)",
                children: [],
              },
              {
                name: "Mark J. Powell (D)",
                value: "House::IN::9::Mark J. Powell (D)",
                children: [],
              },
              {
                name: "Andy Ruff (D)",
                value: "House::IN::9::Andy Ruff (D)",
                children: [],
              },
              {
                name: "Trey Hollingsworth (R)",
                value: "House::IN::9::Trey Hollingsworth (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "IA",
        value: "House::IA",
        children: [
          {
            name: "1",
            value: "House::IA::1",
            children: [
              {
                name: "Abby Finkenauer (D)",
                value: "House::IA::1::Abby Finkenauer (D)",
                children: [],
              },
              {
                name: "Thomas Hansen (R)",
                value: "House::IA::1::Thomas Hansen (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::IA::2",
            children: [
              {
                name: "Rita Hart (D)",
                value: "House::IA::2::Rita Hart (D)",
                children: [],
              },
              {
                name: "Tim Borchardt (R)",
                value: "House::IA::2::Tim Borchardt (R)",
                children: [],
              },
              {
                name: "Steven Everly (R)",
                value: "House::IA::2::Steven Everly (R)",
                children: [],
              },
              {
                name: "Mariannette Miller-Meeks (R)",
                value: "House::IA::2::Mariannette Miller-Meeks (R)",
                children: [],
              },
              {
                name: "Ricky Lee Phillips (R)",
                value: "House::IA::2::Ricky Lee Phillips (R)",
                children: [],
              },
              {
                name: "Robert T. Schilling (R)",
                value: "House::IA::2::Robert T. Schilling (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::IA::3",
            children: [
              {
                name: "Cindy Axne (D)",
                value: "House::IA::3::Cindy Axne (D)",
                children: [],
              },
              {
                name: "Bill Schafer (R)",
                value: "House::IA::3::Bill Schafer (R)",
                children: [],
              },
              {
                name: "David Young (R)",
                value: "House::IA::3::David Young (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::IA::4",
            children: [
              {
                name: "J.D. Scholten (D)",
                value: "House::IA::4::J.D. Scholten (D)",
                children: [],
              },
              {
                name: "Steve King (R)",
                value: "House::IA::4::Steve King (R)",
                children: [],
              },
              {
                name: "Randy Feenstra (R)",
                value: "House::IA::4::Randy Feenstra (R)",
                children: [],
              },
              {
                name: "Steve Reeder (R)",
                value: "House::IA::4::Steve Reeder (R)",
                children: [],
              },
              {
                name: "Bret Richards (R)",
                value: "House::IA::4::Bret Richards (R)",
                children: [],
              },
              {
                name: "Jeremy Taylor (R)",
                value: "House::IA::4::Jeremy Taylor (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "KS",
        value: "House::KS",
        children: [
          {
            name: "1",
            value: "House::KS::1",
            children: [
              {
                name: "Kali Barnett (D)",
                value: "House::KS::1::Kali Barnett (D)",
                children: [],
              },
              {
                name: "Christy Davis (D)",
                value: "House::KS::1::Christy Davis (D)",
                children: [],
              },
              {
                name: "Brandon Williams (D)",
                value: "House::KS::1::Brandon Williams (D)",
                children: [],
              },
              {
                name: "Bill Clifford (R)",
                value: "House::KS::1::Bill Clifford (R)",
                children: [],
              },
              {
                name: "Tracey Mann (R)",
                value: "House::KS::1::Tracey Mann (R)",
                children: [],
              },
              {
                name: "Troy L. Waymaster (R)",
                value: "House::KS::1::Troy L. Waymaster (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::KS::2",
            children: [
              {
                name: "Michelle De La Isla (D)",
                value: "House::KS::2::Michelle De La Isla (D)",
                children: [],
              },
              {
                name: "Jacob LaTurner (R)",
                value: "House::KS::2::Jacob LaTurner (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::KS::3",
            children: [
              {
                name: "Sharice Davids (D)",
                value: "House::KS::3::Sharice Davids (D)",
                children: [],
              },
              {
                name: "Amanda Adkins (R)",
                value: "House::KS::3::Amanda Adkins (R)",
                children: [],
              },
              {
                name: "Mike Beehler (R)",
                value: "House::KS::3::Mike Beehler (R)",
                children: [],
              },
              {
                name: "Adrienne Vallejo Foster (R)",
                value: "House::KS::3::Adrienne Vallejo Foster (R)",
                children: [],
              },
              {
                name: "Sara Hart Weir (R)",
                value: "House::KS::3::Sara Hart Weir (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::KS::4",
            children: [
              {
                name: "Ron Estes (D)",
                value: "House::KS::4::Ron Estes (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "KY",
        value: "House::KY",
        children: [
          {
            name: "1",
            value: "House::KY::1",
            children: [
              {
                name: "James Rhodes (D)",
                value: "House::KY::1::James Rhodes (D)",
                children: [],
              },
              {
                name: "James Comer Jr. (R)",
                value: "House::KY::1::James Comer Jr. (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::KY::2",
            children: [
              {
                name: "Hank Linderman (D)",
                value: "House::KY::2::Hank Linderman (D)",
                children: [],
              },
              {
                name: "Brett Guthrie (R)",
                value: "House::KY::2::Brett Guthrie (R)",
                children: [],
              },
              {
                name: "Kathleen Free (R)",
                value: "House::KY::2::Kathleen Free (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::KY::3",
            children: [
              {
                name: "John A. Yarmuth (D)",
                value: "House::KY::3::John A. Yarmuth (D)",
                children: [],
              },
              {
                name: "Mike Craven (R)",
                value: "House::KY::3::Mike Craven (R)",
                children: [],
              },
              {
                name: "Waymen Eddings (R)",
                value: "House::KY::3::Waymen Eddings (R)",
                children: [],
              },
              {
                name: "Rhonda Palazzo (R)",
                value: "House::KY::3::Rhonda Palazzo (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::KY::4",
            children: [
              {
                name: "Shannon Fabert (D)",
                value: "House::KY::4::Shannon Fabert (D)",
                children: [],
              },
              {
                name: "Alexandra Owensby (D)",
                value: "House::KY::4::Alexandra Owensby (D)",
                children: [],
              },
              {
                name: "Thomas Massie (R)",
                value: "House::KY::4::Thomas Massie (R)",
                children: [],
              },
              {
                name: "Todd McMurty (R)",
                value: "House::KY::4::Todd McMurty (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::KY::5",
            children: [
              {
                name: "Matthew Ryan Best (D)",
                value: "House::KY::5::Matthew Ryan Best (D)",
                children: [],
              },
              {
                name: "Hal Rodger (R)",
                value: "House::KY::5::Hal Rodger (R)",
                children: [],
              },
              {
                name: "Gerardo Serrano (R)",
                value: "House::KY::5::Gerardo Serrano (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::KY::6",
            children: [
              {
                name: "Josh Hicks (D)",
                value: "House::KY::6::Josh Hicks (D)",
                children: [],
              },
              {
                name: "Daniel Kemph (D)",
                value: "House::KY::6::Daniel Kemph (D)",
                children: [],
              },
              {
                name: "Andy Barr (R)",
                value: "House::KY::6::Andy Barr (R)",
                children: [],
              },
              {
                name: "Chuck Eddy (R)",
                value: "House::KY::6::Chuck Eddy (R)",
                children: [],
              },
              {
                name: "Geoff Young (R)",
                value: "House::KY::6::Geoff Young (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "LA",
        value: "House::LA",
        children: [
          {
            name: "1",
            value: "House::LA::1",
            children: [
              {
                name: "Steve Scalise (R)",
                value: "House::LA::1::Steve Scalise (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::LA::2",
            children: [
              {
                name: "Cedric Richmond (D)",
                value: "House::LA::2::Cedric Richmond (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::LA::3",
            children: [
              {
                name: "Clay Higgins (R)",
                value: "House::LA::3::Clay Higgins (R)",
                children: [],
              },
              {
                name: "Rob Anderson (D)",
                value: "House::LA::3::Rob Anderson (D)",
                children: [],
              },
              {
                name: "Verone Thomas (D)",
                value: "House::LA::3::Verone Thomas (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::LA::4",
            children: [
              {
                name: "Mike Johnson (R)",
                value: "House::LA::4::Mike Johnson (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::LA::5",
            children: [
              {
                name: "Candy Christophe (D)",
                value: "House::LA::5::Candy Christophe (D)",
                children: [],
              },
              {
                name: "Brody Pierrottie (D)",
                value: "House::LA::5::Brody Pierrottie (D)",
                children: [],
              },
              {
                name: "Lance Harris (R)",
                value: "House::LA::5::Lance Harris (R)",
                children: [],
              },
              {
                name: "Luke Letlow (R)",
                value: "House::LA::5::Luke Letlow (R)",
                children: [],
              },
              {
                name: "Randall Scott Robinson (R)",
                value: "House::LA::5::Randall Scott Robinson (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::LA::6",
            children: [
              {
                name: "Garret Graves (R)",
                value: "House::LA::6::Garret Graves (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "ME",
        value: "House::ME",
        children: [
          {
            name: "1",
            value: "House::ME::1",
            children: [
              {
                name: "Chellie Pingree (D)",
                value: "House::ME::1::Chellie Pingree (D)",
                children: [],
              },
              {
                name: "Jay Allen (R)",
                value: "House::ME::1::Jay Allen (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::ME::2",
            children: [
              {
                name: "Jared Golden (D)",
                value: "House::ME::2::Jared Golden (D)",
                children: [],
              },
              {
                name: "Adrienne Bennett (R)",
                value: "House::ME::2::Adrienne Bennett (R)",
                children: [],
              },
              {
                name: "Eric Brakey (R)",
                value: "House::ME::2::Eric Brakey (R)",
                children: [],
              },
              {
                name: "Dale Crafts (R)",
                value: "House::ME::2::Dale Crafts (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MD",
        value: "House::MD",
        children: [
          {
            name: "1",
            value: "House::MD::1",
            children: [
              {
                name: "Allison Galbraith (D)",
                value: "House::MD::1::Allison Galbraith (D)",
                children: [],
              },
              {
                name: "Mia Mason (D)",
                value: "House::MD::1::Mia Mason (D)",
                children: [],
              },
              {
                name: "Jennifer Pingley (D)",
                value: "House::MD::1::Jennifer Pingley (D)",
                children: [],
              },
              {
                name: "Andrew Harris (R)",
                value: "House::MD::1::Andrew Harris (R)",
                children: [],
              },
              {
                name: "Jorge Delgado (R)",
                value: "House::MD::1::Jorge Delgado (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MD::2",
            children: [
              {
                name: "Dutch Ruppersberger (D)",
                value: "House::MD::2::Dutch Ruppersberger (D)",
                children: [],
              },
              {
                name: "Michael Feldman (D)",
                value: "House::MD::2::Michael Feldman (D)",
                children: [],
              },
              {
                name: "Jake Pretot (D)",
                value: "House::MD::2::Jake Pretot (D)",
                children: [],
              },
              {
                name: "Scott M. Collier (R)",
                value: "House::MD::2::Scott M. Collier (R)",
                children: [],
              },
              {
                name: "Tim Fazenbaker (R)",
                value: "House::MD::2::Tim Fazenbaker (R)",
                children: [],
              },
              {
                name: "Rick Impallaria (R)",
                value: "House::MD::2::Rick Impallaria (R)",
                children: [],
              },
              {
                name: "Genevieve Morris (R)",
                value: "House::MD::2::Genevieve Morris (R)",
                children: [],
              },
              {
                name: "Johnny Ray Salling (R)",
                value: "House::MD::2::Johnny Ray Salling (R)",
                children: [],
              },
              {
                name: "Jim Simpson (R)",
                value: "House::MD::2::Jim Simpson (R)",
                children: [],
              },
              {
                name: "Blaine Taylor (R)",
                value: "House::MD::2::Blaine Taylor (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MD::3",
            children: [
              {
                name: "John Sarbanes (D)",
                value: "House::MD::3::John Sarbanes (D)",
                children: [],
              },
              {
                name: "Joseph Ardito (D)",
                value: "House::MD::3::Joseph Ardito (D)",
                children: [],
              },
              {
                name: "John Rea (D)",
                value: "House::MD::3::John Rea (D)",
                children: [],
              },
              {
                name: "Charles Anthony (R)",
                value: "House::MD::3::Charles Anthony (R)",
                children: [],
              },
              {
                name: "Thomas Harris (R)",
                value: "House::MD::3::Thomas Harris (R)",
                children: [],
              },
              {
                name: "Reba Hawkins (R)",
                value: "House::MD::3::Reba Hawkins (R)",
                children: [],
              },
              {
                name: "Joshua Morales (R)",
                value: "House::MD::3::Joshua Morales (R)",
                children: [],
              },
              {
                name: "Rob Seyfferth (R)",
                value: "House::MD::3::Rob Seyfferth (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MD::4",
            children: [
              {
                name: "Anthony G. Brown (D)",
                value: "House::MD::4::Anthony G. Brown (D)",
                children: [],
              },
              {
                name: "Shelia Bryant (D)",
                value: "House::MD::4::Shelia Bryant (D)",
                children: [],
              },
              {
                name: "Kim Shelton (D)",
                value: "House::MD::4::Kim Shelton (D)",
                children: [],
              },
              {
                name: "Nnabu Eze (R)",
                value: "House::MD::4::Nnabu Eze (R)",
                children: [],
              },
              {
                name: "Eric Loeb (R)",
                value: "House::MD::4::Eric Loeb (R)",
                children: [],
              },
              {
                name: "George McDermott (R)",
                value: "House::MD::4::George McDermott (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::MD::5",
            children: [
              {
                name: "Steny Hoyer (D)",
                value: "House::MD::5::Steny Hoyer (D)",
                children: [],
              },
              {
                name: "William Devine III (D)",
                value: "House::MD::5::William Devine III (D)",
                children: [],
              },
              {
                name: "Vanessa Marie Hoffman (D)",
                value: "House::MD::5::Vanessa Marie Hoffman (D)",
                children: [],
              },
              {
                name: "Briana Urbina (D)",
                value: "House::MD::5::Briana Urbina (D)",
                children: [],
              },
              {
                name: "Mckayla Wilkes (D)",
                value: "House::MD::5::Mckayla Wilkes (D)",
                children: [],
              },
              {
                name: "Bryan Duval Cubero (R)",
                value: "House::MD::5::Bryan Duval Cubero (R)",
                children: [],
              },
              {
                name: "Lee Havis (R)",
                value: "House::MD::5::Lee Havis (R)",
                children: [],
              },
              {
                name: "Kenneth Lee (R)",
                value: "House::MD::5::Kenneth Lee (R)",
                children: [],
              },
              {
                name: "Chris Palombi (R)",
                value: "House::MD::5::Chris Palombi (R)",
                children: [],
              },
              {
                name: "Douglas Sayers (R)",
                value: "House::MD::5::Douglas Sayers (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::MD::6",
            children: [
              {
                name: "David Trone (D)",
                value: "House::MD::6::David Trone (D)",
                children: [],
              },
              {
                name: "Mawell Bero (D)",
                value: "House::MD::6::Mawell Bero (D)",
                children: [],
              },
              {
                name: "Kevin Caldwell (R)",
                value: "House::MD::6::Kevin Caldwell (R)",
                children: [],
              },
              {
                name: "Chris Meyyur (R)",
                value: "House::MD::6::Chris Meyyur (R)",
                children: [],
              },
              {
                name: "Neil Parrott (R)",
                value: "House::MD::6::Neil Parrott (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::MD::7",
            children: [
              {
                name: "Kweisi Mfume (D)",
                value: "House::MD::7::Kweisi Mfume (D)",
                children: [],
              },
              {
                name: "T. Dan Baker (D)",
                value: "House::MD::7::T. Dan Baker (D)",
                children: [],
              },
              {
                name: "Alicia Brown (D)",
                value: "House::MD::7::Alicia Brown (D)",
                children: [],
              },
              {
                name: "Jill Carter (D)",
                value: "House::MD::7::Jill Carter (D)",
                children: [],
              },
              {
                name: "Matko Lee Chullin III (D)",
                value: "House::MD::7::Matko Lee Chullin III (D)",
                children: [],
              },
              {
                name: "Jermyn Michael Davidson (D)",
                value: "House::MD::7::Jermyn Michael Davidson (D)",
                children: [],
              },
              {
                name: "Darryl Gonzalez (D)",
                value: "House::MD::7::Darryl Gonzalez (D)",
                children: [],
              },
              {
                name: "Mark Gosnell (D)",
                value: "House::MD::7::Mark Gosnell (D)",
                children: [],
              },
              {
                name: "Dan Hiegel (D)",
                value: "House::MD::7::Dan Hiegel (D)",
                children: [],
              },
              {
                name: "Michael Howard Jr. (D)",
                value: "House::MD::7::Michael Howard Jr. (D)",
                children: [],
              },
              {
                name: "Jay Jalisi (D)",
                value: "House::MD::7::Jay Jalisi (D)",
                children: [],
              },
              {
                name: "Adrian Petrus (D)",
                value: "House::MD::7::Adrian Petrus (D)",
                children: [],
              },
              {
                name: "Saafir Rabb (D)",
                value: "House::MD::7::Saafir Rabb (D)",
                children: [],
              },
              {
                name: "Maya Rockeymoore Cummings (D)",
                value: "House::MD::7::Maya Rockeymoore Cummings (D)",
                children: [],
              },
              {
                name: "Gary Schuman (D)",
                value: "House::MD::7::Gary Schuman (D)",
                children: [],
              },
              {
                name: "Charles Smith (D)",
                value: "House::MD::7::Charles Smith (D)",
                children: [],
              },
              {
                name: "Harry Spikes (D)",
                value: "House::MD::7::Harry Spikes (D)",
                children: [],
              },
              {
                name: "Charles Stokes (D)",
                value: "House::MD::7::Charles Stokes (D)",
                children: [],
              },
              {
                name: "Jeffrey Woodard (D)",
                value: "House::MD::7::Jeffrey Woodard (D)",
                children: [],
              },
              {
                name: "Ray Bly (R)",
                value: "House::MD::7::Ray Bly (R)",
                children: [],
              },
              {
                name: "Brian Brown (R)",
                value: "House::MD::7::Brian Brown (R)",
                children: [],
              },
              {
                name: "Kim Klacik (R)",
                value: "House::MD::7::Kim Klacik (R)",
                children: [],
              },
              {
                name: "M.J. Madwolf (R)",
                value: "House::MD::7::M.J. Madwolf (R)",
                children: [],
              },
              {
                name: "Elizabeth Matory (R)",
                value: "House::MD::7::Elizabeth Matory (R)",
                children: [],
              },
              {
                name: "William Newton (R)",
                value: "House::MD::7::William Newton (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::MD::8",
            children: [
              {
                name: "Jamie Raskin (D)",
                value: "House::MD::8::Jamie Raskin (D)",
                children: [],
              },
              {
                name: "Marcia Morgan (D)",
                value: "House::MD::8::Marcia Morgan (D)",
                children: [],
              },
              {
                name: "Utam Paul (D)",
                value: "House::MD::8::Utam Paul (D)",
                children: [],
              },
              {
                name: "Lih Young (D)",
                value: "House::MD::8::Lih Young (D)",
                children: [],
              },
              {
                name: "Gregory Coll (R)",
                value: "House::MD::8::Gregory Coll (R)",
                children: [],
              },
              {
                name: "Bridgette Cooper (R)",
                value: "House::MD::8::Bridgette Cooper (R)",
                children: [],
              },
              {
                name: "Nicholas Gladden (R)",
                value: "House::MD::8::Nicholas Gladden (R)",
                children: [],
              },
              {
                name: "Patricia Rogers (R)",
                value: "House::MD::8::Patricia Rogers (R)",
                children: [],
              },
              {
                name: "Shelly Skolnick (R)",
                value: "House::MD::8::Shelly Skolnick (R)",
                children: [],
              },
              {
                name: "Michael Yadeta (R)",
                value: "House::MD::8::Michael Yadeta (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MA",
        value: "House::MA",
        children: [
          {
            name: "1 ",
            value: "House::MA::1 ",
            children: [
              {
                name: "Richard Neal (D)",
                value: "House::MA::1 ::Richard Neal (D)",
                children: [],
              },
              {
                name: "Alex Morse (D)",
                value: "House::MA::1 ::Alex Morse (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MA::2",
            children: [
              {
                name: "Jim McGovern (D)",
                value: "House::MA::2::Jim McGovern (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MA::3",
            children: [
              {
                name: "Lori Trahan (D)",
                value: "House::MA::3::Lori Trahan (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MA::4",
            children: [
              {
                name: "Jake Auchincloss (D)",
                value: "House::MA::4::Jake Auchincloss (D)",
                children: [],
              },
              {
                name: "Dave Cavell (D)",
                value: "House::MA::4::Dave Cavell (D)",
                children: [],
              },
              {
                name: "Becky Grossman (D)",
                value: "House::MA::4::Becky Grossman (D)",
                children: [],
              },
              {
                name: "Alan Khazei (D)",
                value: "House::MA::4::Alan Khazei (D)",
                children: [],
              },
              {
                name: "Ihssane Leckey (D)",
                value: "House::MA::4::Ihssane Leckey (D)",
                children: [],
              },
              {
                name: "Natalia Linos (D)",
                value: "House::MA::4::Natalia Linos (D)",
                children: [],
              },
              {
                name: "Nick Matthew (D)",
                value: "House::MA::4::Nick Matthew (D)",
                children: [],
              },
              {
                name: "Jesse Mermell (D)",
                value: "House::MA::4::Jesse Mermell (D)",
                children: [],
              },
              {
                name: "Herb Robinson (D)",
                value: "House::MA::4::Herb Robinson (D)",
                children: [],
              },
              {
                name: "Ben Sigel (D)",
                value: "House::MA::4::Ben Sigel (D)",
                children: [],
              },
              {
                name: "Chris Zannetos (D)",
                value: "House::MA::4::Chris Zannetos (D)",
                children: [],
              },
              {
                name: "Julie Hall (R)",
                value: "House::MA::4::Julie Hall (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::MA::5",
            children: [
              {
                name: "Raffaele Santino Depalma (D)",
                value: "House::MA::5::Raffaele Santino Depalma (D)",
                children: [],
              },
              {
                name: "Steven Pickney (D)",
                value: "House::MA::5::Steven Pickney (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::MA::6",
            children: [
              {
                name: "Seth Moulton (D)",
                value: "House::MA::6::Seth Moulton (D)",
                children: [],
              },
              {
                name: "Angus McQuilken (D)",
                value: "House::MA::6::Angus McQuilken (D)",
                children: [],
              },
              {
                name: "Nathaniel Mulcahy (D)",
                value: "House::MA::6::Nathaniel Mulcahy (D)",
                children: [],
              },
              {
                name: "Jamie Zahlaway Belsito (D)",
                value: "House::MA::6::Jamie Zahlaway Belsito (D)",
                children: [],
              },
              {
                name: "John Paul Moran (R)",
                value: "House::MA::6::John Paul Moran (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::MA::7",
            children: [
              {
                name: "Ayanna Pressley (D)",
                value: "House::MA::7::Ayanna Pressley (D)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::MA::8",
            children: [
              {
                name: "Robbie Goldstein (D)",
                value: "House::MA::8::Robbie Goldstein (D)",
                children: [],
              },
              {
                name: "Brianna Wu (D)",
                value: "House::MA::8::Brianna Wu (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::MA::9",
            children: [
              {
                name: "Bill Keating (D)",
                value: "House::MA::9::Bill Keating (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MI",
        value: "House::MI",
        children: [
          {
            name: "1",
            value: "House::MI::1",
            children: [
              {
                name: "Dana Alan Ferguson (D)",
                value: "House::MI::1::Dana Alan Ferguson (D)",
                children: [],
              },
              {
                name: "Linda O'Dell (D)",
                value: "House::MI::1::Linda O'Dell (D)",
                children: [],
              },
            ],
          },
          {
            name: "1 ",
            value: "House::MI::1 ",
            children: [
              {
                name: "Jack Bergman (R)",
                value: "House::MI::1 ::Jack Bergman (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MI::2",
            children: [
              {
                name: "Bryan Berghoef (D)",
                value: "House::MI::2::Bryan Berghoef (D)",
                children: [],
              },
              {
                name: "Bill Huizenga (R)",
                value: "House::MI::2::Bill Huizenga (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MI::3",
            children: [
              {
                name: "Hillary Scholten (D)",
                value: "House::MI::3::Hillary Scholten (D)",
                children: [],
              },
              {
                name: "Lynn Afendoulis (R)",
                value: "House::MI::3::Lynn Afendoulis (R)",
                children: [],
              },
              {
                name: "Joe Farrington (R)",
                value: "House::MI::3::Joe Farrington (R)",
                children: [],
              },
              {
                name: "Peter Meijer (R)",
                value: "House::MI::3::Peter Meijer (R)",
                children: [],
              },
              {
                name: "Tom Norton (R)",
                value: "House::MI::3::Tom Norton (R)",
                children: [],
              },
              {
                name: "Emily Rafi (R)",
                value: "House::MI::3::Emily Rafi (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MI::4",
            children: [
              {
                name: "Anthony Feig (D)",
                value: "House::MI::4::Anthony Feig (D)",
                children: [],
              },
              {
                name: "Jerry Hilliard (D)",
                value: "House::MI::4::Jerry Hilliard (D)",
                children: [],
              },
              {
                name: "John Moolenaar (R)",
                value: "House::MI::4::John Moolenaar (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::MI::5",
            children: [
              {
                name: "Dan Kildee (D)",
                value: "House::MI::5::Dan Kildee (D)",
                children: [],
              },
              {
                name: "Tim Kelly (R)",
                value: "House::MI::5::Tim Kelly (R)",
                children: [],
              },
              {
                name: "Earl Lackie (R)",
                value: "House::MI::5::Earl Lackie (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::MI::6",
            children: [
              {
                name: "Jon Hoadley (D)",
                value: "House::MI::6::Jon Hoadley (D)",
                children: [],
              },
              {
                name: "Jen Richarson (D)",
                value: "House::MI::6::Jen Richarson (D)",
                children: [],
              },
              {
                name: "Fred Upton (R)",
                value: "House::MI::6::Fred Upton (R)",
                children: [],
              },
              {
                name: "Elena Oekle (R)",
                value: "House::MI::6::Elena Oekle (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::MI::7",
            children: [
              {
                name: "Gertchen Driskell (D)",
                value: "House::MI::7::Gertchen Driskell (D)",
                children: [],
              },
              {
                name: "Tim Walkberg (R)",
                value: "House::MI::7::Tim Walkberg (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::MI::8",
            children: [
              {
                name: "Elissa Slotkin (D)",
                value: "House::MI::8::Elissa Slotkin (D)",
                children: [],
              },
              {
                name: "Mike Detmer (R)",
                value: "House::MI::8::Mike Detmer (R)",
                children: [],
              },
              {
                name: "Alan Hoover (R)",
                value: "House::MI::8::Alan Hoover (R)",
                children: [],
              },
              {
                name: "Paul Junge (R)",
                value: "House::MI::8::Paul Junge (R)",
                children: [],
              },
              {
                name: "Kristina Lyke (R)",
                value: "House::MI::8::Kristina Lyke (R)",
                children: [],
              },
              {
                name: "Nikki Snyder (R)",
                value: "House::MI::8::Nikki Snyder (R)",
                children: [],
              },
              {
                name: "Andy Levin (D)",
                value: "House::MI::8::Andy Levin (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::MI::9",
            children: [
              {
                name: "Gabi Grossbard (R)",
                value: "House::MI::9::Gabi Grossbard (R)",
                children: [],
              },
              {
                name: "Charles Langworthy (R)",
                value: "House::MI::9::Charles Langworthy (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::MI::10",
            children: [
              {
                name: "Kimberly Bizon (D)",
                value: "House::MI::10::Kimberly Bizon (D)",
                children: [],
              },
              {
                name: "Kelly Noland (D)",
                value: "House::MI::10::Kelly Noland (D)",
                children: [],
              },
              {
                name: "Shane Hernandez (R)",
                value: "House::MI::10::Shane Hernandez (R)",
                children: [],
              },
              {
                name: "Lisa McClain (R)",
                value: "House::MI::10::Lisa McClain (R)",
                children: [],
              },
              {
                name: "Bisham Singh (R)",
                value: "House::MI::10::Bisham Singh (R)",
                children: [],
              },
              {
                name: "Doug Slocum (R)",
                value: "House::MI::10::Doug Slocum (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::MI::11",
            children: [
              {
                name: "Haley Stevens (D)",
                value: "House::MI::11::Haley Stevens (D)",
                children: [],
              },
              {
                name: "Frank Acosta (R)",
                value: "House::MI::11::Frank Acosta (R)",
                children: [],
              },
              {
                name: "Kerry Bentivolio (R)",
                value: "House::MI::11::Kerry Bentivolio (R)",
                children: [],
              },
              {
                name: "Carmelita Greco (R)",
                value: "House::MI::11::Carmelita Greco (R)",
                children: [],
              },
              {
                name: "Eric Esshaki (R)",
                value: "House::MI::11::Eric Esshaki (R)",
                children: [],
              },
              {
                name: "Whittney Williams (R)",
                value: "House::MI::11::Whittney Williams (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::MI::12",
            children: [
              {
                name: "Debbie Dingell (D)",
                value: "House::MI::12::Debbie Dingell (D)",
                children: [],
              },
              {
                name: "Solomon Rajput (D)",
                value: "House::MI::12::Solomon Rajput (D)",
                children: [],
              },
              {
                name: "Jeff Jones (R)",
                value: "House::MI::12::Jeff Jones (R)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::MI::13",
            children: [
              {
                name: "Rashida Tlaib (D)",
                value: "House::MI::13::Rashida Tlaib (D)",
                children: [],
              },
              {
                name: "Brenda Jones (D)",
                value: "House::MI::13::Brenda Jones (D)",
                children: [],
              },
              {
                name: "David Dudenhoefer (R)",
                value: "House::MI::13::David Dudenhoefer (R)",
                children: [],
              },
              {
                name: "Alfred Lemmo (R)",
                value: "House::MI::13::Alfred Lemmo (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::MI::14",
            children: [
              {
                name: "Brenda lawrence (D)",
                value: "House::MI::14::Brenda lawrence (D)",
                children: [],
              },
              {
                name: "Terrance Morrison (D)",
                value: "House::MI::14::Terrance Morrison (D)",
                children: [],
              },
              {
                name: "Daryle Houston (R)",
                value: "House::MI::14::Daryle Houston (R)",
                children: [],
              },
              {
                name: "Robert Vance Patrick (R)",
                value: "House::MI::14::Robert Vance Patrick (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MN",
        value: "House::MN",
        children: [
          {
            name: "1",
            value: "House::MN::1",
            children: [
              {
                name: "Johnny Akzam (D)",
                value: "House::MN::1::Johnny Akzam (D)",
                children: [],
              },
              {
                name: "Dan Feehan (D)",
                value: "House::MN::1::Dan Feehan (D)",
                children: [],
              },
              {
                name: "Mark Schroepfer (D)",
                value: "House::MN::1::Mark Schroepfer (D)",
                children: [],
              },
              {
                name: "Rich Wright (D)",
                value: "House::MN::1::Rich Wright (D)",
                children: [],
              },
              {
                name: "Jim Hagedorn (R)",
                value: "House::MN::1::Jim Hagedorn (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MN::2",
            children: [
              {
                name: "Angie Craig (D)",
                value: "House::MN::2::Angie Craig (D)",
                children: [],
              },
              {
                name: "Regina Barr (R)",
                value: "House::MN::2::Regina Barr (R)",
                children: [],
              },
              {
                name: "Erika Cashin (R)",
                value: "House::MN::2::Erika Cashin (R)",
                children: [],
              },
              {
                name: "Tyler Kistner (R)",
                value: "House::MN::2::Tyler Kistner (R)",
                children: [],
              },
              {
                name: "Rick Olson (R)",
                value: "House::MN::2::Rick Olson (R)",
                children: [],
              },
              {
                name: "Kerry Zeiler (R)",
                value: "House::MN::2::Kerry Zeiler (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MN::3",
            children: [
              {
                name: "Dean Phillips (D)",
                value: "House::MN::3::Dean Phillips (D)",
                children: [],
              },
              {
                name: "Kendall Qualls (R)",
                value: "House::MN::3::Kendall Qualls (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MN::4",
            children: [
              {
                name: "Davis Sandbeck (D)",
                value: "House::MN::4::Davis Sandbeck (D)",
                children: [],
              },
              {
                name: "Travis Ekbom (R)",
                value: "House::MN::4::Travis Ekbom (R)",
                children: [],
              },
              {
                name: "Sia Lo (R)",
                value: "House::MN::4::Sia Lo (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::MN::5",
            children: [
              {
                name: "Ilhan Omar (D)",
                value: "House::MN::5::Ilhan Omar (D)",
                children: [],
              },
              {
                name: "John Mason (D)",
                value: "House::MN::5::John Mason (D)",
                children: [],
              },
              {
                name: "Antone Melton-Meaux (D)",
                value: "House::MN::5::Antone Melton-Meaux (D)",
                children: [],
              },
              {
                name: "Leila Shukri Adan (D)",
                value: "House::MN::5::Leila Shukri Adan (D)",
                children: [],
              },
              {
                name: "Haji Yussuf (D)",
                value: "House::MN::5::Haji Yussuf (D)",
                children: [],
              },
              {
                name: "Lacy Johnson (D)",
                value: "House::MN::5::Lacy Johnson (D)",
                children: [],
              },
              {
                name: "Danielle Stella (D)",
                value: "House::MN::5::Danielle Stella (D)",
                children: [],
              },
              {
                name: "Laverne Turner (D)",
                value: "House::MN::5::Laverne Turner (D)",
                children: [],
              },
              {
                name: "Lucia Vogel (D)",
                value: "House::MN::5::Lucia Vogel (D)",
                children: [],
              },
              {
                name: "Alley Waterbury (D)",
                value: "House::MN::5::Alley Waterbury (D)",
                children: [],
              },
              {
                name: "Dalia al-Aqidi (D)",
                value: "House::MN::5::Dalia al-Aqidi (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::MN::6",
            children: [
              {
                name: "Ian Todd (D)",
                value: "House::MN::6::Ian Todd (D)",
                children: [],
              },
              {
                name: "Tawnja Zahradka (D)",
                value: "House::MN::6::Tawnja Zahradka (D)",
                children: [],
              },
              {
                name: "Tom Emmer (R)",
                value: "House::MN::6::Tom Emmer (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::MN::7",
            children: [
              {
                name: "Collin Peterson (D)",
                value: "House::MN::7::Collin Peterson (D)",
                children: [],
              },
              {
                name: "Thaddeus Laugisch (D)",
                value: "House::MN::7::Thaddeus Laugisch (D)",
                children: [],
              },
              {
                name: "Noel Collis (R)",
                value: "House::MN::7::Noel Collis (R)",
                children: [],
              },
              {
                name: "Michelle Fischbach (R)",
                value: "House::MN::7::Michelle Fischbach (R)",
                children: [],
              },
              {
                name: "Dave Hughes (R)",
                value: "House::MN::7::Dave Hughes (R)",
                children: [],
              },
              {
                name: "Joel Novak (R)",
                value: "House::MN::7::Joel Novak (R)",
                children: [],
              },
              {
                name: "Jayesun Sherman (R)",
                value: "House::MN::7::Jayesun Sherman (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::MN::8",
            children: [
              {
                name: "Quinn Nystrom (D)",
                value: "House::MN::8::Quinn Nystrom (D)",
                children: [],
              },
              {
                name: "Noel Collis (D)",
                value: "House::MN::8::Noel Collis (D)",
                children: [],
              },
              {
                name: "Michelle Fischbach (D)",
                value: "House::MN::8::Michelle Fischbach (D)",
                children: [],
              },
              {
                name: "Dave Hughes (D)",
                value: "House::MN::8::Dave Hughes (D)",
                children: [],
              },
              {
                name: "Joel Novak (D)",
                value: "House::MN::8::Joel Novak (D)",
                children: [],
              },
              {
                name: "Jayesun Sherman (D)",
                value: "House::MN::8::Jayesun Sherman (D)",
                children: [],
              },
              {
                name: "Peter Stauber (R)",
                value: "House::MN::8::Peter Stauber (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MS",
        value: "House::MS",
        children: [
          {
            name: "1",
            value: "House::MS::1",
            children: [
              {
                name: "Trent Kelly (R)",
                value: "House::MS::1::Trent Kelly (R)",
                children: [],
              },
              {
                name: "Antonia Eliason (D)",
                value: "House::MS::1::Antonia Eliason (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MS::2",
            children: [
              {
                name: "Bennie Thompson (D)",
                value: "House::MS::2::Bennie Thompson (D)",
                children: [],
              },
              {
                name: "Thomas Carey (R)",
                value: "House::MS::2::Thomas Carey (R)",
                children: [],
              },
              {
                name: "Brain Flowers (R)",
                value: "House::MS::2::Brain Flowers (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MS::3",
            children: [
              {
                name: "Michael Guest (R)",
                value: "House::MS::3::Michael Guest (R)",
                children: [],
              },
              {
                name: "Dorothy Benford (D)",
                value: "House::MS::3::Dorothy Benford (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MS::4",
            children: [
              {
                name: "Steven Plazzo (R)",
                value: "House::MS::4::Steven Plazzo (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MO",
        value: "House::MO",
        children: [
          {
            name: "1",
            value: "House::MO::1",
            children: [
              {
                name: "William Lacy Clay (D)",
                value: "House::MO::1::William Lacy Clay (D)",
                children: [],
              },
              {
                name: "Katherine Bruckner (D)",
                value: "House::MO::1::Katherine Bruckner (D)",
                children: [],
              },
              {
                name: "Cori Bush (D)",
                value: "House::MO::1::Cori Bush (D)",
                children: [],
              },
              {
                name: "Winnie Heartstrong (R)",
                value: "House::MO::1::Winnie Heartstrong (R)",
                children: [],
              },
              {
                name: "Anthony Rogers (R)",
                value: "House::MO::1::Anthony Rogers (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::MO::2",
            children: [
              {
                name: "Jill Schupp (D)",
                value: "House::MO::2::Jill Schupp (D)",
                children: [],
              },
              {
                name: "Ann Wagner (R)",
                value: "House::MO::2::Ann Wagner (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::MO::3",
            children: [
              {
                name: "Dennis Oglesby (D)",
                value: "House::MO::3::Dennis Oglesby (D)",
                children: [],
              },
              {
                name: "Megan Rezabek (D)",
                value: "House::MO::3::Megan Rezabek (D)",
                children: [],
              },
              {
                name: "Blaine Luetkemeyer (R)",
                value: "House::MO::3::Blaine Luetkemeyer (R)",
                children: [],
              },
              {
                name: "Jeffrey Nowak (R)",
                value: "House::MO::3::Jeffrey Nowak (R)",
                children: [],
              },
              {
                name: "Lynette Trares (R)",
                value: "House::MO::3::Lynette Trares (R)",
                children: [],
              },
              {
                name: "Brandon Wilkinson (R)",
                value: "House::MO::3::Brandon Wilkinson (R)",
                children: [],
              },
              {
                name: "Adela Wisdom (R)",
                value: "House::MO::3::Adela Wisdom (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::MO::4",
            children: [
              {
                name: "Lindsey Simmons (D)",
                value: "House::MO::4::Lindsey Simmons (D)",
                children: [],
              },
              {
                name: "Vicky Hartzler (R)",
                value: "House::MO::4::Vicky Hartzler (R)",
                children: [],
              },
              {
                name: "Neal Gist (R)",
                value: "House::MO::4::Neal Gist (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::MO::5",
            children: [
              {
                name: "Emanuel Cleaver (D)",
                value: "House::MO::5::Emanuel Cleaver (D)",
                children: [],
              },
              {
                name: "Maite Salazar (D)",
                value: "House::MO::5::Maite Salazar (D)",
                children: [],
              },
              {
                name: "Jerry Barham (R)",
                value: "House::MO::5::Jerry Barham (R)",
                children: [],
              },
              {
                name: "Clay Chastain (R)",
                value: "House::MO::5::Clay Chastain (R)",
                children: [],
              },
              {
                name: "Ryan Derks (R)",
                value: "House::MO::5::Ryan Derks (R)",
                children: [],
              },
              {
                name: "R. H. Hess (R)",
                value: "House::MO::5::R. H. Hess (R)",
                children: [],
              },
              {
                name: "Richonda Oaks (R)",
                value: "House::MO::5::Richonda Oaks (R)",
                children: [],
              },
              {
                name: "Weldon Woodward (R)",
                value: "House::MO::5::Weldon Woodward (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::MO::6",
            children: [
              {
                name: "Ramona Farris (D)",
                value: "House::MO::6::Ramona Farris (D)",
                children: [],
              },
              {
                name: "Henry Martin (D)",
                value: "House::MO::6::Henry Martin (D)",
                children: [],
              },
              {
                name: "Gena Ross (D)",
                value: "House::MO::6::Gena Ross (D)",
                children: [],
              },
              {
                name: "Donald Robert Sartain (D)",
                value: "House::MO::6::Donald Robert Sartain (D)",
                children: [],
              },
              {
                name: "Charles West (D)",
                value: "House::MO::6::Charles West (D)",
                children: [],
              },
              {
                name: "Sam Graves (R)",
                value: "House::MO::6::Sam Graves (R)",
                children: [],
              },
              {
                name: "Christopher Ryan (R)",
                value: "House::MO::6::Christopher Ryan (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::MO::7",
            children: [
              {
                name: "Teresa Montseny (D)",
                value: "House::MO::7::Teresa Montseny (D)",
                children: [],
              },
              {
                name: "Billy Long (R)",
                value: "House::MO::7::Billy Long (R)",
                children: [],
              },
              {
                name: "Steve Chentnik (R)",
                value: "House::MO::7::Steve Chentnik (R)",
                children: [],
              },
              {
                name: "Eric Harleman (R)",
                value: "House::MO::7::Eric Harleman (R)",
                children: [],
              },
              {
                name: "Camille Lombardi-Olive (R)",
                value: "House::MO::7::Camille Lombardi-Olive (R)",
                children: [],
              },
              {
                name: "Kevin VanStory (R)",
                value: "House::MO::7::Kevin VanStory (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::MO::8",
            children: [
              {
                name: "Kathy Ellis (D)",
                value: "House::MO::8::Kathy Ellis (D)",
                children: [],
              },
              {
                name: "Jason Smith (R)",
                value: "House::MO::8::Jason Smith (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "MT",
        value: "House::MT",
        children: [
          {
            name: 1,
            value: "House::MT::1",
            children: [
              {
                name: "Kathleen Williams (D)",
                value: "House::MT::1::Kathleen Williams (D)",
                children: [],
              },
              {
                name: "Tom Winter (D)",
                value: "House::MT::1::Tom Winter (D)",
                children: [],
              },
              {
                name: "Joe Dooling (R)",
                value: "House::MT::1::Joe Dooling (R)",
                children: [],
              },
              {
                name: "John Evankovich (R)",
                value: "House::MT::1::John Evankovich (R)",
                children: [],
              },
              {
                name: "Debra Lamm (R)",
                value: "House::MT::1::Debra Lamm (R)",
                children: [],
              },
              {
                name: "Mark McGinley (R)",
                value: "House::MT::1::Mark McGinley (R)",
                children: [],
              },
              {
                name: "Matt Rosendale (R)",
                value: "House::MT::1::Matt Rosendale (R)",
                children: [],
              },
              {
                name: "Corey Stapleton (R)",
                value: "House::MT::1::Corey Stapleton (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NE",
        value: "House::NE",
        children: [
          {
            name: "1",
            value: "House::NE::1",
            children: [
              {
                name: "Jeffery Fortenberry (R)",
                value: "House::NE::1::Jeffery Fortenberry (R)",
                children: [],
              },
              {
                name: "Kate Bolz (D)",
                value: "House::NE::1::Kate Bolz (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NE::2",
            children: [
              {
                name: "Don Bacon (R)",
                value: "House::NE::2::Don Bacon (R)",
                children: [],
              },
              {
                name: "Kara Eastman (D)",
                value: "House::NE::2::Kara Eastman (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NE::3",
            children: [
              {
                name: "Adrian Smith (R)",
                value: "House::NE::3::Adrian Smith (R)",
                children: [],
              },
              {
                name: "Mark Elworth Jr. (D)",
                value: "House::NE::3::Mark Elworth Jr. (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NV",
        value: "House::NV",
        children: [
          {
            name: "1",
            value: "House::NV::1",
            children: [
              {
                name: "Dina Titus (Incumbent) (D)",
                value: "House::NV::1::Dina Titus (Incumbent) (D)",
                children: [],
              },
              {
                name: "Allen Rheinhart (D)",
                value: "House::NV::1::Allen Rheinhart (D)",
                children: [],
              },
              {
                name: "Anthony Thomas Jr. (D)",
                value: "House::NV::1::Anthony Thomas Jr. (D)",
                children: [],
              },
              {
                name: "Joyce Bentley (R)",
                value: "House::NV::1::Joyce Bentley (R)",
                children: [],
              },
              {
                name: "Josh Elliott (R)",
                value: "House::NV::1::Josh Elliott (R)",
                children: [],
              },
              {
                name: "Eddie Hamilton (R)",
                value: "House::NV::1::Eddie Hamilton (R)",
                children: [],
              },
              {
                name: "Citlaly Larios-Elias (R)",
                value: "House::NV::1::Citlaly Larios-Elias (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NV::2",
            children: [
              {
                name: "Patricia Ackerman (D)",
                value: "House::NV::2::Patricia Ackerman (D)",
                children: [],
              },
              {
                name: "Ed Cohen (D)",
                value: "House::NV::2::Ed Cohen (D)",
                children: [],
              },
              {
                name: "Reynaldo Hernandez (D)",
                value: "House::NV::2::Reynaldo Hernandez (D)",
                children: [],
              },
              {
                name: "Clint Koble (D)",
                value: "House::NV::2::Clint Koble (D)",
                children: [],
              },
              {
                name: "Ian Luetkehans (D)",
                value: "House::NV::2::Ian Luetkehans (D)",
                children: [],
              },
              {
                name: "Steven Schiffman (D)",
                value: "House::NV::2::Steven Schiffman (D)",
                children: [],
              },
              {
                name: "Rick Shepherd (D)",
                value: "House::NV::2::Rick Shepherd (D)",
                children: [],
              },
              {
                name: "Mark Amodei (R)",
                value: "House::NV::2::Mark Amodei (R)",
                children: [],
              },
              {
                name: "Joel Beck (R)",
                value: "House::NV::2::Joel Beck (R)",
                children: [],
              },
              {
                name: "Jesse Hurley (R)",
                value: "House::NV::2::Jesse Hurley (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NV::3",
            children: [
              {
                name: "Susie Lee (Incumbent) (D)",
                value: "House::NV::3::Susie Lee (Incumbent) (D)",
                children: [],
              },
              {
                name: "Dennis Sullivan (D)",
                value: "House::NV::3::Dennis Sullivan (D)",
                children: [],
              },
              {
                name: "Tiffany Ann Watson (D)",
                value: "House::NV::3::Tiffany Ann Watson (D)",
                children: [],
              },
              {
                name: "Brian Nadell (R)",
                value: "House::NV::3::Brian Nadell (R)",
                children: [],
              },
              {
                name: "Corwin Newberry (R)",
                value: "House::NV::3::Corwin Newberry (R)",
                children: [],
              },
              {
                name: "Mindy Robinson (R)",
                value: "House::NV::3::Mindy Robinson (R)",
                children: [],
              },
              {
                name: "Daniel Rodimer (R)",
                value: "House::NV::3::Daniel Rodimer (R)",
                children: [],
              },
              {
                name: "Dan Schwartz (R)",
                value: "House::NV::3::Dan Schwartz (R)",
                children: [],
              },
              {
                name: "Victor Willert (R)",
                value: "House::NV::3::Victor Willert (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::NV::4",
            children: [
              {
                name: "Steven Horsford (Incumbent) (D)",
                value: "House::NV::4::Steven Horsford (Incumbent) (D)",
                children: [],
              },
              {
                name: "George Brucato (D)",
                value: "House::NV::4::George Brucato (D)",
                children: [],
              },
              {
                name: "Chris Colley (D)",
                value: "House::NV::4::Chris Colley (D)",
                children: [],
              },
              {
                name: "Gabrielle D'Ayr (D)",
                value: "House::NV::4::Gabrielle D'Ayr (D)",
                children: [],
              },
              {
                name: "Jennifer Eason (D)",
                value: "House::NV::4::Jennifer Eason (D)",
                children: [],
              },
              {
                name: "Gregory Kempton (D)",
                value: "House::NV::4::Gregory Kempton (D)",
                children: [],
              },
              {
                name: "Rosalie Bingham (R)",
                value: "House::NV::4::Rosalie Bingham (R)",
                children: [],
              },
              {
                name: "Leo Blundo (R)",
                value: "House::NV::4::Leo Blundo (R)",
                children: [],
              },
              {
                name: "Jim Marchant (R)",
                value: "House::NV::4::Jim Marchant (R)",
                children: [],
              },
              {
                name: "Charles Navarro (R)",
                value: "House::NV::4::Charles Navarro (R)",
                children: [],
              },
              {
                name: "Sam Peters (R)",
                value: "House::NV::4::Sam Peters (R)",
                children: [],
              },
              {
                name: "Randi Reed (R)",
                value: "House::NV::4::Randi Reed (R)",
                children: [],
              },
              {
                name: "Lisa Song Sutton (R)",
                value: "House::NV::4::Lisa Song Sutton (R)",
                children: [],
              },
              {
                name: "Rebecca Wood (R)",
                value: "House::NV::4::Rebecca Wood (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NH",
        value: "House::NH",
        children: [
          {
            name: "1",
            value: "House::NH::1",
            children: [
              {
                name: "Chris Pappas (D)",
                value: "House::NH::1::Chris Pappas (D)",
                children: [],
              },
              {
                name: "Kevin Freeman (D)",
                value: "House::NH::1::Kevin Freeman (D)",
                children: [],
              },
              {
                name: "Matt Mayberry (R)",
                value: "House::NH::1::Matt Mayberry (R)",
                children: [],
              },
              {
                name: "Matt Mowers (R)",
                value: "House::NH::1::Matt Mowers (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NH::2",
            children: [
              {
                name: "Annie Kuster (D)",
                value: "House::NH::2::Annie Kuster (D)",
                children: [],
              },
              {
                name: "Lynne Blankenbeker (R)",
                value: "House::NH::2::Lynne Blankenbeker (R)",
                children: [],
              },
              {
                name: "Eli Clemmer (R)",
                value: "House::NH::2::Eli Clemmer (R)",
                children: [],
              },
              {
                name: "Steve Negron (R)",
                value: "House::NH::2::Steve Negron (R)",
                children: [],
              },
              {
                name: "Gilead Towne (R)",
                value: "House::NH::2::Gilead Towne (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NJ",
        value: "House::NJ",
        children: [
          {
            name: "1",
            value: "House::NJ::1",
            children: [
              {
                name: "Donald Norcross (D)",
                value: "House::NJ::1::Donald Norcross (D)",
                children: [],
              },
              {
                name: "Claire Gustafson (R)",
                value: "House::NJ::1::Claire Gustafson (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NJ::2",
            children: [
              {
                name: "Will Cunningham (D)",
                value: "House::NJ::2::Will Cunningham (D)",
                children: [],
              },
              {
                name: "John Francis III (D)",
                value: "House::NJ::2::John Francis III (D)",
                children: [],
              },
              {
                name: "Brigid Callahan Harrison (D)",
                value: "House::NJ::2::Brigid Callahan Harrison (D)",
                children: [],
              },
              {
                name: "Amy Kennedy (D)",
                value: "House::NJ::2::Amy Kennedy (D)",
                children: [],
              },
              {
                name: "Robert Turkavage (D)",
                value: "House::NJ::2::Robert Turkavage (D)",
                children: [],
              },
              {
                name: "Jeff Van Drew (R)",
                value: "House::NJ::2::Jeff Van Drew (R)",
                children: [],
              },
              {
                name: "Bob Patterson (R)",
                value: "House::NJ::2::Bob Patterson (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NJ::3",
            children: [
              {
                name: "Andrew Kim (D)",
                value: "House::NJ::3::Andrew Kim (D)",
                children: [],
              },
              {
                name: "Kate Gibbs (R)",
                value: "House::NJ::3::Kate Gibbs (R)",
                children: [],
              },
              {
                name: "David Richter (R)",
                value: "House::NJ::3::David Richter (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::NJ::4",
            children: [
              {
                name: "David Applefield (D)",
                value: "House::NJ::4::David Applefield (D)",
                children: [],
              },
              {
                name: "Christine Conforti (D)",
                value: "House::NJ::4::Christine Conforti (D)",
                children: [],
              },
              {
                name: "Stephanie Schmid (D)",
                value: "House::NJ::4::Stephanie Schmid (D)",
                children: [],
              },
              {
                name: "Chris Smith (R)",
                value: "House::NJ::4::Chris Smith (R)",
                children: [],
              },
              {
                name: "Alter Eliezer Richter (R)",
                value: "House::NJ::4::Alter Eliezer Richter (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::NJ::5",
            children: [
              {
                name: "Josh Gottheimer (D)",
                value: "House::NJ::5::Josh Gottheimer (D)",
                children: [],
              },
              {
                name: "Arati Kreibich (D)",
                value: "House::NJ::5::Arati Kreibich (D)",
                children: [],
              },
              {
                name: "James Baldini (R)",
                value: "House::NJ::5::James Baldini (R)",
                children: [],
              },
              {
                name: "Hector Castillo (R)",
                value: "House::NJ::5::Hector Castillo (R)",
                children: [],
              },
              {
                name: "John McCann (R)",
                value: "House::NJ::5::John McCann (R)",
                children: [],
              },
              {
                name: "Frank Pallotta (R)",
                value: "House::NJ::5::Frank Pallotta (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::NJ::6",
            children: [
              {
                name: "Frank Pallone (D)",
                value: "House::NJ::6::Frank Pallone (D)",
                children: [],
              },
              {
                name: "Amani Al-Khatahtbeh (D)",
                value: "House::NJ::6::Amani Al-Khatahtbeh (D)",
                children: [],
              },
              {
                name: "Russell Cirincione (D)",
                value: "House::NJ::6::Russell Cirincione (D)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::NJ::7",
            children: [
              {
                name: "Tom Malinowsky (D)",
                value: "House::NJ::7::Tom Malinowsky (D)",
                children: [],
              },
              {
                name: "Raafat Barsoom (R)",
                value: "House::NJ::7::Raafat Barsoom (R)",
                children: [],
              },
              {
                name: "Thomas Kean Jr. (R)",
                value: "House::NJ::7::Thomas Kean Jr. (R)",
                children: [],
              },
              {
                name: "Tom Phillips (R)",
                value: "House::NJ::7::Tom Phillips (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::NJ::8",
            children: [
              {
                name: "Albio Sires (D)",
                value: "House::NJ::8::Albio Sires (D)",
                children: [],
              },
              {
                name: "Hector Oseguera (D)",
                value: "House::NJ::8::Hector Oseguera (D)",
                children: [],
              },
              {
                name: "Will Sheehan (D)",
                value: "House::NJ::8::Will Sheehan (D)",
                children: [],
              },
              {
                name: "Jason Mushnick (R)",
                value: "House::NJ::8::Jason Mushnick (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::NJ::9",
            children: [
              {
                name: "Bill Pascrell (D)",
                value: "House::NJ::9::Bill Pascrell (D)",
                children: [],
              },
              {
                name: "Alp Basaran (D)",
                value: "House::NJ::9::Alp Basaran (D)",
                children: [],
              },
              {
                name: "Zinovia Spezakis (D)",
                value: "House::NJ::9::Zinovia Spezakis (D)",
                children: [],
              },
              {
                name: "Billy Prempeh (R)",
                value: "House::NJ::9::Billy Prempeh (R)",
                children: [],
              },
              {
                name: "Timothy Walsh (R)",
                value: "House::NJ::9::Timothy Walsh (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::NJ::10",
            children: [
              {
                name: "Donald Payne Jr. (D)",
                value: "House::NJ::10::Donald Payne Jr. (D)",
                children: [],
              },
              {
                name: "John Flora (D)",
                value: "House::NJ::10::John Flora (D)",
                children: [],
              },
              {
                name: "Eugene Mazo (D)",
                value: "House::NJ::10::Eugene Mazo (D)",
                children: [],
              },
              {
                name: "Jennifer Zinone (R)",
                value: "House::NJ::10::Jennifer Zinone (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::NJ::11",
            children: [
              {
                name: "Mikkie Sherrill (D)",
                value: "House::NJ::11::Mikkie Sherrill (D)",
                children: [],
              },
              {
                name: "Rosemary Becchi (R)",
                value: "House::NJ::11::Rosemary Becchi (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::NJ::12",
            children: [
              {
                name: "Bonnie Watson Coleman (D)",
                value: "House::NJ::12::Bonnie Watson Coleman (D)",
                children: [],
              },
              {
                name: "Lisa McCormick (D)",
                value: "House::NJ::12::Lisa McCormick (D)",
                children: [],
              },
              {
                name: "Mark Razzoli (R)",
                value: "House::NJ::12::Mark Razzoli (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NM",
        value: "House::NM",
        children: [
          {
            name: "1",
            value: "House::NM::1",
            children: [
              {
                name: "Debra Haaland (D)",
                value: "House::NM::1::Debra Haaland (D)",
                children: [],
              },
              {
                name: "Michelle Garcia Holmes (R)",
                value: "House::NM::1::Michelle Garcia Holmes (R)",
                children: [],
              },
              {
                name: "Brett Kokinadis (R)",
                value: "House::NM::1::Brett Kokinadis (R)",
                children: [],
              },
              {
                name: "Jared Vander Dussen (R)",
                value: "House::NM::1::Jared Vander Dussen (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NM::2",
            children: [
              {
                name: "Xochitl Torres Small (D)",
                value: "House::NM::2::Xochitl Torres Small (D)",
                children: [],
              },
              {
                name: "Claire Chase (R)",
                value: "House::NM::2::Claire Chase (R)",
                children: [],
              },
              {
                name: "Yvette Herrell (R)",
                value: "House::NM::2::Yvette Herrell (R)",
                children: [],
              },
              {
                name: "Chris Mathys (R)",
                value: "House::NM::2::Chris Mathys (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NM::3",
            children: [
              {
                name: "John Blair (D)",
                value: "House::NM::3::John Blair (D)",
                children: [],
              },
              {
                name: "Teresa Leger Fernandez (D)",
                value: "House::NM::3::Teresa Leger Fernandez (D)",
                children: [],
              },
              {
                name: "Laura Montoya (D)",
                value: "House::NM::3::Laura Montoya (D)",
                children: [],
              },
              {
                name: "Valerie Plame (D)",
                value: "House::NM::3::Valerie Plame (D)",
                children: [],
              },
              {
                name: "Joseph Sanchez (D)",
                value: "House::NM::3::Joseph Sanchez (D)",
                children: [],
              },
              {
                name: "Marco Serna (D)",
                value: "House::NM::3::Marco Serna (D)",
                children: [],
              },
              {
                name: "Kyle Tisdel (D)",
                value: "House::NM::3::Kyle Tisdel (D)",
                children: [],
              },
              {
                name: "Karen Bedonie (R)",
                value: "House::NM::3::Karen Bedonie (R)",
                children: [],
              },
              {
                name: "Alexis Johnson (R)",
                value: "House::NM::3::Alexis Johnson (R)",
                children: [],
              },
              {
                name: "Harry Montoya (R)",
                value: "House::NM::3::Harry Montoya (R)",
                children: [],
              },
              {
                name: "Angela Gale Morales (R)",
                value: "House::NM::3::Angela Gale Morales (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NY",
        value: "House::NY",
        children: [
          {
            name: "1",
            value: "House::NY::1",
            children: [
              {
                name: "Greg Fischer (D)",
                value: "House::NY::1::Greg Fischer (D)",
                children: [],
              },
              {
                name: "Bridget M. Fleming (D)",
                value: "House::NY::1::Bridget M. Fleming (D)",
                children: [],
              },
              {
                name: "Perry Gershon (D)",
                value: "House::NY::1::Perry Gershon (D)",
                children: [],
              },
              {
                name: "Nancy Goroff (D)",
                value: "House::NY::1::Nancy Goroff (D)",
                children: [],
              },
              {
                name: "Lee Zeldin (R)",
                value: "House::NY::1::Lee Zeldin (R)",
                children: [],
              },
              {
                name: "David Gokhstein (R)",
                value: "House::NY::1::David Gokhstein (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NY::2",
            children: [
              {
                name: "Jackie Gordon (D)",
                value: "House::NY::2::Jackie Gordon (D)",
                children: [],
              },
              {
                name: "Patricia Maher (D)",
                value: "House::NY::2::Patricia Maher (D)",
                children: [],
              },
              {
                name: "Andrew Garbarino (R)",
                value: "House::NY::2::Andrew Garbarino (R)",
                children: [],
              },
              {
                name: "Michael LiPetri (R)",
                value: "House::NY::2::Michael LiPetri (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NY::3",
            children: [
              {
                name: "Tom Suozzi (D)",
                value: "House::NY::3::Tom Suozzi (D)",
                children: [],
              },
              {
                name: "Melanie D'Arrigo (D)",
                value: "House::NY::3::Melanie D'Arrigo (D)",
                children: [],
              },
              {
                name: "Michael Weinstock (D)",
                value: "House::NY::3::Michael Weinstock (D)",
                children: [],
              },
              {
                name: "George Devolder-Santos (R)",
                value: "House::NY::3::George Devolder-Santos (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::NY::4",
            children: [
              {
                name: "Kathleen Rice (D)",
                value: "House::NY::4::Kathleen Rice (D)",
                children: [],
              },
              {
                name: "Cindy Grosz (R)",
                value: "House::NY::4::Cindy Grosz (R)",
                children: [],
              },
              {
                name: "Douglas Tuman (R)",
                value: "House::NY::4::Douglas Tuman (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::NY::5",
            children: [
              {
                name: "Gregory W. Meeks (D)",
                value: "House::NY::5::Gregory W. Meeks (D)",
                children: [],
              },
              {
                name: "Shaniyat Chowdhury (D)",
                value: "House::NY::5::Shaniyat Chowdhury (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::NY::6",
            children: [
              {
                name: "Grace Meng (D)",
                value: "House::NY::6::Grace Meng (D)",
                children: [],
              },
              {
                name: "Sandra Choi (D)",
                value: "House::NY::6::Sandra Choi (D)",
                children: [],
              },
              {
                name: "Melquiades Gagarin (D)",
                value: "House::NY::6::Melquiades Gagarin (D)",
                children: [],
              },
              {
                name: "Thomas Zmich (R)",
                value: "House::NY::6::Thomas Zmich (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::NY::7",
            children: [
              {
                name: "Nydia Velazquez (Incumbent) (D)",
                value: "House::NY::7::Nydia Velazquez (Incumbent) (D)",
                children: [],
              },
              {
                name: "Paperboy Prince (D)",
                value: "House::NY::7::Paperboy Prince (D)",
                children: [],
              },
              {
                name: "Brian Kelly (R)",
                value: "House::NY::7::Brian Kelly (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::NY::8",
            children: [
              {
                name: "Hakeem Jeffries (D)",
                value: "House::NY::8::Hakeem Jeffries (D)",
                children: [],
              },
              {
                name: "Garfield Wallace (R)",
                value: "House::NY::8::Garfield Wallace (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::NY::9",
            children: [
              {
                name: "Yvette D. Clarke (D)",
                value: "House::NY::9::Yvette D. Clarke (D)",
                children: [],
              },
              {
                name: "Constantin Jean-Pierre (R)",
                value: "House::NY::9::Constantin Jean-Pierre (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::NY::10",
            children: [
              {
                name: "Jerrold Nadler (D)",
                value: "House::NY::10::Jerrold Nadler (D)",
                children: [],
              },
              {
                name: "Lindsey Boylan (D)",
                value: "House::NY::10::Lindsey Boylan (D)",
                children: [],
              },
              {
                name: "Jonathan Herzog (D)",
                value: "House::NY::10::Jonathan Herzog (D)",
                children: [],
              },
              {
                name: "Cathy Bernstein (R)",
                value: "House::NY::10::Cathy Bernstein (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::NY::11",
            children: [
              {
                name: "Max Rose (D)",
                value: "House::NY::11::Max Rose (D)",
                children: [],
              },
              {
                name: "Joe Caldarera (R)",
                value: "House::NY::11::Joe Caldarera (R)",
                children: [],
              },
              {
                name: "Nicole Malliotakis (R)",
                value: "House::NY::11::Nicole Malliotakis (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::NY::12",
            children: [
              {
                name: "Carolyn B. Maloney (Incumbent) (D)",
                value: "House::NY::12::Carolyn B. Maloney (Incumbent) (D)",
                children: [],
              },
              {
                name: "Lauren Ashcraft (D)",
                value: "House::NY::12::Lauren Ashcraft (D)",
                children: [],
              },
              {
                name: "Peter Harrison (D)",
                value: "House::NY::12::Peter Harrison (D)",
                children: [],
              },
              {
                name: "Suraj Patel (D)",
                value: "House::NY::12::Suraj Patel (D)",
                children: [],
              },
              {
                name: "Carlos Santiago-Cano (R)",
                value: "House::NY::12::Carlos Santiago-Cano (R)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::NY::13",
            children: [
              {
                name: "Adriano Espaillat (D)",
                value: "House::NY::13::Adriano Espaillat (D)",
                children: [],
              },
              {
                name: "James Felton Keith (D)",
                value: "House::NY::13::James Felton Keith (D)",
                children: [],
              },
              {
                name: "Ramon Rodriguez (D)",
                value: "House::NY::13::Ramon Rodriguez (D)",
                children: [],
              },
              {
                name: "Lovelynn Gwinn (R)",
                value: "House::NY::13::Lovelynn Gwinn (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::NY::14",
            children: [
              {
                name: "Alexandria Ocasio-Cortez (D)",
                value: "House::NY::14::Alexandria Ocasio-Cortez (D)",
                children: [],
              },
              {
                name: "Michelle Caruso-Cabrera (D)",
                value: "House::NY::14::Michelle Caruso-Cabrera (D)",
                children: [],
              },
              {
                name: "Badrun Khan (D)",
                value: "House::NY::14::Badrun Khan (D)",
                children: [],
              },
              {
                name: "Sam Sloan (D)",
                value: "House::NY::14::Sam Sloan (D)",
                children: [],
              },
              {
                name: "John Commings (R)",
                value: "House::NY::14::John Commings (R)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::NY::15",
            children: [
              {
                name: "Frangell Basora (D)",
                value: "House::NY::15::Frangell Basora (D)",
                children: [],
              },
              {
                name: "Michael Blake (D)",
                value: "House::NY::15::Michael Blake (D)",
                children: [],
              },
              {
                name: "Ruben Diaz (D)",
                value: "House::NY::15::Ruben Diaz (D)",
                children: [],
              },
              {
                name: "Mark Escoffery-Bey (D)",
                value: "House::NY::15::Mark Escoffery-Bey (D)",
                children: [],
              },
              {
                name: "Samelys Lopez (D)",
                value: "House::NY::15::Samelys Lopez (D)",
                children: [],
              },
              {
                name: "Melissa Mark-Viverito (D)",
                value: "House::NY::15::Melissa Mark-Viverito (D)",
                children: [],
              },
              {
                name: "Chivona Newsome (D)",
                value: "House::NY::15::Chivona Newsome (D)",
                children: [],
              },
              {
                name: "Julio Pabon (D)",
                value: "House::NY::15::Julio Pabon (D)",
                children: [],
              },
              {
                name: "Tomas Ramos (D)",
                value: "House::NY::15::Tomas Ramos (D)",
                children: [],
              },
              {
                name: "Ydanis Rodriguez (D)",
                value: "House::NY::15::Ydanis Rodriguez (D)",
                children: [],
              },
              {
                name: "Marlene Tapper (D)",
                value: "House::NY::15::Marlene Tapper (D)",
                children: [],
              },
              {
                name: "Ritchie Torres (D)",
                value: "House::NY::15::Ritchie Torres (D)",
                children: [],
              },
              {
                name: "Orlando Molina (R)",
                value: "House::NY::15::Orlando Molina (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::NY::16",
            children: [
              {
                name: "Eliot Engel ( (D)",
                value: "House::NY::16::Eliot Engel ( (D)",
                children: [],
              },
              {
                name: "Jamaal Bowman (D)",
                value: "House::NY::16::Jamaal Bowman (D)",
                children: [],
              },
              {
                name: "Chris Fink (D)",
                value: "House::NY::16::Chris Fink (D)",
                children: [],
              },
              {
                name: "Andom Ghebreghiorgis (D)",
                value: "House::NY::16::Andom Ghebreghiorgis (D)",
                children: [],
              },
              {
                name: "Sammy Ravelo (D)",
                value: "House::NY::16::Sammy Ravelo (D)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::NY::17",
            children: [
              {
                name: "David Buchwald (D)",
                value: "House::NY::17::David Buchwald (D)",
                children: [],
              },
              {
                name: "David Carlucci (D)",
                value: "House::NY::17::David Carlucci (D)",
                children: [],
              },
              {
                name: "Asha Castleberry-Hernandez (D)",
                value: "House::NY::17::Asha Castleberry-Hernandez (D)",
                children: [],
              },
              {
                name: "Evelyn Farkas (D)",
                value: "House::NY::17::Evelyn Farkas (D)",
                children: [],
              },
              {
                name: "Allison Fine (D)",
                value: "House::NY::17::Allison Fine (D)",
                children: [],
              },
              {
                name: "Mondaire Jones (D)",
                value: "House::NY::17::Mondaire Jones (D)",
                children: [],
              },
              {
                name: "Catherine Parker (D)",
                value: "House::NY::17::Catherine Parker (D)",
                children: [],
              },
              {
                name: "Adam Schleifer (D)",
                value: "House::NY::17::Adam Schleifer (D)",
                children: [],
              },
              {
                name: "Yehudis Gottesfeld (R)",
                value: "House::NY::17::Yehudis Gottesfeld (R)",
                children: [],
              },
              {
                name: "Maureen McArdle Schulman (R)",
                value: "House::NY::17::Maureen McArdle Schulman (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::NY::18",
            children: [
              {
                name: "Sean Maloney (D)",
                value: "House::NY::18::Sean Maloney (D)",
                children: [],
              },
              {
                name: "Chele Farley (R)",
                value: "House::NY::18::Chele Farley (R)",
                children: [],
              },
            ],
          },
          {
            name: "19",
            value: "House::NY::19",
            children: [
              {
                name: "Antonio Delgado (D)",
                value: "House::NY::19::Antonio Delgado (D)",
                children: [],
              },
              {
                name: "Ola Hawatmeh (R)",
                value: "House::NY::19::Ola Hawatmeh (R)",
                children: [],
              },
              {
                name: "Kyle Van De Water (R)",
                value: "House::NY::19::Kyle Van De Water (R)",
                children: [],
              },
            ],
          },
          {
            name: "20",
            value: "House::NY::20",
            children: [
              {
                name: "Paul Tonko (D)",
                value: "House::NY::20::Paul Tonko (D)",
                children: [],
              },
              {
                name: "Riley Seaburg (D)",
                value: "House::NY::20::Riley Seaburg (D)",
                children: [],
              },
              {
                name: "Elizabeth Joy (R)",
                value: "House::NY::20::Elizabeth Joy (R)",
                children: [],
              },
              {
                name: "Michael Seney (R)",
                value: "House::NY::20::Michael Seney (R)",
                children: [],
              },
            ],
          },
          {
            name: "21",
            value: "House::NY::21",
            children: [
              {
                name: "Tedra Cobb (D)",
                value: "House::NY::21::Tedra Cobb (D)",
                children: [],
              },
              {
                name: "Elise Stefanik (R)",
                value: "House::NY::21::Elise Stefanik (R)",
                children: [],
              },
            ],
          },
          {
            name: "22",
            value: "House::NY::22",
            children: [
              {
                name: "Anthony Brindisi (D)",
                value: "House::NY::22::Anthony Brindisi (D)",
                children: [],
              },
              {
                name: "George Phillips (R)",
                value: "House::NY::22::George Phillips (R)",
                children: [],
              },
              {
                name: "Claudia Tenney (R)",
                value: "House::NY::22::Claudia Tenney (R)",
                children: [],
              },
            ],
          },
          {
            name: "23",
            value: "House::NY::23",
            children: [
              {
                name: "Tracy Mitrano (D)",
                value: "House::NY::23::Tracy Mitrano (D)",
                children: [],
              },
              {
                name: "Scott Noren (D)",
                value: "House::NY::23::Scott Noren (D)",
                children: [],
              },
              {
                name: "Tom Reed (R)",
                value: "House::NY::23::Tom Reed (R)",
                children: [],
              },
            ],
          },
          {
            name: "24",
            value: "House::NY::24",
            children: [
              {
                name: "Dana Balter (D)",
                value: "House::NY::24::Dana Balter (D)",
                children: [],
              },
              {
                name: "Francis Conole (D)",
                value: "House::NY::24::Francis Conole (D)",
                children: [],
              },
              {
                name: "John Katko (R)",
                value: "House::NY::24::John Katko (R)",
                children: [],
              },
            ],
          },
          {
            name: "25",
            value: "House::NY::25",
            children: [
              {
                name: "Joseph Morelle (D)",
                value: "House::NY::25::Joseph Morelle (D)",
                children: [],
              },
              {
                name: "Robin Wilt (D)",
                value: "House::NY::25::Robin Wilt (D)",
                children: [],
              },
              {
                name: "George Mitris (R)",
                value: "House::NY::25::George Mitris (R)",
                children: [],
              },
            ],
          },
          {
            name: "26",
            value: "House::NY::26",
            children: [
              {
                name: "Brian Higgins (D)",
                value: "House::NY::26::Brian Higgins (D)",
                children: [],
              },
              {
                name: "Emin Eddie Egriu (D)",
                value: "House::NY::26::Emin Eddie Egriu (D)",
                children: [],
              },
              {
                name: "Ricky Donovan, Sr. (R)",
                value: "House::NY::26::Ricky Donovan, Sr. (R)",
                children: [],
              },
              {
                name: "Jim Samsel (R)",
                value: "House::NY::26::Jim Samsel (R)",
                children: [],
              },
            ],
          },
          {
            name: "27",
            value: "House::NY::27",
            children: [
              {
                name: "Nate McMurray (D)",
                value: "House::NY::27::Nate McMurray (D)",
                children: [],
              },
              {
                name: "Christopher Jacobs (R)",
                value: "House::NY::27::Christopher Jacobs (R)",
                children: [],
              },
              {
                name: "Stefan Mychajliw Jr. (R)",
                value: "House::NY::27::Stefan Mychajliw Jr. (R)",
                children: [],
              },
              {
                name: "Beth Parlato (R)",
                value: "House::NY::27::Beth Parlato (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "NC",
        value: "House::NC",
        children: [
          {
            name: "1",
            value: "House::NC::1",
            children: [
              {
                name: "G.K. Butterfield (D)",
                value: "House::NC::1::G.K. Butterfield (D)",
                children: [],
              },
              {
                name: "Sandy Smith (R)",
                value: "House::NC::1::Sandy Smith (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::NC::2",
            children: [
              {
                name: "Deborah Ross (D)",
                value: "House::NC::2::Deborah Ross (D)",
                children: [],
              },
              {
                name: "Alan Swain (R)",
                value: "House::NC::2::Alan Swain (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::NC::3",
            children: [
              {
                name: "Gregory Murphy (R)",
                value: "House::NC::3::Gregory Murphy (R)",
                children: [],
              },
              {
                name: "Daryl Farrow (D)",
                value: "House::NC::3::Daryl Farrow (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::NC::4",
            children: [
              {
                name: "David Price (D)",
                value: "House::NC::4::David Price (D)",
                children: [],
              },
              {
                name: "Robert Thomas (R)",
                value: "House::NC::4::Robert Thomas (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::NC::5",
            children: [
              {
                name: "Virginia Foxx (R)",
                value: "House::NC::5::Virginia Foxx (R)",
                children: [],
              },
              {
                name: "David Wilson Brown (D)",
                value: "House::NC::5::David Wilson Brown (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::NC::6",
            children: [
              {
                name: "Kathy Manning (D)",
                value: "House::NC::6::Kathy Manning (D)",
                children: [],
              },
              {
                name: "Joseph Lee Haywood (R)",
                value: "House::NC::6::Joseph Lee Haywood (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::NC::7",
            children: [
              {
                name: "David Rouzer (R)",
                value: "House::NC::7::David Rouzer (R)",
                children: [],
              },
              {
                name: "Christopher Ward (D)",
                value: "House::NC::7::Christopher Ward (D)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::NC::8",
            children: [
              {
                name: "Richard Hudson (R)",
                value: "House::NC::8::Richard Hudson (R)",
                children: [],
              },
              {
                name: "Patricia Timmons-Goodson (D)",
                value: "House::NC::8::Patricia Timmons-Goodson (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::NC::9",
            children: [
              {
                name: "Dan Bishop (R)",
                value: "House::NC::9::Dan Bishop (R)",
                children: [],
              },
              {
                name: "Cynthia Wallace (D)",
                value: "House::NC::9::Cynthia Wallace (D)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::NC::10",
            children: [
              {
                name: "Patrick T. McHenry (R)",
                value: "House::NC::10::Patrick T. McHenry (R)",
                children: [],
              },
              {
                name: "David Parker (D)",
                value: "House::NC::10::David Parker (D)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::NC::11",
            children: [
              {
                name: "Morris Davis (D)",
                value: "House::NC::11::Morris Davis (D)",
                children: [],
              },
              {
                name: "Lynda Bennett (R)",
                value: "House::NC::11::Lynda Bennett (R)",
                children: [],
              },
              {
                name: "Madison Cawthorn (R)",
                value: "House::NC::11::Madison Cawthorn (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::NC::12",
            children: [
              {
                name: "Alma Adams (D)",
                value: "House::NC::12::Alma Adams (D)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::NC::13",
            children: [
              {
                name: "Scott Huffman (D)",
                value: "House::NC::13::Scott Huffman (D)",
                children: [],
              },
              {
                name: "Tedd Budd (R)",
                value: "House::NC::13::Tedd Budd (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "ND",
        value: "House::ND",
        children: [
          {
            name: 1,
            value: "House::ND::1",
            children: [
              {
                name: "Zachary Raknerud (D)",
                value: "House::ND::1::Zachary Raknerud (D)",
                children: [],
              },
              {
                name: "Roland Riemers (D)",
                value: "House::ND::1::Roland Riemers (D)",
                children: [],
              },
              {
                name: "Kelly Armstrong (R)",
                value: "House::ND::1::Kelly Armstrong (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "OH",
        value: "House::OH",
        children: [
          {
            name: "1",
            value: "House::OH::1",
            children: [
              {
                name: "Steve Chabot (R)",
                value: "House::OH::1::Steve Chabot (R)",
                children: [],
              },
              {
                name: "Kate Schroder (D)",
                value: "House::OH::1::Kate Schroder (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::OH::2",
            children: [
              {
                name: "Brad Wenstrup (R)",
                value: "House::OH::2::Brad Wenstrup (R)",
                children: [],
              },
              {
                name: "Jaime Castle (D)",
                value: "House::OH::2::Jaime Castle (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::OH::3",
            children: [
              {
                name: "Joyce Beatty (D)",
                value: "House::OH::3::Joyce Beatty (D)",
                children: [],
              },
              {
                name: "Mark Richardson (R)",
                value: "House::OH::3::Mark Richardson (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::OH::4",
            children: [
              {
                name: "Jim Jordan (R)",
                value: "House::OH::4::Jim Jordan (R)",
                children: [],
              },
              {
                name: "Shannon Freshour (D)",
                value: "House::OH::4::Shannon Freshour (D)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::OH::5",
            children: [
              {
                name: "Bob Latta (R)",
                value: "House::OH::5::Bob Latta (R)",
                children: [],
              },
              {
                name: "Nick Rubando (D)",
                value: "House::OH::5::Nick Rubando (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::OH::6",
            children: [
              {
                name: "Bill Johnson (R)",
                value: "House::OH::6::Bill Johnson (R)",
                children: [],
              },
              {
                name: "Shawna Roberts (D)",
                value: "House::OH::6::Shawna Roberts (D)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::OH::7",
            children: [
              {
                name: "Bob Gibbs (R)",
                value: "House::OH::7::Bob Gibbs (R)",
                children: [],
              },
              {
                name: "Quentin Potter (D)",
                value: "House::OH::7::Quentin Potter (D)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::OH::8",
            children: [
              {
                name: "Warren Davidson (R)",
                value: "House::OH::8::Warren Davidson (R)",
                children: [],
              },
              {
                name: "Vanessa Enoch (D)",
                value: "House::OH::8::Vanessa Enoch (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::OH::9",
            children: [
              {
                name: "Marcy Kaptur (D)",
                value: "House::OH::9::Marcy Kaptur (D)",
                children: [],
              },
              {
                name: "Rob Weber (R)",
                value: "House::OH::9::Rob Weber (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::OH::10",
            children: [
              {
                name: "Michael Turner (R)",
                value: "House::OH::10::Michael Turner (R)",
                children: [],
              },
              {
                name: "Desiree Tims (D)",
                value: "House::OH::10::Desiree Tims (D)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::OH::11",
            children: [
              {
                name: "Marcia Fudge (D)",
                value: "House::OH::11::Marcia Fudge (D)",
                children: [],
              },
              {
                name: "Laverne Gore (R)",
                value: "House::OH::11::Laverne Gore (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::OH::12",
            children: [
              {
                name: "Troy Balderson (R)",
                value: "House::OH::12::Troy Balderson (R)",
                children: [],
              },
              {
                name: "Alaina Shearer (D)",
                value: "House::OH::12::Alaina Shearer (D)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::OH::13",
            children: [
              {
                name: "Tim Ryan (D)",
                value: "House::OH::13::Tim Ryan (D)",
                children: [],
              },
              {
                name: "Christina Hagan (R)",
                value: "House::OH::13::Christina Hagan (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::OH::14",
            children: [
              {
                name: "David Joyce (R)",
                value: "House::OH::14::David Joyce (R)",
                children: [],
              },
              {
                name: "Hillary O'Connor Mueri (D)",
                value: "House::OH::14::Hillary O'Connor Mueri (D)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::OH::15",
            children: [
              {
                name: "Steve Stivers (R)",
                value: "House::OH::15::Steve Stivers (R)",
                children: [],
              },
              {
                name: "Joel Newby (D)",
                value: "House::OH::15::Joel Newby (D)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::OH::16",
            children: [
              {
                name: "Anthony Gonzalez (R)",
                value: "House::OH::16::Anthony Gonzalez (R)",
                children: [],
              },
              {
                name: "Aaron Godfrey (D)",
                value: "House::OH::16::Aaron Godfrey (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "OK",
        value: "House::OK",
        children: [
          {
            name: "1",
            value: "House::OK::1",
            children: [
              {
                name: "Kojo Asamoa-Caesar (D)",
                value: "House::OK::1::Kojo Asamoa-Caesar (D)",
                children: [],
              },
              {
                name: "Mark Keeter (D)",
                value: "House::OK::1::Mark Keeter (D)",
                children: [],
              },
              {
                name: "Kevin Hern (R)",
                value: "House::OK::1::Kevin Hern (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::OK::2",
            children: [
              {
                name: "Markwayne Mullin (Incumbent) (R)",
                value: "House::OK::2::Markwayne Mullin (Incumbent) (R)",
                children: [],
              },
              {
                name: "Rhonda Hopkins (R)",
                value: "House::OK::2::Rhonda Hopkins (R)",
                children: [],
              },
              {
                name: "Joseph Silk (R)",
                value: "House::OK::2::Joseph Silk (R)",
                children: [],
              },
              {
                name: "Danyell Lanier (D)",
                value: "House::OK::2::Danyell Lanier (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::OK::3",
            children: [
              {
                name: "Frank Lucas (R)",
                value: "House::OK::3::Frank Lucas (R)",
                children: [],
              },
              {
                name: "Zoe Ann Midyett (D)",
                value: "House::OK::3::Zoe Ann Midyett (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::OK::4",
            children: [
              {
                name: "John Argo (D)",
                value: "House::OK::4::John Argo (D)",
                children: [],
              },
              {
                name: "Mary Brannon (D)",
                value: "House::OK::4::Mary Brannon (D)",
                children: [],
              },
              {
                name: "David Slemmons (D)",
                value: "House::OK::4::David Slemmons (D)",
                children: [],
              },
              {
                name: "Tom Cole (R)",
                value: "House::OK::4::Tom Cole (R)",
                children: [],
              },
              {
                name: "Gilbert Sanders (R)",
                value: "House::OK::4::Gilbert Sanders (R)",
                children: [],
              },
              {
                name: "Trevor Sipes (R)",
                value: "House::OK::4::Trevor Sipes (R)",
                children: [],
              },
              {
                name: "James Taylor (R)",
                value: "House::OK::4::James Taylor (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::OK::5",
            children: [
              {
                name: "Kendra Horn (D)",
                value: "House::OK::5::Kendra Horn (D)",
                children: [],
              },
              {
                name: "Tom Guild (D)",
                value: "House::OK::5::Tom Guild (D)",
                children: [],
              },
              {
                name: "Michael Ballard (R)",
                value: "House::OK::5::Michael Ballard (R)",
                children: [],
              },
              {
                name: "Janet Barresi (R)",
                value: "House::OK::5::Janet Barresi (R)",
                children: [],
              },
              {
                name: "Stephanie Bice (R)",
                value: "House::OK::5::Stephanie Bice (R)",
                children: [],
              },
              {
                name: "David Hill (R)",
                value: "House::OK::5::David Hill (R)",
                children: [],
              },
              {
                name: "Shelli Landon (R)",
                value: "House::OK::5::Shelli Landon (R)",
                children: [],
              },
              {
                name: "Jake Merrick (R)",
                value: "House::OK::5::Jake Merrick (R)",
                children: [],
              },
              {
                name: "Terry Neese (R)",
                value: "House::OK::5::Terry Neese (R)",
                children: [],
              },
              {
                name: "Charles Tuffy Pringle (R)",
                value: "House::OK::5::Charles Tuffy Pringle (R)",
                children: [],
              },
              {
                name: "Miles Rahimi (R)",
                value: "House::OK::5::Miles Rahimi (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "OR",
        value: "House::OR",
        children: [
          {
            name: "1",
            value: "House::OR::1",
            children: [
              {
                name: "Suzanne Bonamici (Incumbent) (D)",
                value: "House::OR::1::Suzanne Bonamici (Incumbent) (D)",
                children: [],
              },
              {
                name: "Ricky Barajas (D)",
                value: "House::OR::1::Ricky Barajas (D)",
                children: [],
              },
              {
                name: "Heidi Briones (D)",
                value: "House::OR::1::Heidi Briones (D)",
                children: [],
              },
              {
                name: "Amanda Siebe (D)",
                value: "House::OR::1::Amanda Siebe (D)",
                children: [],
              },
              {
                name: "Christopher Christensen (R)",
                value: "House::OR::1::Christopher Christensen (R)",
                children: [],
              },
              {
                name: "Army Murray (R)",
                value: "House::OR::1::Army Murray (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::OR::2",
            children: [
              {
                name: "Nick Heuertz (D)",
                value: "House::OR::2::Nick Heuertz (D)",
                children: [],
              },
              {
                name: "John Holm (D)",
                value: "House::OR::2::John Holm (D)",
                children: [],
              },
              {
                name: "Jack Howard (D)",
                value: "House::OR::2::Jack Howard (D)",
                children: [],
              },
              {
                name: "Alex Spenser (D)",
                value: "House::OR::2::Alex Spenser (D)",
                children: [],
              },
              {
                name: "Chris Vaughn (D)",
                value: "House::OR::2::Chris Vaughn (D)",
                children: [],
              },
              {
                name: "Jason Atkinson (R)",
                value: "House::OR::2::Jason Atkinson (R)",
                children: [],
              },
              {
                name: "Cliff Bentz (R)",
                value: "House::OR::2::Cliff Bentz (R)",
                children: [],
              },
              {
                name: "Knute Buehler (R)",
                value: "House::OR::2::Knute Buehler (R)",
                children: [],
              },
              {
                name: "David Campbell (R)",
                value: "House::OR::2::David Campbell (R)",
                children: [],
              },
              {
                name: "Glenn Carey (R)",
                value: "House::OR::2::Glenn Carey (R)",
                children: [],
              },
              {
                name: "Jimmy Crumpacker (R)",
                value: "House::OR::2::Jimmy Crumpacker (R)",
                children: [],
              },
              {
                name: "Travis Fager (R)",
                value: "House::OR::2::Travis Fager (R)",
                children: [],
              },
              {
                name: "Justin Livingston (R)",
                value: "House::OR::2::Justin Livingston (R)",
                children: [],
              },
              {
                name: "Kenneth Medenbach (R)",
                value: "House::OR::2::Kenneth Medenbach (R)",
                children: [],
              },
              {
                name: "Mark Roberts (R)",
                value: "House::OR::2::Mark Roberts (R)",
                children: [],
              },
              {
                name: "Jeff Smith (R)",
                value: "House::OR::2::Jeff Smith (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::OR::3",
            children: [
              {
                name: "Earl Blumenauer (D)",
                value: "House::OR::3::Earl Blumenauer (D)",
                children: [],
              },
              {
                name: "Matthew Davis (D)",
                value: "House::OR::3::Matthew Davis (D)",
                children: [],
              },
              {
                name: "Albert Lee (D)",
                value: "House::OR::3::Albert Lee (D)",
                children: [],
              },
              {
                name: "Charles Rand Barnett (D)",
                value: "House::OR::3::Charles Rand Barnett (D)",
                children: [],
              },
              {
                name: "Dane Wilcox (D)",
                value: "House::OR::3::Dane Wilcox (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::OR::4",
            children: [
              {
                name: "Joanna Harbour (R)",
                value: "House::OR::4::Joanna Harbour (R)",
                children: [],
              },
              {
                name: "Tom Harrison (R)",
                value: "House::OR::4::Tom Harrison (R)",
                children: [],
              },
              {
                name: "Frank Hecker (R)",
                value: "House::OR::4::Frank Hecker (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::OR::5",
            children: [
              {
                name: "Kurt Schrader (D)",
                value: "House::OR::5::Kurt Schrader (D)",
                children: [],
              },
              {
                name: "Mark Gamba (D)",
                value: "House::OR::5::Mark Gamba (D)",
                children: [],
              },
              {
                name: "Blair Reynolds (D)",
                value: "House::OR::5::Blair Reynolds (D)",
                children: [],
              },
              {
                name: "G. Shane Dinkel (R)",
                value: "House::OR::5::G. Shane Dinkel (R)",
                children: [],
              },
              {
                name: "Joey Nations (R)",
                value: "House::OR::5::Joey Nations (R)",
                children: [],
              },
              {
                name: "Angela Roman (R)",
                value: "House::OR::5::Angela Roman (R)",
                children: [],
              },
              {
                name: "Amy Ryan Courser (R)",
                value: "House::OR::5::Amy Ryan Courser (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "PA",
        value: "House::PA",
        children: [
          {
            name: "1",
            value: "House::PA::1",
            children: [
              {
                name: "Christina Finello (D)",
                value: "House::PA::1::Christina Finello (D)",
                children: [],
              },
              {
                name: "Skylar Hurwitz (D)",
                value: "House::PA::1::Skylar Hurwitz (D)",
                children: [],
              },
              {
                name: "Brian Fitzpatrick (R)",
                value: "House::PA::1::Brian Fitzpatrick (R)",
                children: [],
              },
              {
                name: "Andrew Meehan (R)",
                value: "House::PA::1::Andrew Meehan (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::PA::2",
            children: [
              {
                name: "Brendan Boyle (D)",
                value: "House::PA::2::Brendan Boyle (D)",
                children: [],
              },
              {
                name: "David Torres (R)",
                value: "House::PA::2::David Torres (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::PA::3",
            children: [
              {
                name: "Dwight Evans (D)",
                value: "House::PA::3::Dwight Evans (D)",
                children: [],
              },
              {
                name: "Michael Harvey (R)",
                value: "House::PA::3::Michael Harvey (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::PA::4",
            children: [
              {
                name: "Madelenine Dean (D)",
                value: "House::PA::4::Madelenine Dean (D)",
                children: [],
              },
              {
                name: "Kathy Barnette (R)",
                value: "House::PA::4::Kathy Barnette (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::PA::5",
            children: [
              {
                name: "Mary Gay Scanlon (D)",
                value: "House::PA::5::Mary Gay Scanlon (D)",
                children: [],
              },
              {
                name: "Louis Lanni (D)",
                value: "House::PA::5::Louis Lanni (D)",
                children: [],
              },
              {
                name: "Robert Jordan (R)",
                value: "House::PA::5::Robert Jordan (R)",
                children: [],
              },
              {
                name: "Dasha Pruett (R)",
                value: "House::PA::5::Dasha Pruett (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::PA::6",
            children: [
              {
                name: "Chrissy Houlahan (D)",
                value: "House::PA::6::Chrissy Houlahan (D)",
                children: [],
              },
              {
                name: "John Emmons (R)",
                value: "House::PA::6::John Emmons (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::PA::7",
            children: [
              {
                name: "Susan Wild (D)",
                value: "House::PA::7::Susan Wild (D)",
                children: [],
              },
              {
                name: "Dean Browning (R)",
                value: "House::PA::7::Dean Browning (R)",
                children: [],
              },
              {
                name: "Lisa Scheller (R)",
                value: "House::PA::7::Lisa Scheller (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::PA::8",
            children: [
              {
                name: "Matt Cartwright (D)",
                value: "House::PA::8::Matt Cartwright (D)",
                children: [],
              },
              {
                name: "Jim Bognet (R)",
                value: "House::PA::8::Jim Bognet (R)",
                children: [],
              },
              {
                name: "Mikel Cammisa (R)",
                value: "House::PA::8::Mikel Cammisa (R)",
                children: [],
              },
              {
                name: "Teddy Daniels (R)",
                value: "House::PA::8::Teddy Daniels (R)",
                children: [],
              },
              {
                name: "Earl Granville (R)",
                value: "House::PA::8::Earl Granville (R)",
                children: [],
              },
              {
                name: "Harry Haas (R)",
                value: "House::PA::8::Harry Haas (R)",
                children: [],
              },
              {
                name: "Mike Marsicano (R)",
                value: "House::PA::8::Mike Marsicano (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::PA::9",
            children: [
              {
                name: "Laura Quick (D)",
                value: "House::PA::9::Laura Quick (D)",
                children: [],
              },
              {
                name: "Gary Wegman (D)",
                value: "House::PA::9::Gary Wegman (D)",
                children: [],
              },
              {
                name: "Dan Meuser (R)",
                value: "House::PA::9::Dan Meuser (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::PA::10",
            children: [
              {
                name: "Tom Brier (D)",
                value: "House::PA::10::Tom Brier (D)",
                children: [],
              },
              {
                name: "Eugene DePasquale (D)",
                value: "House::PA::10::Eugene DePasquale (D)",
                children: [],
              },
              {
                name: "Scott Perry (R)",
                value: "House::PA::10::Scott Perry (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::PA::11",
            children: [
              {
                name: "Paul Daigle (D)",
                value: "House::PA::11::Paul Daigle (D)",
                children: [],
              },
              {
                name: "Sarah Hammond (D)",
                value: "House::PA::11::Sarah Hammond (D)",
                children: [],
              },
              {
                name: "Lloyd Smucker (R)",
                value: "House::PA::11::Lloyd Smucker (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::PA::12",
            children: [
              {
                name: "Lee Griffin (D)",
                value: "House::PA::12::Lee Griffin (D)",
                children: [],
              },
              {
                name: "Fred Keller (R)",
                value: "House::PA::12::Fred Keller (R)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::PA::13",
            children: [
              {
                name: "Todd Rowley (D)",
                value: "House::PA::13::Todd Rowley (D)",
                children: [],
              },
              {
                name: "John Joyce (R)",
                value: "House::PA::13::John Joyce (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::PA::14",
            children: [
              {
                name: "William Marx (D)",
                value: "House::PA::14::William Marx (D)",
                children: [],
              },
              {
                name: "Guy Reschenthaler (R)",
                value: "House::PA::14::Guy Reschenthaler (R)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::PA::15",
            children: [
              {
                name: "Robert Willaims (D)",
                value: "House::PA::15::Robert Willaims (D)",
                children: [],
              },
              {
                name: "Glenn Thompson (R)",
                value: "House::PA::15::Glenn Thompson (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::PA::16",
            children: [
              {
                name: "Kristy Gnibus (D)",
                value: "House::PA::16::Kristy Gnibus (D)",
                children: [],
              },
              {
                name: "Mike Kelly (R)",
                value: "House::PA::16::Mike Kelly (R)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::PA::17",
            children: [
              {
                name: "Conor Lamb (D)",
                value: "House::PA::17::Conor Lamb (D)",
                children: [],
              },
              {
                name: "Sean Parnell (R)",
                value: "House::PA::17::Sean Parnell (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::PA::18",
            children: [
              {
                name: "Michael Doyle (D)",
                value: "House::PA::18::Michael Doyle (D)",
                children: [],
              },
              {
                name: "Jerry Dickinson (D)",
                value: "House::PA::18::Jerry Dickinson (D)",
                children: [],
              },
              {
                name: "Luke Negron (R)",
                value: "House::PA::18::Luke Negron (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "RI",
        value: "House::RI",
        children: [
          {
            name: "1",
            value: "House::RI::1",
            children: [
              {
                name: "David Cicilline (D)",
                value: "House::RI::1::David Cicilline (D)",
                children: [],
              },
              {
                name: "Frederick Wysocki (R)",
                value: "House::RI::1::Frederick Wysocki (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::RI::2",
            children: [
              {
                name: "Jim Langevin (D)",
                value: "House::RI::2::Jim Langevin (D)",
                children: [],
              },
              {
                name: "Robert Lancia (R)",
                value: "House::RI::2::Robert Lancia (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "SC",
        value: "House::SC",
        children: [
          {
            name: "1 ",
            value: "House::SC::1 ",
            children: [
              {
                name: "Joe Cunningham (D)",
                value: "House::SC::1 ::Joe Cunningham (D)",
                children: [],
              },
              {
                name: "Chris Cox (R)",
                value: "House::SC::1 ::Chris Cox (R)",
                children: [],
              },
              {
                name: "Kathy Landing (R)",
                value: "House::SC::1 ::Kathy Landing (R)",
                children: [],
              },
              {
                name: "Nancy Mace (R)",
                value: "House::SC::1 ::Nancy Mace (R)",
                children: [],
              },
              {
                name: "Brad Mole (R)",
                value: "House::SC::1 ::Brad Mole (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::SC::2",
            children: [
              {
                name: "Adair Ford Boroughs (D)",
                value: "House::SC::2::Adair Ford Boroughs (D)",
                children: [],
              },
              {
                name: "Joe Wilson (R)",
                value: "House::SC::2::Joe Wilson (R)",
                children: [],
              },
              {
                name: "Michael Bishop (R)",
                value: "House::SC::2::Michael Bishop (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::SC::3",
            children: [
              {
                name: "Jeff Duncan (R)",
                value: "House::SC::3::Jeff Duncan (R)",
                children: [],
              },
              {
                name: "Hosea Cleveland (D)",
                value: "House::SC::3::Hosea Cleveland (D)",
                children: [],
              },
              {
                name: "Mark Welch (D)",
                value: "House::SC::3::Mark Welch (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::SC::4",
            children: [
              {
                name: "Kim Nelson (D)",
                value: "House::SC::4::Kim Nelson (D)",
                children: [],
              },
              {
                name: "William Timmons (R)",
                value: "House::SC::4::William Timmons (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::SC::5",
            children: [
              {
                name: "Ralph Norman (R)",
                value: "House::SC::5::Ralph Norman (R)",
                children: [],
              },
              {
                name: "Moe Brown (D)",
                value: "House::SC::5::Moe Brown (D)",
                children: [],
              },
              {
                name: "Sidney Moore (D)",
                value: "House::SC::5::Sidney Moore (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::SC::6",
            children: [
              {
                name: "James Clyburn (D)",
                value: "House::SC::6::James Clyburn (D)",
                children: [],
              },
              {
                name: "John McCollum (R)",
                value: "House::SC::6::John McCollum (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::SC::7",
            children: [
              {
                name: "Melissa Watson (D)",
                value: "House::SC::7::Melissa Watson (D)",
                children: [],
              },
              {
                name: "Robert Williams (D)",
                value: "House::SC::7::Robert Williams (D)",
                children: [],
              },
              {
                name: "William Williams (D)",
                value: "House::SC::7::William Williams (D)",
                children: [],
              },
              {
                name: "Tom Rice (R)",
                value: "House::SC::7::Tom Rice (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "SD",
        value: "House::SD",
        children: [
          {
            name: 1,
            value: "House::SD::1",
            children: [
              {
                name: "Dusty Jonhson (R)",
                value: "House::SD::1::Dusty Jonhson (R)",
                children: [],
              },
              {
                name: "Elizabeth May (R)",
                value: "House::SD::1::Elizabeth May (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "TN",
        value: "House::TN",
        children: [
          {
            name: "1",
            value: "House::TN::1",
            children: [
              {
                name: "Chris Rowe (D)",
                value: "House::TN::1::Chris Rowe (D)",
                children: [],
              },
              {
                name: "Larry Smith (D)",
                value: "House::TN::1::Larry Smith (D)",
                children: [],
              },
              {
                name: "Blair Walsingham (D)",
                value: "House::TN::1::Blair Walsingham (D)",
                children: [],
              },
              {
                name: "Jay Adkins (R)",
                value: "House::TN::1::Jay Adkins (R)",
                children: [],
              },
              {
                name: "Phil Arlinghaus (R)",
                value: "House::TN::1::Phil Arlinghaus (R)",
                children: [],
              },
              {
                name: "Richard Baker (R)",
                value: "House::TN::1::Richard Baker (R)",
                children: [],
              },
              {
                name: "Chance Cansler (R)",
                value: "House::TN::1::Chance Cansler (R)",
                children: [],
              },
              {
                name: "John Clark (R)",
                value: "House::TN::1::John Clark (R)",
                children: [],
              },
              {
                name: "Rusty Crowe (R)",
                value: "House::TN::1::Rusty Crowe (R)",
                children: [],
              },
              {
                name: "Steve Darden (R)",
                value: "House::TN::1::Steve Darden (R)",
                children: [],
              },
              {
                name: "Chad Fleenor (R)",
                value: "House::TN::1::Chad Fleenor (R)",
                children: [],
              },
              {
                name: "Robert Franklin (R)",
                value: "House::TN::1::Robert Franklin (R)",
                children: [],
              },
              {
                name: "Josh Gapp (R)",
                value: "House::TN::1::Josh Gapp (R)",
                children: [],
              },
              {
                name: "Diana Harshbarger (R)",
                value: "House::TN::1::Diana Harshbarger (R)",
                children: [],
              },
              {
                name: "David Hawk (R)",
                value: "House::TN::1::David Hawk (R)",
                children: [],
              },
              {
                name: "Timothy Hill (R)",
                value: "House::TN::1::Timothy Hill (R)",
                children: [],
              },
              {
                name: "Chuck Miller (R)",
                value: "House::TN::1::Chuck Miller (R)",
                children: [],
              },
              {
                name: "Carter Quillen (R)",
                value: "House::TN::1::Carter Quillen (R)",
                children: [],
              },
              {
                name: "Nichole Williams (R)",
                value: "House::TN::1::Nichole Williams (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::TN::2",
            children: [
              {
                name: "Chance Brown (D)",
                value: "House::TN::2::Chance Brown (D)",
                children: [],
              },
              {
                name: "Renee Hoyos (D)",
                value: "House::TN::2::Renee Hoyos (D)",
                children: [],
              },
              {
                name: "Tim Burchett (R)",
                value: "House::TN::2::Tim Burchett (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::TN::3",
            children: [
              {
                name: "Meg Gorman (D)",
                value: "House::TN::3::Meg Gorman (D)",
                children: [],
              },
              {
                name: "Charles J. Fleischmann (R)",
                value: "House::TN::3::Charles J. Fleischmann (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::TN::4",
            children: [
              {
                name: "Noelle Bivens (D)",
                value: "House::TN::4::Noelle Bivens (D)",
                children: [],
              },
              {
                name: "Chirstopher Hale (D)",
                value: "House::TN::4::Chirstopher Hale (D)",
                children: [],
              },
              {
                name: "Scott DesJarlais (R)",
                value: "House::TN::4::Scott DesJarlais (R)",
                children: [],
              },
              {
                name: "Doug Meyer (R)",
                value: "House::TN::4::Doug Meyer (R)",
                children: [],
              },
              {
                name: "Randy Sharp (R)",
                value: "House::TN::4::Randy Sharp (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::TN::5",
            children: [
              {
                name: "Jim Cooper (Incumbent) (D)",
                value: "House::TN::5::Jim Cooper (Incumbent) (D)",
                children: [],
              },
              {
                name: "Keeda Haynes (D)",
                value: "House::TN::5::Keeda Haynes (D)",
                children: [],
              },
              {
                name: "Joshua Rawlings (D)",
                value: "House::TN::5::Joshua Rawlings (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::TN::6",
            children: [
              {
                name: "Christopher Finley (D)",
                value: "House::TN::6::Christopher Finley (D)",
                children: [],
              },
              {
                name: "John Rose (D)",
                value: "House::TN::6::John Rose (D)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::TN::7",
            children: [
              {
                name: "Kiran Sreepada (D)",
                value: "House::TN::7::Kiran Sreepada (D)",
                children: [],
              },
              {
                name: "Mark Green (R)",
                value: "House::TN::7::Mark Green (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::TN::8",
            children: [
              {
                name: "Erika Stotts Pearson (D)",
                value: "House::TN::8::Erika Stotts Pearson (D)",
                children: [],
              },
              {
                name: "Lawrence Pivnick (D)",
                value: "House::TN::8::Lawrence Pivnick (D)",
                children: [],
              },
              {
                name: "Hollis Skinner (D)",
                value: "House::TN::8::Hollis Skinner (D)",
                children: [],
              },
              {
                name: "Savannah Williamson (D)",
                value: "House::TN::8::Savannah Williamson (D)",
                children: [],
              },
              {
                name: "David Kustoff (R)",
                value: "House::TN::8::David Kustoff (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::TN::9",
            children: [
              {
                name: "Steve Cohen (Incumbent) (D)",
                value: "House::TN::9::Steve Cohen (Incumbent) (D)",
                children: [],
              },
              {
                name: "Leo AwGoWhat (D)",
                value: "House::TN::9::Leo AwGoWhat (D)",
                children: [],
              },
              {
                name: "Corey Strong (D)",
                value: "House::TN::9::Corey Strong (D)",
                children: [],
              },
              {
                name: "Charlotte Bergmann (R)",
                value: "House::TN::9::Charlotte Bergmann (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "TX",
        value: "House::TX",
        children: [
          {
            name: "1",
            value: "House::TX::1",
            children: [
              {
                name: "Louis B. Gohmert Jr. (R)",
                value: "House::TX::1::Louis B. Gohmert Jr. (R)",
                children: [],
              },
              {
                name: "Hank Gilbert (D)",
                value: "House::TX::1::Hank Gilbert (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::TX::2",
            children: [
              {
                name: "Daniel Crenshaw (R)",
                value: "House::TX::2::Daniel Crenshaw (R)",
                children: [],
              },
              {
                name: "Sima Ladjevardian (D)",
                value: "House::TX::2::Sima Ladjevardian (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::TX::3",
            children: [
              {
                name: "Van Taylor (R)",
                value: "House::TX::3::Van Taylor (R)",
                children: [],
              },
              {
                name: "Sean McCaffity (D)",
                value: "House::TX::3::Sean McCaffity (D)",
                children: [],
              },
              {
                name: "Lulu Seikaly (D)",
                value: "House::TX::3::Lulu Seikaly (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::TX::4",
            children: [
              {
                name: "John Ratcliffe (R)",
                value: "House::TX::4::John Ratcliffe (R)",
                children: [],
              },
              {
                name: "Russell Foster (D)",
                value: "House::TX::4::Russell Foster (D)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::TX::5",
            children: [
              {
                name: "Lance Gooden (R)",
                value: "House::TX::5::Lance Gooden (R)",
                children: [],
              },
              {
                name: "Carolyn Salter (D)",
                value: "House::TX::5::Carolyn Salter (D)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::TX::6",
            children: [
              {
                name: "Ronald Wright (R)",
                value: "House::TX::6::Ronald Wright (R)",
                children: [],
              },
              {
                name: "Stephen Daniel (D)",
                value: "House::TX::6::Stephen Daniel (D)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::TX::7",
            children: [
              {
                name: "Lizzie Pannill Fletcher (D)",
                value: "House::TX::7::Lizzie Pannill Fletcher (D)",
                children: [],
              },
              {
                name: "Wesley Hunt (R)",
                value: "House::TX::7::Wesley Hunt (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::TX::8",
            children: [
              {
                name: "Kevin Brady (R)",
                value: "House::TX::8::Kevin Brady (R)",
                children: [],
              },
              {
                name: "Elizabeth Hernandez (D)",
                value: "House::TX::8::Elizabeth Hernandez (D)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::TX::9",
            children: [
              {
                name: "Al Green (D)",
                value: "House::TX::9::Al Green (D)",
                children: [],
              },
              {
                name: "Johnny Teague (R)",
                value: "House::TX::9::Johnny Teague (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::TX::10",
            children: [
              {
                name: "Michael McCaul (R)",
                value: "House::TX::10::Michael McCaul (R)",
                children: [],
              },
              {
                name: "Pritesh Gandhi (D)",
                value: "House::TX::10::Pritesh Gandhi (D)",
                children: [],
              },
              {
                name: "Mike Siegel (D)",
                value: "House::TX::10::Mike Siegel (D)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::TX::11",
            children: [
              {
                name: "Jon Mark Hogg (D)",
                value: "House::TX::11::Jon Mark Hogg (D)",
                children: [],
              },
              {
                name: "August Pfluger (R)",
                value: "House::TX::11::August Pfluger (R)",
                children: [],
              },
            ],
          },
          {
            name: "12",
            value: "House::TX::12",
            children: [
              {
                name: "Kay Granger (R)",
                value: "House::TX::12::Kay Granger (R)",
                children: [],
              },
              {
                name: "Lisa Welch (D)",
                value: "House::TX::12::Lisa Welch (D)",
                children: [],
              },
            ],
          },
          {
            name: "13",
            value: "House::TX::13",
            children: [
              {
                name: "Greg Sagan (D)",
                value: "House::TX::13::Greg Sagan (D)",
                children: [],
              },
              {
                name: "Gus Trujillo (D)",
                value: "House::TX::13::Gus Trujillo (D)",
                children: [],
              },
              {
                name: "Ronny L. Jackson (R)",
                value: "House::TX::13::Ronny L. Jackson (R)",
                children: [],
              },
              {
                name: "Josh Winegarner (R)",
                value: "House::TX::13::Josh Winegarner (R)",
                children: [],
              },
            ],
          },
          {
            name: "14",
            value: "House::TX::14",
            children: [
              {
                name: "Randy Weber (R)",
                value: "House::TX::14::Randy Weber (R)",
                children: [],
              },
              {
                name: "Adrienne Bell (D)",
                value: "House::TX::14::Adrienne Bell (D)",
                children: [],
              },
            ],
          },
          {
            name: "15",
            value: "House::TX::15",
            children: [
              {
                name: "Vicente Gonzalez Jr. (D)",
                value: "House::TX::15::Vicente Gonzalez Jr. (D)",
                children: [],
              },
              {
                name: "Monica De La Cruz Hernandez (R)",
                value: "House::TX::15::Monica De La Cruz Hernandez (R)",
                children: [],
              },
              {
                name: "Ryan Krause (R)",
                value: "House::TX::15::Ryan Krause (R)",
                children: [],
              },
            ],
          },
          {
            name: "16",
            value: "House::TX::16",
            children: [
              {
                name: "Veronica Escobar (D)",
                value: "House::TX::16::Veronica Escobar (D)",
                children: [],
              },
              {
                name: "Irene Armendariz-Jackson (R)",
                value: "House::TX::16::Irene Armendariz-Jackson (R)",
                children: [],
              },
              {
                name: "Samuel Williams Jr. (R)",
                value: "House::TX::16::Samuel Williams Jr. (R)",
                children: [],
              },
            ],
          },
          {
            name: "17",
            value: "House::TX::17",
            children: [
              {
                name: "David Jaramillo (D)",
                value: "House::TX::17::David Jaramillo (D)",
                children: [],
              },
              {
                name: "Rick Kennedy (D)",
                value: "House::TX::17::Rick Kennedy (D)",
                children: [],
              },
              {
                name: "Pete Sessions (R)",
                value: "House::TX::17::Pete Sessions (R)",
                children: [],
              },
              {
                name: "Renee Swann (R)",
                value: "House::TX::17::Renee Swann (R)",
                children: [],
              },
            ],
          },
          {
            name: "18",
            value: "House::TX::18",
            children: [
              {
                name: "Robert Cadena (R)",
                value: "House::TX::18::Robert Cadena (R)",
                children: [],
              },
              {
                name: "Wendell Champion (R)",
                value: "House::TX::18::Wendell Champion (R)",
                children: [],
              },
              {
                name: "Shelia Jackson Lee (D)",
                value: "House::TX::18::Shelia Jackson Lee (D)",
                children: [],
              },
            ],
          },
          {
            name: "19",
            value: "House::TX::19",
            children: [
              {
                name: "Jodey Arrington (R)",
                value: "House::TX::19::Jodey Arrington (R)",
                children: [],
              },
              {
                name: "Tom Watson (D)",
                value: "House::TX::19::Tom Watson (D)",
                children: [],
              },
            ],
          },
          {
            name: "20",
            value: "House::TX::20",
            children: [
              {
                name: "Joaquin Castro (D)",
                value: "House::TX::20::Joaquin Castro (D)",
                children: [],
              },
              {
                name: "Gary Allen (R)",
                value: "House::TX::20::Gary Allen (R)",
                children: [],
              },
              {
                name: "Mauro Garza (R)",
                value: "House::TX::20::Mauro Garza (R)",
                children: [],
              },
            ],
          },
          {
            name: "21",
            value: "House::TX::21",
            children: [
              {
                name: "Chip Roy (R)",
                value: "House::TX::21::Chip Roy (R)",
                children: [],
              },
              {
                name: "Wendy Davis (D)",
                value: "House::TX::21::Wendy Davis (D)",
                children: [],
              },
            ],
          },
          {
            name: "22",
            value: "House::TX::22",
            children: [
              {
                name: "Sri Preston Kulkarni (D)",
                value: "House::TX::22::Sri Preston Kulkarni (D)",
                children: [],
              },
              {
                name: "Troy Nehls (R)",
                value: "House::TX::22::Troy Nehls (R)",
                children: [],
              },
              {
                name: "Kathaleen Wall (R)",
                value: "House::TX::22::Kathaleen Wall (R)",
                children: [],
              },
            ],
          },
          {
            name: "23",
            value: "House::TX::23",
            children: [
              {
                name: "Gina Ortiz Jones (D)",
                value: "House::TX::23::Gina Ortiz Jones (D)",
                children: [],
              },
              {
                name: "Tony Gonzales (R)",
                value: "House::TX::23::Tony Gonzales (R)",
                children: [],
              },
              {
                name: "Raul Reyes Jr. (R)",
                value: "House::TX::23::Raul Reyes Jr. (R)",
                children: [],
              },
            ],
          },
          {
            name: "24",
            value: "House::TX::24",
            children: [
              {
                name: "Beth Van Duyne (R)",
                value: "House::TX::24::Beth Van Duyne (R)",
                children: [],
              },
              {
                name: "Kim Olson (D)",
                value: "House::TX::24::Kim Olson (D)",
                children: [],
              },
              {
                name: "Candace Valenzuela (D)",
                value: "House::TX::24::Candace Valenzuela (D)",
                children: [],
              },
            ],
          },
          {
            name: "25",
            value: "House::TX::25",
            children: [
              {
                name: "Roger Williams (R)",
                value: "House::TX::25::Roger Williams (R)",
                children: [],
              },
              {
                name: "Julie Oliver (D)",
                value: "House::TX::25::Julie Oliver (D)",
                children: [],
              },
            ],
          },
          {
            name: "26",
            value: "House::TX::26",
            children: [
              {
                name: "Michael C. Burgess (R)",
                value: "House::TX::26::Michael C. Burgess (R)",
                children: [],
              },
              {
                name: "Carol Iannuzzi (D)",
                value: "House::TX::26::Carol Iannuzzi (D)",
                children: [],
              },
            ],
          },
          {
            name: "27",
            value: "House::TX::27",
            children: [
              {
                name: "Michael Cloud (R)",
                value: "House::TX::27::Michael Cloud (R)",
                children: [],
              },
              {
                name: "Ricardo De La Fuente (D)",
                value: "House::TX::27::Ricardo De La Fuente (D)",
                children: [],
              },
            ],
          },
          {
            name: "28",
            value: "House::TX::28",
            children: [
              {
                name: "Henry Cuellar (D)",
                value: "House::TX::28::Henry Cuellar (D)",
                children: [],
              },
              {
                name: "Sandra Whitten (R)",
                value: "House::TX::28::Sandra Whitten (R)",
                children: [],
              },
            ],
          },
          {
            name: "29",
            value: "House::TX::29",
            children: [
              {
                name: "Sylvia Garcia (D)",
                value: "House::TX::29::Sylvia Garcia (D)",
                children: [],
              },
              {
                name: "Jaimy Annette Zoboulikos-Blanco (R)",
                value: "House::TX::29::Jaimy Annette Zoboulikos-Blanco (R)",
                children: [],
              },
            ],
          },
          {
            name: "30",
            value: "House::TX::30",
            children: [
              {
                name: "Eddie Bernice Johnson (D)",
                value: "House::TX::30::Eddie Bernice Johnson (D)",
                children: [],
              },
              {
                name: "Tre Pennie (R)",
                value: "House::TX::30::Tre Pennie (R)",
                children: [],
              },
            ],
          },
          {
            name: "31",
            value: "House::TX::31",
            children: [
              {
                name: "John Carter (R)",
                value: "House::TX::31::John Carter (R)",
                children: [],
              },
              {
                name: "Donna Imam (D)",
                value: "House::TX::31::Donna Imam (D)",
                children: [],
              },
              {
                name: "Christine Mann (D)",
                value: "House::TX::31::Christine Mann (D)",
                children: [],
              },
            ],
          },
          {
            name: "32",
            value: "House::TX::32",
            children: [
              {
                name: "Colin Allred (D)",
                value: "House::TX::32::Colin Allred (D)",
                children: [],
              },
              {
                name: "Genevieve Collins (R)",
                value: "House::TX::32::Genevieve Collins (R)",
                children: [],
              },
            ],
          },
          {
            name: "33",
            value: "House::TX::33",
            children: [
              {
                name: "Marc Veasey (D)",
                value: "House::TX::33::Marc Veasey (D)",
                children: [],
              },
              {
                name: "Fabian Cordova Vasquez (R)",
                value: "House::TX::33::Fabian Cordova Vasquez (R)",
                children: [],
              },
            ],
          },
          {
            name: "34",
            value: "House::TX::34",
            children: [
              {
                name: "Filemon Vela (D)",
                value: "House::TX::34::Filemon Vela (D)",
                children: [],
              },
              {
                name: "Rey Gonzalez Jr. (R)",
                value: "House::TX::34::Rey Gonzalez Jr. (R)",
                children: [],
              },
            ],
          },
          {
            name: "35",
            value: "House::TX::35",
            children: [
              {
                name: "Lloyd Doggett (D)",
                value: "House::TX::35::Lloyd Doggett (D)",
                children: [],
              },
              {
                name: "Jenny Garcia Sharon (R)",
                value: "House::TX::35::Jenny Garcia Sharon (R)",
                children: [],
              },
              {
                name: "William Hayward (R)",
                value: "House::TX::35::William Hayward (R)",
                children: [],
              },
            ],
          },
          {
            name: "36",
            value: "House::TX::36",
            children: [
              {
                name: "Brian Babin (R)",
                value: "House::TX::36::Brian Babin (R)",
                children: [],
              },
              {
                name: "Rashad Lewis (D)",
                value: "House::TX::36::Rashad Lewis (D)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "UT",
        value: "House::UT",
        children: [
          {
            name: "1",
            value: "House::UT::1",
            children: [
              {
                name: "Jamie Cheek (D)",
                value: "House::UT::1::Jamie Cheek (D)",
                children: [],
              },
              {
                name: "Darren Parry (D)",
                value: "House::UT::1::Darren Parry (D)",
                children: [],
              },
              {
                name: "Kerry Gibson (R)",
                value: "House::UT::1::Kerry Gibson (R)",
                children: [],
              },
              {
                name: "Blake Moore (R)",
                value: "House::UT::1::Blake Moore (R)",
                children: [],
              },
              {
                name: "Bob Stevenson (R)",
                value: "House::UT::1::Bob Stevenson (R)",
                children: [],
              },
              {
                name: "Katie Witt (R)",
                value: "House::UT::1::Katie Witt (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::UT::2",
            children: [
              {
                name: "Chris Stewart (R)",
                value: "House::UT::2::Chris Stewart (R)",
                children: [],
              },
              {
                name: "John Kael Weston (D)",
                value: "House::UT::2::John Kael Weston (D)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::UT::3",
            children: [
              {
                name: "John Curtis (R)",
                value: "House::UT::3::John Curtis (R)",
                children: [],
              },
              {
                name: "Devin Thorpe (D)",
                value: "House::UT::3::Devin Thorpe (D)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::UT::4",
            children: [
              {
                name: "Ben McAdams (D)",
                value: "House::UT::4::Ben McAdams (D)",
                children: [],
              },
              {
                name: "Trent Christensen (R)",
                value: "House::UT::4::Trent Christensen (R)",
                children: [],
              },
              {
                name: "Kim Coleman (R)",
                value: "House::UT::4::Kim Coleman (R)",
                children: [],
              },
              {
                name: "Jay McFarland (R)",
                value: "House::UT::4::Jay McFarland (R)",
                children: [],
              },
              {
                name: "Burgess Owens (R)",
                value: "House::UT::4::Burgess Owens (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "VT",
        value: "House::VT",
        children: [
          {
            name: 1,
            value: "House::VT::1",
            children: [
              {
                name: "Peter Welch (D)",
                value: "House::VT::1::Peter Welch (D)",
                children: [],
              },
              {
                name: "Jimmy Rodriguez (R)",
                value: "House::VT::1::Jimmy Rodriguez (R)",
                children: [],
              },
              {
                name: "Justin Tuthill (R)",
                value: "House::VT::1::Justin Tuthill (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "VA",
        value: "House::VA",
        children: [
          {
            name: "1",
            value: "House::VA::1",
            children: [
              {
                name: "Robert J. Wittman (R)",
                value: "House::VA::1::Robert J. Wittman (R)",
                children: [],
              },
              {
                name: "Qasim Rashid (D)",
                value: "House::VA::1::Qasim Rashid (D)",
                children: [],
              },
              {
                name: "Vangie Williams (D)",
                value: "House::VA::1::Vangie Williams (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::VA::2",
            children: [
              {
                name: "Elaine Luria (D)",
                value: "House::VA::2::Elaine Luria (D)",
                children: [],
              },
              {
                name: "Jarome Bell (R)",
                value: "House::VA::2::Jarome Bell (R)",
                children: [],
              },
              {
                name: "Ben Loyola (R)",
                value: "House::VA::2::Ben Loyola (R)",
                children: [],
              },
              {
                name: "Scott Taylor (R)",
                value: "House::VA::2::Scott Taylor (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::VA::3",
            children: [
              {
                name: "Robert C. Scott (D)",
                value: "House::VA::3::Robert C. Scott (D)",
                children: [],
              },
              {
                name: "John Collick (R)",
                value: "House::VA::3::John Collick (R)",
                children: [],
              },
              {
                name: "Madison Downs (R)",
                value: "House::VA::3::Madison Downs (R)",
                children: [],
              },
              {
                name: "George Yacus (R)",
                value: "House::VA::3::George Yacus (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::VA::4",
            children: [
              {
                name: "Aston Donald McEachin (D)",
                value: "House::VA::4::Aston Donald McEachin (D)",
                children: [],
              },
              {
                name: "R. Cazel Levine (D)",
                value: "House::VA::4::R. Cazel Levine (D)",
                children: [],
              },
              {
                name: "Leon Benjamin (R)",
                value: "House::VA::4::Leon Benjamin (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::VA::5",
            children: [
              {
                name: "Roger Dean Huffstetler (D)",
                value: "House::VA::5::Roger Dean Huffstetler (D)",
                children: [],
              },
              {
                name: "John Lesinski (D)",
                value: "House::VA::5::John Lesinski (D)",
                children: [],
              },
              {
                name: "Claire Russo (D)",
                value: "House::VA::5::Claire Russo (D)",
                children: [],
              },
              {
                name: "Cameron Webb (D)",
                value: "House::VA::5::Cameron Webb (D)",
                children: [],
              },
              {
                name: "Denver Lee Riggleman III (R)",
                value: "House::VA::5::Denver Lee Riggleman III (R)",
                children: [],
              },
              {
                name: "Bob Good (R)",
                value: "House::VA::5::Bob Good (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::VA::6",
            children: [
              {
                name: "Benjamin Lee Cline (R)",
                value: "House::VA::6::Benjamin Lee Cline (R)",
                children: [],
              },
              {
                name: "Nicholas Betts (R)",
                value: "House::VA::6::Nicholas Betts (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::VA::7",
            children: [
              {
                name: "Abigail Spanberger (D)",
                value: "House::VA::7::Abigail Spanberger (D)",
                children: [],
              },
              {
                name: "Craig Ennis (R)",
                value: "House::VA::7::Craig Ennis (R)",
                children: [],
              },
              {
                name: "Nick Freitas (R)",
                value: "House::VA::7::Nick Freitas (R)",
                children: [],
              },
              {
                name: "Pete Greenwald (R)",
                value: "House::VA::7::Pete Greenwald (R)",
                children: [],
              },
              {
                name: "Andrew Knaggs (R)",
                value: "House::VA::7::Andrew Knaggs (R)",
                children: [],
              },
              {
                name: "John McGuire (R)",
                value: "House::VA::7::John McGuire (R)",
                children: [],
              },
              {
                name: "Tina Ramirez (R)",
                value: "House::VA::7::Tina Ramirez (R)",
                children: [],
              },
              {
                name: "Jason Alexander Roberge (R)",
                value: "House::VA::7::Jason Alexander Roberge (R)",
                children: [],
              },
              {
                name: "Bridgette Williams (R)",
                value: "House::VA::7::Bridgette Williams (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::VA::8",
            children: [
              {
                name: "Donald Sternoff Beyer Jr. (D)",
                value: "House::VA::8::Donald Sternoff Beyer Jr. (D)",
                children: [],
              },
              {
                name: "Mark Ellmore (R)",
                value: "House::VA::8::Mark Ellmore (R)",
                children: [],
              },
              {
                name: "Jeff Jordan (R)",
                value: "House::VA::8::Jeff Jordan (R)",
                children: [],
              },
              {
                name: "Heerak Christian Kim (R)",
                value: "House::VA::8::Heerak Christian Kim (R)",
                children: [],
              },
              {
                name: "Mike Webb (R)",
                value: "House::VA::8::Mike Webb (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::VA::9",
            children: [
              {
                name: "H. Morgan Griffith (R)",
                value: "House::VA::9::H. Morgan Griffith (R)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::VA::10",
            children: [
              {
                name: "Jennifer Wexton (D)",
                value: "House::VA::10::Jennifer Wexton (D)",
                children: [],
              },
              {
                name: "Aliscia Andrews (R)",
                value: "House::VA::10::Aliscia Andrews (R)",
                children: [],
              },
              {
                name: "Jeffery Anthony Dove Jr. (R)",
                value: "House::VA::10::Jeffery Anthony Dove Jr. (R)",
                children: [],
              },
              {
                name: "Rob Jones (R)",
                value: "House::VA::10::Rob Jones (R)",
                children: [],
              },
              {
                name: "Elizabeth Stone (R)",
                value: "House::VA::10::Elizabeth Stone (R)",
                children: [],
              },
              {
                name: "Matthew Truong (R)",
                value: "House::VA::10::Matthew Truong (R)",
                children: [],
              },
            ],
          },
          {
            name: "11",
            value: "House::VA::11",
            children: [
              {
                name: "Gerald Edward Connolly (D)",
                value: "House::VA::11::Gerald Edward Connolly (D)",
                children: [],
              },
              {
                name: "Zainab Mohsini (D)",
                value: "House::VA::11::Zainab Mohsini (D)",
                children: [],
              },
              {
                name: "Manga Anantatmula (R)",
                value: "House::VA::11::Manga Anantatmula (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "WA",
        value: "House::WA",
        children: [
          {
            name: "1",
            value: "House::WA::1",
            children: [
              {
                name: "Jeffrey Beeler (R)",
                value: "House::WA::1::Jeffrey Beeler (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::WA::2",
            children: [
              {
                name: "Rick Larsen (D)",
                value: "House::WA::2::Rick Larsen (D)",
                children: [],
              },
              {
                name: "Jason Call (D)",
                value: "House::WA::2::Jason Call (D)",
                children: [],
              },
              {
                name: "Timothy Hazelo (R)",
                value: "House::WA::2::Timothy Hazelo (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::WA::3",
            children: [
              {
                name: "Jaime Herrera Beutler (R)",
                value: "House::WA::3::Jaime Herrera Beutler (R)",
                children: [],
              },
              {
                name: "Peter Khalil (D)",
                value: "House::WA::3::Peter Khalil (D)",
                children: [],
              },
              {
                name: "Carolyn Long (D)",
                value: "House::WA::3::Carolyn Long (D)",
                children: [],
              },
              {
                name: "Rudy Atencio (R)",
                value: "House::WA::3::Rudy Atencio (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::WA::4",
            children: [
              {
                name: "Dan Newhouse (R)",
                value: "House::WA::4::Dan Newhouse (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::WA::5",
            children: [
              {
                name: "Cathy McMorris Rodgers (R)",
                value: "House::WA::5::Cathy McMorris Rodgers (R)",
                children: [],
              },
              {
                name: "Christopher Armitage (D)",
                value: "House::WA::5::Christopher Armitage (D)",
                children: [],
              },
              {
                name: "Rob Chase (R)",
                value: "House::WA::5::Rob Chase (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::WA::6",
            children: [
              {
                name: "Derek Kilmer (D)",
                value: "House::WA::6::Derek Kilmer (D)",
                children: [],
              },
              {
                name: "Rebecca Parson (D)",
                value: "House::WA::6::Rebecca Parson (D)",
                children: [],
              },
              {
                name: "Elizabeth Kreiselmaier (R)",
                value: "House::WA::6::Elizabeth Kreiselmaier (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::WA::7",
            children: [
              {
                name: "Pramila Jayapal (D)",
                value: "House::WA::7::Pramila Jayapal (D)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::WA::8",
            children: [
              {
                name: "James Mitchell (D)",
                value: "House::WA::8::James Mitchell (D)",
                children: [],
              },
              {
                name: "Jesse Jensen (R)",
                value: "House::WA::8::Jesse Jensen (R)",
                children: [],
              },
              {
                name: "Keith Swank (R)",
                value: "House::WA::8::Keith Swank (R)",
                children: [],
              },
            ],
          },
          {
            name: "9",
            value: "House::WA::9",
            children: [
              {
                name: "Adam Smith (D)",
                value: "House::WA::9::Adam Smith (D)",
                children: [],
              },
            ],
          },
          {
            name: "10",
            value: "House::WA::10",
            children: [
              {
                name: "Joshua Collins (D)",
                value: "House::WA::10::Joshua Collins (D)",
                children: [],
              },
              {
                name: "Beth Doglio (D)",
                value: "House::WA::10::Beth Doglio (D)",
                children: [],
              },
              {
                name: "Phil Gardner (D)",
                value: "House::WA::10::Phil Gardner (D)",
                children: [],
              },
              {
                name: "Kristine Reeves (D)",
                value: "House::WA::10::Kristine Reeves (D)",
                children: [],
              },
              {
                name: "Marilyn Strickland (D)",
                value: "House::WA::10::Marilyn Strickland (D)",
                children: [],
              },
              {
                name: "Dan Gordon (R)",
                value: "House::WA::10::Dan Gordon (R)",
                children: [],
              },
              {
                name: "Don Hewett (R)",
                value: "House::WA::10::Don Hewett (R)",
                children: [],
              },
              {
                name: "Rian Ingrim (R)",
                value: "House::WA::10::Rian Ingrim (R)",
                children: [],
              },
              {
                name: "Jackson Maynard (R)",
                value: "House::WA::10::Jackson Maynard (R)",
                children: [],
              },
              {
                name: "Nancy Slotnick (R)",
                value: "House::WA::10::Nancy Slotnick (R)",
                children: [],
              },
              {
                name: "Ryan Tate (R)",
                value: "House::WA::10::Ryan Tate (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "WV",
        value: "House::WV",
        children: [
          {
            name: "1",
            value: "House::WV::1",
            children: [
              {
                name: "Natalie Cline (D)",
                value: "House::WV::1::Natalie Cline (D)",
                children: [],
              },
              {
                name: "Tom Payne (D)",
                value: "House::WV::1::Tom Payne (D)",
                children: [],
              },
              {
                name: "David McKinley (R)",
                value: "House::WV::1::David McKinley (R)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::WV::2",
            children: [
              {
                name: "Cathy Kunkel (D)",
                value: "House::WV::2::Cathy Kunkel (D)",
                children: [],
              },
              {
                name: "Alexander Mooney (R)",
                value: "House::WV::2::Alexander Mooney (R)",
                children: [],
              },
              {
                name: "Matthew Hahn (R)",
                value: "House::WV::2::Matthew Hahn (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::WV::3",
            children: [
              {
                name: "Paul Davis (D)",
                value: "House::WV::3::Paul Davis (D)",
                children: [],
              },
              {
                name: "Jeff Lewis (D)",
                value: "House::WV::3::Jeff Lewis (D)",
                children: [],
              },
              {
                name: "Hilary Turner (D)",
                value: "House::WV::3::Hilary Turner (D)",
                children: [],
              },
              {
                name: "Eugene Watson (D)",
                value: "House::WV::3::Eugene Watson (D)",
                children: [],
              },
              {
                name: "Carol Miller (R)",
                value: "House::WV::3::Carol Miller (R)",
                children: [],
              },
              {
                name: "Russell Siegel (R)",
                value: "House::WV::3::Russell Siegel (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "WI",
        value: "House::WI",
        children: [
          {
            name: "1",
            value: "House::WI::1",
            children: [
              {
                name: "Angela Cunningham (D)",
                value: "House::WI::1::Angela Cunningham (D)",
                children: [],
              },
              {
                name: "Josh Pade (D)",
                value: "House::WI::1::Josh Pade (D)",
                children: [],
              },
              {
                name: "Roger Polack (D)",
                value: "House::WI::1::Roger Polack (D)",
                children: [],
              },
            ],
          },
          {
            name: "2",
            value: "House::WI::2",
            children: [
              {
                name: "Mark Pocan (D)",
                value: "House::WI::2::Mark Pocan (D)",
                children: [],
              },
              {
                name: "Jeffery Heller (R)",
                value: "House::WI::2::Jeffery Heller (R)",
                children: [],
              },
              {
                name: "Daniel Theron (R)",
                value: "House::WI::2::Daniel Theron (R)",
                children: [],
              },
            ],
          },
          {
            name: "3",
            value: "House::WI::3",
            children: [
              {
                name: "Ronald James Kind (D)",
                value: "House::WI::3::Ronald James Kind (D)",
                children: [],
              },
              {
                name: "Mark A. Neumann (D)",
                value: "House::WI::3::Mark A. Neumann (D)",
                children: [],
              },
              {
                name: "Brandon Cook (R)",
                value: "House::WI::3::Brandon Cook (R)",
                children: [],
              },
              {
                name: "Jessi Ebben (R)",
                value: "House::WI::3::Jessi Ebben (R)",
                children: [],
              },
              {
                name: "John Garske (R)",
                value: "House::WI::3::John Garske (R)",
                children: [],
              },
              {
                name: "Tim Peters (R)",
                value: "House::WI::3::Tim Peters (R)",
                children: [],
              },
              {
                name: "Jonathan Sundblom (R)",
                value: "House::WI::3::Jonathan Sundblom (R)",
                children: [],
              },
              {
                name: "Derrick Van Orden (R)",
                value: "House::WI::3::Derrick Van Orden (R)",
                children: [],
              },
              {
                name: "Alex Virijevich (R)",
                value: "House::WI::3::Alex Virijevich (R)",
                children: [],
              },
            ],
          },
          {
            name: "4",
            value: "House::WI::4",
            children: [
              {
                name: "Gwen Moore (D)",
                value: "House::WI::4::Gwen Moore (D)",
                children: [],
              },
              {
                name: "Cynthia Werner (R)",
                value: "House::WI::4::Cynthia Werner (R)",
                children: [],
              },
            ],
          },
          {
            name: "5",
            value: "House::WI::5",
            children: [
              {
                name: "Tom Palzewicz (D)",
                value: "House::WI::5::Tom Palzewicz (D)",
                children: [],
              },
              {
                name: "Clifford Detemple (R)",
                value: "House::WI::5::Clifford Detemple (R)",
                children: [],
              },
              {
                name: "Scott Fitzgerald (R)",
                value: "House::WI::5::Scott Fitzgerald (R)",
                children: [],
              },
            ],
          },
          {
            name: "6",
            value: "House::WI::6",
            children: [
              {
                name: "Michael Beardsley (D)",
                value: "House::WI::6::Michael Beardsley (D)",
                children: [],
              },
              {
                name: "Matthew Boor (D)",
                value: "House::WI::6::Matthew Boor (D)",
                children: [],
              },
              {
                name: "Jessica King (D)",
                value: "House::WI::6::Jessica King (D)",
                children: [],
              },
              {
                name: "Amy Washburn (D)",
                value: "House::WI::6::Amy Washburn (D)",
                children: [],
              },
              {
                name: "Glenn Grothman (R)",
                value: "House::WI::6::Glenn Grothman (R)",
                children: [],
              },
            ],
          },
          {
            name: "7",
            value: "House::WI::7",
            children: [
              {
                name: "Tom Tiffany (R)",
                value: "House::WI::7::Tom Tiffany (R)",
                children: [],
              },
            ],
          },
          {
            name: "8",
            value: "House::WI::8",
            children: [
              {
                name: "Amanda Stuck (D)",
                value: "House::WI::8::Amanda Stuck (D)",
                children: [],
              },
              {
                name: "Mike Gallagher (R)",
                value: "House::WI::8::Mike Gallagher (R)",
                children: [],
              },
            ],
          },
        ],
      },
      {
        name: "WY",
        value: "House::WY",
        children: [
          {
            name: 1,
            value: "House::WY::1",
            children: [
              {
                name: "Carl Beach (D)",
                value: "House::WY::1::Carl Beach (D)",
                children: [],
              },
              {
                name: "Carol Hafner (D)",
                value: "House::WY::1::Carol Hafner (D)",
                children: [],
              },
              {
                name: "Liz Cheney (R)",
                value: "House::WY::1::Liz Cheney (R)",
                children: [],
              },
            ],
          },
        ],
      },
    ],
  },
];
module.exports.loadCandidates = loadCandidates;
