/*
Copyright (c) 2018 IETF Trust, Austin Wright, Henry Andrews, Geraint Luff,
and Cloudflare, Inc. All rights reserved.


Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Retrieved from http://json-schema.org/
*/

const Draft4 = {
  id: "http://json-schema.org/draft-04/schema#",
  $schema: "http://json-schema.org/draft-04/schema#",
  description:
    "Modified JSON Schema draft v4 that includes the optional '$ref' and 'format'",
  definitions: {
    schemaArray: {
      type: "array",
      minItems: 1,
      items: { $ref: "#" }
    },
    positiveInteger: {
      type: "integer",
      minimum: 0
    },
    positiveIntegerDefault0: {
      allOf: [{ $ref: "#/definitions/positiveInteger" }, { default: 0 }]
    },
    simpleTypes: {
      enum: [
        "array",
        "boolean",
        "integer",
        "null",
        "number",
        "object",
        "string"
      ]
    },
    stringArray: {
      type: "array",
      items: { type: "string" },
      minItems: 1,
      uniqueItems: true
    }
  },
  type: "object",
  properties: {
    id: {
      type: "string",
      format: "uri"
    },
    $schema: {
      type: "string",
      format: "uri"
    },
    $ref: {
      type: "string"
    },
    format: {
      type: "string"
    },
    title: {
      type: "string"
    },
    description: {
      type: "string"
    },
    default: {},
    multipleOf: {
      type: "number",
      minimum: 0,
      exclusiveMinimum: true
    },
    maximum: {
      type: "number"
    },
    exclusiveMaximum: {
      type: "boolean",
      default: false
    },
    minimum: {
      type: "number"
    },
    exclusiveMinimum: {
      type: "boolean",
      default: false
    },
    maxLength: { $ref: "#/definitions/positiveInteger" },
    minLength: { $ref: "#/definitions/positiveIntegerDefault0" },
    pattern: {
      type: "string",
      format: "regex"
    },
    additionalItems: {
      anyOf: [{ type: "boolean" }, { $ref: "#" }],
      default: {}
    },
    items: {
      anyOf: [{ $ref: "#" }, { $ref: "#/definitions/schemaArray" }],
      default: {}
    },
    maxItems: { $ref: "#/definitions/positiveInteger" },
    minItems: { $ref: "#/definitions/positiveIntegerDefault0" },
    uniqueItems: {
      type: "boolean",
      default: false
    },
    maxProperties: { $ref: "#/definitions/positiveInteger" },
    minProperties: { $ref: "#/definitions/positiveIntegerDefault0" },
    required: { $ref: "#/definitions/stringArray" },
    additionalProperties: {
      anyOf: [{ type: "boolean" }, { $ref: "#" }],
      default: {}
    },
    definitions: {
      type: "object",
      additionalProperties: { $ref: "#" },
      default: {}
    },
    properties: {
      type: "object",
      additionalProperties: { $ref: "#" },
      default: {}
    },
    patternProperties: {
      type: "object",
      additionalProperties: { $ref: "#" },
      default: {}
    },
    dependencies: {
      type: "object",
      additionalProperties: {
        anyOf: [{ $ref: "#" }, { $ref: "#/definitions/stringArray" }]
      }
    },
    enum: {
      type: "array",
      minItems: 1,
      uniqueItems: true
    },
    type: {
      anyOf: [
        { $ref: "#/definitions/simpleTypes" },
        {
          type: "array",
          items: { $ref: "#/definitions/simpleTypes" },
          minItems: 1,
          uniqueItems: true
        }
      ]
    },
    allOf: { $ref: "#/definitions/schemaArray" },
    anyOf: { $ref: "#/definitions/schemaArray" },
    oneOf: { $ref: "#/definitions/schemaArray" },
    not: { $ref: "#" }
  },
  dependencies: {
    exclusiveMaximum: ["maximum"],
    exclusiveMinimum: ["minimum"]
  },
  default: {}
};

module.exports.Draft4 = Draft4;
