.PHONY: fmt test yarn

fmt:
	yarn fmt:all

.npmrc:
	echo '@digiresilience:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/16744138/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc

yarn:
	yarn

test: yarn
	yarn test
	yarn lint

publish: test .npmrc
	npm publish
