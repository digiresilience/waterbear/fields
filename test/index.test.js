const {
  indexOfField,
  getSubmitterFields,
  getAnalystFields,
  getRequiredFieldIds,
  getOptionIdsForField,
  getOptionsForField,
  getFieldById,
  getFieldIds,
  validateFields
} = require("../src/fields");
const {
  validateSchema,
  validateData,
  fieldsToSchema
} = require("../src/validation");

test("validateFields", () => {
  const r = validateFields({});
  expect(typeof r === "object").toBe(true);

  expect(Array.isArray(r.details)).toBe(true);
});

test("getFieldIds", () => {
  expect(
    getFieldIds([
      { id: "foo", one: 1 },
      { id: "bar", one: 2 }
    ])
  ).toEqual(["foo", "bar"]);
});

test("indexOfField", () => {
  expect(
    indexOfField(
      [
        { id: "foo", one: 1 },
        { id: "bar", one: 2 }
      ],
      "bar"
    )
  ).toEqual(1);
});

test("getFieldById", () => {
  const fields = [
    { id: "foo", one: 1 },
    { id: "bar", one: 2 }
  ];
  expect(getFieldById(fields, "foo")).toEqual({ id: "foo", one: 1 });
  expect(getFieldById(fields, "nope")).toBeFalsy();
});
test("getOptionsForField", () => {
  const fields = [
    { id: "foo", one: 1 },
    { id: "seasons", options: ["winter", "summer"] },
    { id: "bar", one: 2 }
  ];
  expect(getOptionsForField(fields, "seasons")).toEqual(["winter", "summer"]);
  expect(getOptionsForField(fields, "bar")).toBe(false);
  expect(getOptionsForField(fields, "nope")).toBe(false);
});

test("getOptionIdsForField", () => {
  const fields = [
    { id: "foo", type: "text" },
    {
      id: "seasons",
      type: "select",
      options: [
        { id: "win", name: "Winter" },
        { id: "sum", name: "Summer" }
      ]
    },
    { id: "bar", type: "text" }
  ];
  expect(getOptionIdsForField(fields, "seasons")).toEqual(["win", "sum"]);
  expect(getOptionIdsForField(fields, "foo")).toBe(false);
  expect(getOptionIdsForField(fields, "nope")).toBe(false);
});

test("getRequiredFieldIds", () => {
  const fields = [
    { id: "foo", type: "text", required: true },
    { id: "bar", type: "text" },
    { id: "car", type: "text", required: true }
  ];
  expect(getRequiredFieldIds(fields)).toEqual(["foo", "car"]);
});

test("getSubmitterFields", () => {
  const fields = [
    { id: "foo", type: "text" },
    { id: "bar", type: "text", for: "submitter" },
    { id: "car", type: "text", required: true }
  ];
  expect(getSubmitterFields(fields)).toEqual([
    { id: "bar", type: "text", for: "submitter" }
  ]);
});

test("getAnalystFields", () => {
  const fields = [
    { id: "foo", type: "text" },
    { id: "bar", type: "text", for: "submitter" },
    { id: "car", type: "text", for: "analyst", required: true }
  ];
  expect(getAnalystFields(fields)).toEqual([
    { id: "car", type: "text", for: "analyst", required: true }
  ]);
});
test("fieldsToSchema", () => {
  const fields = [
    { id: "foo", type: "text", required: true },
    {
      id: "seasons",
      type: "select",
      options: [
        { id: "win", name: "Winter" },
        { id: "sum", name: "Summer" }
      ]
    },
    { id: "bar", type: "text" }
  ];
  expect(fieldsToSchema(fields)).toEqual({
    $schema: "http://json-schema.org/draft-04/schema#",
    type: "object",
    required: ["foo"],
    properties: {
      foo: { type: "string" },
      seasons: { type: ["string", "null"], enum: ["win", "sum", null] },
      bar: { type: "string" }
    }
  });

  const fields2 = [
    {
      id: "seasons",
      type: "select",
      required: true,
      options: [
        { id: "win", name: "Winter" },
        { id: "sum", name: "Summer" }
      ]
    }
  ];

  expect(fieldsToSchema(fields2)).toEqual({
    $schema: "http://json-schema.org/draft-04/schema#",
    type: "object",
    required: ["seasons"],
    properties: {
      seasons: { type: "string", enum: ["win", "sum"] }
    }
  });
});

test("validateSchema", () => {
  const fields = [
    { id: "foo", type: "text" },
    {
      id: "seasons",
      type: "select",
      options: [
        { id: "win", name: "Winter" },
        { id: "sum", name: "Summer" }
      ]
    },
    { id: "bar", type: "text" }
  ];
  const schema = fieldsToSchema(fields);
  expect(validateSchema(schema)).toEqual(true);

  schema.properties = 0;
  expect(() => validateSchema(schema)).toThrow();
});

test("validateData", () => {
  const fields = [
    { id: "foo", type: "text" },
    {
      id: "seasons",
      type: "select",
      options: [
        { id: "win", name: "Winter" },
        { id: "sum", name: "Summer" }
      ]
    },
    { id: "bar", type: "text" }
  ];
  const schema = fieldsToSchema(fields);
  const data = {
    foo: "hello",
    bar: "world",
    seasons: "win"
  };
  expect(validateData(schema, data)).toHaveLength(0);

  const data2 = {
    foo: "hello",
    bar: "world",
    seasons: "venus"
  };
  let res = validateData(schema, data2);
  expect(res).toHaveLength(1);
  expect(res[0]).toHaveProperty("dataPath", "seasons");

  res = validateData(schema, {
    seasons: null
  });
  expect(res).toHaveLength(0);

  res = validateData(schema, {
    seasons: ""
  });
  expect(res).toHaveLength(1);

  res = validateData(schema, {
    seasons: []
  });
  expect(res).toHaveLength(2);
});
