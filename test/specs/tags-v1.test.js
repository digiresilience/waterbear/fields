const {
  getFieldById,
  validateFields,
  loadFields
} = require("../../src/fields");
const { TagsV1 } = require("../../src/specs/tags-v1");

const fields = loadFields(TagsV1);
test("v1 validates", () => {
  expect(validateFields(fields)).toEqual(true);

  const desc = getFieldById(fields, "incident");
  expect(desc.name).toEqual("Incident");
});
