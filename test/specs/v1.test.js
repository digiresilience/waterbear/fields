const {
  getFieldById,
  getSubmitterFields,
  getDefaults,
  validateFields,
  loadFields
} = require("../../src/fields");
const { FieldsV1 } = require("../../src/specs/v1");
const {
  validateSchema,
  validateData,
  fieldsToSchema
} = require("../../src/validation");

const fields = loadFields(FieldsV1);
const submitterFields = getSubmitterFields(fields);
test("v1 validates", () => {
  expect(validateFields(fields)).toEqual(true);

  // a random sampling of tests, this isn't exhaustive
  const desc = getFieldById(fields, "description");
  expect(desc.name).toEqual("Description");
  expect(desc.hidden).toEqual(false);
  expect(desc.default).toEqual(null);

  const actor = getFieldById(fields, "actor");
  expect(actor.name).toEqual("Actor");

  const communities = getFieldById(fields, "communities");
  expect(communities.name).toEqual("Target Community");

  const medium = getFieldById(fields, "medium");
  expect(medium.type).toEqual("multiselect");
  expect(medium.default).toEqual([]);

  const email = getFieldById(fields, "submitter_email");
  expect(email.type).toEqual("text");
  expect(email.hidden).toEqual(true);
  expect(email.default).toEqual(null);
});

test("v1 is a valid schema", () => {
  const schema = fieldsToSchema(fields);
  expect(schema).toBeTruthy();
  expect(validateSchema(schema)).toEqual(true);
});

describe("v1 data validation", () => {
  const schema = fieldsToSchema(fields);
  test("minimum possible valid user response", () => {
    const data = {
      description: "It was a snowy wintry day..",
      submitter_email: "foo@foo.com",
      reason: "If only we could know"
    };
    const errors = validateData(schema, data);
    expect(errors).toHaveLength(0);
  });
  test("invalid response", () => {
    const data = {
      description: null,
      reason: 0
    };
    const errors = validateData(schema, data);
    expect(errors).toHaveLength(3);
  });

  test("different inputs", () => {
    let errors = validateData(schema, {
      description: "",
      submitter_email: "foo@foo.com",
      attachments: null
    });
    expect(errors).toHaveLength(0);

    errors = validateData(schema, {
      description: "",
      submitter_email: "foo@foo.com",
      attachments: []
    });
    expect(errors).toHaveLength(0);

    errors = validateData(schema, {
      description: "",
      submitter_email: "foo@foo.com",
      attachments: [{ filename: "foo.txt", mimetype: "text", payload: "foo" }]
    });

    expect(errors).toHaveLength(0);
    errors = validateData(schema, {
      description: "",
      submitter_email: "foo@foo.com",
      attachments: ""
    });
    expect(errors).toHaveLength(1);

    errors = validateData(schema, {
      description: "",
      submitter_email: "foo@foo.com",
      follow_up: null
    });
    expect(errors).toHaveLength(0);

    errors = validateData(schema, {
      description: "",
      follow_up: "",
      submitter_email: "foo@foo.com"
    });
    expect(errors).toHaveLength(1);
  });
  test("full valid response", () => {
    const data = {
      description: "It was a snowy wintry day..",
      reason: "If only we could know",
      medium: ["social-media-first-hand", "event"],
      medium_other: "saw it in grafitti too",
      geography: "KS",
      links: ["https://123", "https://789"],
      submitter_email: "alice@alice.com",
      partner_organization: "EL"
    };
    const errors = validateData(schema, data);
    expect(errors).toHaveLength(0);
  });

  test("defaults should validate", () => {
    const data = getDefaults(submitterFields);
    const errors = validateData(schema, data);
    expect(errors).toHaveLength(2);
    expect(errors[0]).toHaveProperty("dataPath", "description");
    expect(errors[1]).toHaveProperty("dataPath", "submitter_email");
  });
});
