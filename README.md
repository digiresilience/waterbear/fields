# 🐻 fields
[![pipeline status](https://gitlab.com/digiresilience/waterbear/fields/badges/develop/pipeline.svg)](https://gitlab.com/digiresilience/waterbear/fields/-/commits/develop)
[![coverage report](https://gitlab.com/digiresilience/waterbear/fields/badges/develop/coverage.svg)](https://gitlab.com/digiresilience/waterbear/fields/-/commits/develop)


A little library to standardize the form fields across all the components.

## Usage

```javascript
const { FieldsV1 } = require("@digiresilience/waterbear-fields");
const {
  getFieldIds,
  getOptionsForField,
  getOptionIdsForField,
  getSubmitterFields
} = require("@digiresilience/waterbear-fields/src/fields");
const {
  validateData,
  fieldsToSchema
} = require("@digiresilience/waterbear-fields/src/validation");

// get the ids of all the fields
const fieldIds = getFieldIds(FieldsV1);

/* => 
[
  'description',
  'time_of_day',
  'links',
  'witness',
  'medium',
  'medium_other',
  'reason',
  'sensitivity',
  'additional_info',
  'geography'
]
*/

// get the options for the field "witness"
const witnessOptions = getOptionsForField(FieldsV1, "witness");
/* => 
[
  { id: 'me', name: 'This happened to me' },
  { id: 'second-hand', name: 'This happened to someone I know' },
  { id: 'first-hand', name: 'I saw this' },
  { id: 'received', name: 'Someone sent this to me' },
  { id: 'other', name: 'None of these' }
]
*/

// like the previous one, but returns just the options' ids
const witnessOptions = getOptionIdssForField(FieldsV1, "witness");
// => [ 'me', 'second-hand', 'first-hand', 'received', 'other' ]

// get just the fields that a submitter should submit
const submitterFields = getSubmitterFields(FieldsV1);
/* =>
[
  {
    id: 'description',
    type: 'longtext',
    required: true,
    for: 'submitter'
  },
... snip ...
]
*/

// here we have some user submitted data
const userSubmittedData = {
  reason: "If only we could know",
  time_of_day: "morning",
  witness: "second-hand",
  medium: ["social-media", "event"],
  medium_other: "saw it in grafitti too",
  sensitivity: "nothing sensitive",
  geography: "KS",
  links: ["https://123", "https://789"]
};

// convert the fields spec into a json schema
const schema = fieldsToSchema(FieldsV1);
// validate this data against the schema...
// ...if there are no errors this function should return an empty array
const errors = validateData(schema, userSubmittedData);

// uhoh!
/* => 
[
  {
    keyword: 'required',
    dataPath: 'description',
    schemaPath: '#/required',
    params: { missingProperty: 'description' },
    message: 'is a required property',

    data: {
      reason: 'If only we could know',
      time_of_day: 'morning',
      witness: 'second-hand',
      medium: [Array],
      medium_other: 'saw it in grafitti too',
      sensitivity: 'nothing sensitive',
      geography: 'KS',
      links: [Array]
    }
  }
]
*/
```

## Development

```javascript

yarn
yarn test --watch
yarn lint

```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/waterbear/fields/blob/master/LICENSE.md)

analyst-backend is a free software project licensed under the GNU Affero
General Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).
